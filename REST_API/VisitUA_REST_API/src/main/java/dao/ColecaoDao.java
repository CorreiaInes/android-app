package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.Colecao;

/**
 * Data Access Object Coleções.
 */
public class ColecaoDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;
    
    /**
     * Retorna todas as Coleções.
     * @return lista de instâncias de Colecao.
     */
    public List<Colecao> getColecoes()
    {
        List<Colecao> list = null;
        
        try
        {
            list = new ArrayList();
            Colecao colecao;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Colecao;";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                colecao = new Colecao();
                
                colecao.setId(res.getInt("id"));
                colecao.setNome(res.getString("nome"));
                colecao.setDescricao(res.getString("descricao"));
                colecao.setDoador(res.getString("doador"));
                colecao.setColecaoMae(res.getInt("colecaoMae"));
                colecao.setLocalVisita(res.getString("localVisita"));
                colecao.setCoordenadasLocal(res.getString("coordenadasLocal"));
                        
                list.add(colecao);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        List<Colecao> toBeRemoved = new ArrayList<>();
        
        for(Colecao c : list)
        {
            Integer mae = c.getColecaoMae();
            if(mae != null)
               for(Colecao cM : list)
               {
                   if(cM.getId() == mae)
                   {
                       cM.setSubColecao(c);
                       toBeRemoved.add(c);
                   }
               }
        }
        
        for(Colecao c : toBeRemoved)
            list.remove(c);
        
        return list;
    }
    
    /**
     * Retorna uma Coleção da BD.
     * @param id id da Coleção.
     * @return instância de Colecao.
     */
    public Colecao getColecao(Integer id)
    {
        Colecao colecao = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Colecao WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                colecao = new Colecao();
                
                colecao.setId(res.getInt("id"));
                colecao.setNome(res.getString("nome"));
                colecao.setDescricao(res.getString("descricao"));
                colecao.setDoador(res.getString("doador"));
                colecao.setColecaoMae(res.getInt("colecaoMae"));
                colecao.setLocalVisita(res.getString("localVisita"));
                colecao.setCoordenadasLocal(res.getString("coordenadasLocal"));
                
                sql = "SELECT * FROM Colecao WHERE colecaoMae = " + id + ";";
                pst = connection.prepareStatement(sql);
                System.out.println(pst.toString());
                ResultSet tempRes = pst.executeQuery();
                while(tempRes.next())
                {   
                    Colecao sub = new Colecao();

                    sub.setId(tempRes.getInt("id"));
                    sub.setNome(tempRes.getString("nome"));
                    sub.setDescricao(tempRes.getString("descricao"));
                    sub.setDoador(tempRes.getString("doador"));
                    sub.setColecaoMae(tempRes.getInt("colecaoMae"));

                    colecao.setSubColecao(sub);
                }
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return colecao;
    }
    
    /**
     * Insere uma Coleção na BD.
     * @param colecao instância de um objeto Colecao.
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    public boolean insertColecao(Colecao colecao)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "INSERT INTO Colecao (nome, descricao, doador, colecaoMae, localVisita, coordenadasLocal) VALUES (?, ?, ?, ?, ?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, colecao.getNome());
            pst.setString(2, colecao.getDescricao());
            pst.setString(3, colecao.getDoador());
            if(colecao.getColecaoMae() == null) 
                pst.setString(4, null);
            else
                pst.setString(4, colecao.getColecaoMae().toString());
            pst.setString(5, colecao.getLocalVisita());
            pst.setString(6, colecao.getCoordenadasLocal());
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Remove uma Coleção na BD.
     * @param id id da Coleção.
     * @return true, se o registo for removido; false, se o registo não for removido.
     */
    public boolean deleteColecao(Integer id)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM Colecao WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean updateColecao (Colecao colecao)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "UPDATE Colecao SET nome = ?, descricao = ?, doador = ?, colecaoMae = ?, localVisita = ?, coordenadasLocal = ? WHERE id = " + colecao.getId() + ";";
            pst = connection.prepareStatement(sql);
            pst.setString(1, colecao.getNome());
            pst.setString(2, colecao.getDescricao());
            pst.setString(3, colecao.getDoador());
            if(colecao.getColecaoMae() == null) 
                pst.setString(4, null);
            else
                pst.setString(4, colecao.getColecaoMae().toString());
            pst.setString(5, colecao.getLocalVisita());
            pst.setString(6, colecao.getCoordenadasLocal());
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            return true; 
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }      
    }
    
}
