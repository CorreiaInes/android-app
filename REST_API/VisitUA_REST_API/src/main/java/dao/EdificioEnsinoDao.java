package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.EdificioEnsino;

/**
 * Data Access Object Ensino por Edifício.
 */
public class EdificioEnsinoDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;

    public List<EdificioEnsino> getEdificioEnsinos()
    {
        List<EdificioEnsino> list = null;
        
        try
        {
            list = new ArrayList();
            EdificioEnsino edificioEnsino;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM EdificioEnsino;";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                edificioEnsino = new EdificioEnsino();
                
                edificioEnsino.setIdEdificio(res.getString("edificio"));
                edificioEnsino.setIdEnsino(res.getInt("ensino"));
                        
                list.add(edificioEnsino);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return list;
    }
    
    public boolean insertEnsino(EdificioEnsino edificioEnsino)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            
            sql = "INSERT INTO EdificioEnsino (edificio, ensino) VALUES (?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, edificioEnsino.getIdEdificio());
            if(edificioEnsino.getIdEnsino()!= null)
                pst.setString(2, edificioEnsino.getIdEnsino().toString());
            else
                pst.setString(2, null);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean deleteEnsino(String idEdificio, Integer idEnsino)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM EdificioEnsino WHERE edificio = '" + idEdificio + "' AND ensino = " + idEnsino + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
}
