package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.ParquePublico;

/**
 * Data Access Object Parque Público.
 */
public class ParquePublicoDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;
    
    /**
     * Retorna todos os Parques Públicos.
     * @return lista de instâncias de ParquePublico.
     */
    public List<ParquePublico> getParquesPulbicos()
    {
        List<ParquePublico> list = null;
        
        try
        {
            list = new ArrayList();
            ParquePublico parque;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM ParquePublico;";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                parque = new ParquePublico();
                
                parque.setId(res.getInt("id"));
                parque.setNome(res.getString("nome"));
                parque.setCoordenadas(res.getString("coordenadas"));
                        
                list.add(parque);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return list;
    }
    
    public ParquePublico getParquePublico(Integer id)
    {
        ParquePublico parque = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM ParquePublico WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                parque = new ParquePublico();
                
                parque.setId(res.getInt("id"));
                parque.setNome(res.getString("nome"));
                parque.setCoordenadas(res.getString("coordenadas"));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return parque;
    }
    
    public boolean insertParquePublico(ParquePublico parque)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "INSERT INTO ParquePublico (nome, coordenadas)  VALUES (?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, parque.getNome());
            pst.setString(2, parque.getCoordenadas());
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Remove um Serviço na BD.
     * @param id id do Serviço.
     * @return true, se o registo for removido; false, se o registo não for removido.
     */
    public boolean deleteParquePublico(String id)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM ParquePublico WHERE id = '" + id + "';";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean updateParquePublico(ParquePublico parque)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "UPDATE ParquePublico SET nome=?, coordenadas=? WHERE id = " + parque.getId() + ";";
            pst = connection.prepareStatement(sql);
            pst.setString(1, parque.getNome());
            pst.setString(2, parque.getCoordenadas());
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            return true; 
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }      
    }
    
}
