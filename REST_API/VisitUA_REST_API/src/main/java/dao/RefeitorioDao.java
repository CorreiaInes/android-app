package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.Refeitorio;

/**
 * Data Access Object Coleções.
 */
public class RefeitorioDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;
    
    /**
     * Retorna todas as Refeitório.
     * @return lista de instâncias de Refeitorio.
     */
    public List<Refeitorio> getRefeitorios()
    {
        List<Refeitorio> list = null;
        
        try
        {
            list = new ArrayList();
            Refeitorio refeitorio;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Refeitorio;";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                refeitorio = new Refeitorio();
                
                refeitorio.setId(res.getInt("id"));
                refeitorio.setNome(res.getString("nome"));
                refeitorio.setCoordenadas(res.getString("coordenadas"));
                        
                list.add(refeitorio);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return list;
    }
    
    public Refeitorio getRefeitorio(Integer id)
    {
        Refeitorio refeitorio = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Refeitorio WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                refeitorio = new Refeitorio();
                
                refeitorio.setId(res.getInt("id"));
                refeitorio.setNome(res.getString("nome"));
                refeitorio.setCoordenadas(res.getString("coordenadas"));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return refeitorio;
    }
    
    public boolean insertRefeitorio(Refeitorio refeitorio)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "INSERT INTO Refeitorio (nome, coordenadas)  VALUES (?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, refeitorio.getNome());
            pst.setString(2, refeitorio.getCoordenadas());
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Remove um Serviço na BD.
     * @param id id do Serviço.
     * @return true, se o registo for removido; false, se o registo não for removido.
     */
    public boolean deleteRefeitorio(String id)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM Refeitorio WHERE id = '" + id + "';";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean updateEdificio(Refeitorio edificio)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "UPDATE Refeitorio SET nome=?, coordenadas=? WHERE id = " + edificio.getId() + ";";
            pst = connection.prepareStatement(sql);
            pst.setString(1, edificio.getNome());
            pst.setString(2, edificio.getCoordenadas());
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            return true; 
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }      
    }
    
}
