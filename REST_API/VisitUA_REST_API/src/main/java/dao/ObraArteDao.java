package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.ObraArte;

/**
 * Data Access Object Obras de Arte.
 */
public class ObraArteDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;
    
    /**
     * Retorna uma lista de instâncias POJOS ObraArte, que contêm dados sobre 
     * as obras de arte obtidos a partir da Base de Dados
     * @return lista de instâncias de ObraArte.
     */
    public List<ObraArte> getObrasArte()
    {
        List<ObraArte> list = null;
        
        try
        {
            list = new ArrayList();
            ObraArte obraarte;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM ObraArte;";
            pst = connection.prepareStatement(sql);
            //System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                obraarte = new ObraArte();
                
                obraarte.setId(res.getInt("id"));
                obraarte.setNome(res.getString("nome"));
                obraarte.setLocal(res.getString("local_"));
                obraarte.setDimensoes(res.getString("dimensoes"));
                obraarte.setAutor(res.getString("autor"));
                obraarte.setMateriais(res.getString("materiais"));
                obraarte.setDataColocacaoUA(res.getString("dataColocacaoUA"));
                obraarte.setCoordenadas(res.getString("coordenadas"));
                        
                list.add(obraarte);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return list;
    }
    
    /**
     * Retorna uma Obra de Arte da BD.
     * @param id id da Obra de Arte.
     * @return instância de ObraArte.
     */
    public ObraArte getObraArte(Integer id)
    {
        ObraArte obraarte = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM ObraArte WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                obraarte = new ObraArte();
                
                obraarte.setId(res.getInt("id"));
                obraarte.setNome(res.getString("nome"));
                obraarte.setLocal(res.getString("local_"));
                obraarte.setDimensoes(res.getString("dimensoes"));
                obraarte.setAutor(res.getString("autor"));
                obraarte.setMateriais(res.getString("materiais"));
                obraarte.setDataColocacaoUA(res.getString("dataColocacaoUA"));
                obraarte.setCoordenadas(res.getString("coordenadas"));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return obraarte;
    }
    
    /**
     * Insere uma Obra de Arte na BD.
     * @param obraarte instância de um objeto ObraArte.
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    public boolean insertObraArte(ObraArte obraarte)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "INSERT INTO ObraArte (nome, local_, dimensoes, autor, materiais, dataColocacaoUA, coordenadas)  VALUES (?, ?, ?, ?, ?, ?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, obraarte.getNome());
            pst.setString(2, obraarte.getLocal());
            pst.setString(3, obraarte.getDimensoes());
            pst.setString(4, obraarte.getAutor());
            pst.setString(5, obraarte.getMateriais());
            pst.setString(6, obraarte.getDataColocacaoUA());
            pst.setString(7, obraarte.getCoordenadas());
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Remove uma Obra de Arte na BD.
     * @param id id da Obra de Arte.
     * @return true, se o registo for removido; false, se o registo não for removido.
     */
    public boolean deleteObraArte(Integer id)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM ObraArte WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean updateObraArte (ObraArte obraarte)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "UPDATE ObraArte SET nome = ?, local_ = ?, dimensoes = ?, autor = ?, materiais = ?, dataColocacaoUA = ?, coordenadas = ? WHERE id = " + obraarte.getId() + ";";
            pst = connection.prepareStatement(sql);
            pst.setString(1, obraarte.getNome());
            pst.setString(2, obraarte.getLocal());
            pst.setString(3, obraarte.getDimensoes());
            pst.setString(4, obraarte.getAutor());
            pst.setString(5, obraarte.getMateriais());
            pst.setString(6, obraarte.getDataColocacaoUA());
            pst.setString(7, obraarte.getCoordenadas());
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            return true; 
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }      
    }
    
}
