package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.Servico;

/**
 * Data Access Object Serviços.
 */
public class ServicoDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;
    
    /**
     * Retorna uma lista de instâncias POJOS Servico, que contêm dados sobre 
     * os serviços obtidos a partir da Base de Dados
     * @return lista de instâncias de Servico.
     */
    public List<Servico> getServicos()
    {
        List<Servico> list = null;
        
        try
        {
            list = new ArrayList();
            Servico servico;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Servico;";
            pst = connection.prepareStatement(sql);
            //System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                servico = new Servico();
                
                servico.setId(res.getInt("id"));
                servico.setDesignacao(res.getString("designacao"));
                servico.setLocal(res.getString("local_"));
                servico.setCoordenadas(res.getString("coordenadas"));
                servico.setHorario(res.getString("horario"));
                        
                list.add(servico);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return list;
    }
    
    /**
     * Retorna um Serviço da BD.
     * @param id id da Obra de Arte.
     * @return instância de ObraArte.
     */
    public Servico getServico(Integer id)
    {
        Servico servico = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Servico WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                servico = new Servico();
                
                servico.setId(res.getInt("id"));
                servico.setDesignacao(res.getString("designacao"));
                servico.setLocal(res.getString("local_"));
                servico.setCoordenadas(res.getString("coordenadas"));
                servico.setHorario(res.getString("horario"));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return servico;
    }
    
    /**
     * Insere um Serviço na BD.
     * @param servico instância de um objeto Servico.
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    public boolean insertServico(Servico servico)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "INSERT INTO Servico (local_, designacao, coordenadas, horario) VALUES (?, ?, ?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, servico.getLocal());
            pst.setString(2, servico.getDesignacao());
            pst.setString(3, servico.getCoordenadas());
            pst.setString(4, servico.getHorario());
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Remove um Serviço na BD.
     * @param id id do Serviço.
     * @return true, se o registo for removido; false, se o registo não for removido.
     */
    public boolean deleteServico(Integer id)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM Servico WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean updateServico (Servico servico)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "UPDATE Servico SET local_=?, designacao=?, coordenadas=?, horario=? WHERE id = " + servico.getId() + ";";
            pst = connection.prepareStatement(sql);
            pst.setString(1, servico.getLocal());
            pst.setString(2, servico.getDesignacao());
            pst.setString(3, servico.getCoordenadas());
            pst.setString(4, servico.getHorario());
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            return true; 
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }      
    }
    
}
