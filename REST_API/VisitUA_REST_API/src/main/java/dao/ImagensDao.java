package dao;

import controller.DBConnection;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.*;
import java.util.logging.*;
import javax.imageio.ImageIO;

/**
 * Data Access Object Imagens.
 */
public class ImagensDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;
    
    public BufferedImage getObrasArteImagem(Integer id)
    {
        BufferedImage img = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT imagem FROM ObraArte WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                Blob blob = res.getBlob("imagem");  
                InputStream in = blob.getBinaryStream();  
                img = ImageIO.read(in);
            }
        }
        catch (SQLException | IOException ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return img;
    }
    
    public BufferedImage getEdificioImagem(String id)
    {
        BufferedImage img = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT imagem FROM Edificio WHERE id = '" + id + "';";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                Blob blob = res.getBlob("imagem");  
                InputStream in = blob.getBinaryStream();  
                img = ImageIO.read(in);
            }
        }
        catch (SQLException | IOException ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return img;
    }
    
    public BufferedImage getColecoesImagem(Integer id)
    {
        BufferedImage img = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT imagem FROM Colecao WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                Blob blob = res.getBlob("imagem");  
                InputStream in = blob.getBinaryStream();  
                img = ImageIO.read(in);
            }
        }
        catch (SQLException | IOException ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return img;
    }
    
    public BufferedImage getServicosImagem(Integer id)
    {
        BufferedImage img = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT imagem FROM Servico WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                Blob blob = res.getBlob("imagem");  
                InputStream in = blob.getBinaryStream();  
                img = ImageIO.read(in);
            }
        }
        catch (SQLException | IOException ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return img;
    }
    
    public boolean uploadColecaoImagem(Integer id, BufferedImage img)
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            
            connection = DBConnection.OpenConnection();
            
            Blob blob = connection.createBlob();
            blob.setBytes(1, imgBytes);
            
            sql = "UPDATE Colecao SET imagem = ? WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            pst.setBlob(1, blob);
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            
            return true; 
        }
        catch (Exception ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return false;
    }
    
    public boolean uploadObraArteImagem(Integer id, BufferedImage img)
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            
            connection = DBConnection.OpenConnection();
            
            Blob blob = connection.createBlob();
            blob.setBytes(1, imgBytes);
            
            sql = "UPDATE ObraArte SET imagem = ? WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            pst.setBlob(1, blob);
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            
            return true; 
        }
        catch (Exception ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return false;
    }
    
    public boolean uploadEdificioImagem(String id, BufferedImage img)
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            
            connection = DBConnection.OpenConnection();
            
            Blob blob = connection.createBlob();
            blob.setBytes(1, imgBytes);
            
            sql = "UPDATE Edificio SET imagem = ? WHERE id = '" + id + "';";
            pst = connection.prepareStatement(sql);
            pst.setBlob(1, blob);
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            
            return true; 
        }
        catch (Exception ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return false;
    }
    
    public boolean uploadServicoImagem(Integer id, BufferedImage img)
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            
            connection = DBConnection.OpenConnection();
            
            Blob blob = connection.createBlob();
            blob.setBytes(1, imgBytes);
            
            sql = "UPDATE Servico SET imagem = ? WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            pst.setBlob(1, blob);
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            
            return true; 
        }
        catch (Exception ex)
        {
            Logger.getLogger(ImagensDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return false;
    }
    
}
