package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.Ensino;

/**
 * Data Access Object Ensino.
 */
public class EnsinoDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;

    public List<Ensino> getEnsinos()
    {
        List<Ensino> list = null;
        
        try
        {
            list = new ArrayList();
            Ensino ensino;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Ensino;";
            pst = connection.prepareStatement(sql);
            //System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                ensino = new Ensino();
                
                ensino.setId(res.getInt("id"));
                ensino.setNome(res.getString("nome"));
                ensino.setTipo(res.getString("tipo"));
                        
                list.add(ensino);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return list;
    }
    
    public Ensino getEnsino(Integer id)
    {
        Ensino ensino = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Ensino WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                ensino = new Ensino();
                
                ensino.setId(res.getInt("id"));
                ensino.setNome(res.getString("nome"));
                ensino.setTipo(res.getString("tipo"));
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return ensino;
    }
    
    public boolean insertEnsino(Ensino ensino)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            
            // Obter o id do último registo inserido
            sql = "SELECT * FROM Ensino ORDER BY ID DESC LIMIT 1;";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            Integer id = null;
            if(res.next())
                id = res.getInt("id") + 1;
            
            sql = "INSERT INTO Ensino (id, nome, tipo) VALUES (?, ?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, id.toString());
            pst.setString(2, ensino.getNome());
            pst.setString(3, ensino.getTipo());
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException | NullPointerException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean deleteEnsino(Integer id)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM Ensino WHERE id = " + id + ";";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
}
