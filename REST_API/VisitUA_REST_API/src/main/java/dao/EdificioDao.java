package dao;

import controller.DBConnection;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.Edificio;
import model.Ensino;

/**
 * Data Access Object Edificios.
 */
public class EdificioDao {
    
    private Connection connection;
    private String sql;
    private PreparedStatement pst;
    private ResultSet res;
    
    /**
     * Retorna uma lista de instâncias POJOS Edificio, que contêm dados sobre 
     * os edificios de arte obtidos a partir da Base de Dados
     * @return lista de instâncias de Edificio.
     */
    public List<Edificio> getEdificios()
    {
        List<Edificio> list = null;
        
        try
        {
            list = new ArrayList();
            Edificio edificio;
 
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Edificio;";
            pst = connection.prepareStatement(sql);
            //System.out.println(pst.toString());
            res = pst.executeQuery();
            
            while(res.next())
            {   
                edificio = new Edificio();
                
                edificio.setId(res.getString("id"));
                edificio.setNome(res.getString("nome"));
                edificio.setTipo(res.getString("tipo"));
                edificio.setSigla(res.getString("sigla"));
                edificio.setDataConstrucao(res.getString("dataConstrucao"));
                edificio.setDescricao(res.getString("descricao"));
                edificio.setCoordenadas(res.getString("coordenadas"));
                edificio.setArquiteto(res.getString("arquiteto"));
                
                String temp_sql = "SELECT Ensino.id, Ensino.tipo, Ensino.nome FROM EdificioEnsino JOIN Ensino ON ensino = id WHERE edificio = \'" + edificio.getId() + "\';";
                PreparedStatement temp_pst = connection.prepareStatement(temp_sql);
                ResultSet temp_res = temp_pst.executeQuery();
                while(temp_res.next())
                {
                    Ensino ensino = new Ensino();
                    ensino.setId(temp_res.getInt("id"));
                    ensino.setTipo(temp_res.getString("tipo"));
                    ensino.setNome(temp_res.getString("nome"));
                    
                    edificio.setEnsino(ensino);
                }
                        
                list.add(edificio);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return list;
    }
    
    public Edificio getEdificio(String id, String nome)
    {
        Edificio edificio = null;
        
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "SELECT * FROM Edificio WHERE id = '" + id + "' and nome = '" + nome + "';";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            res = pst.executeQuery();
            
            if(res.next())
            {   
                edificio = new Edificio();
                
                edificio.setId(res.getString("id"));
                edificio.setNome(res.getString("nome"));
                edificio.setTipo(res.getString("tipo"));
                edificio.setSigla(res.getString("sigla"));
                edificio.setDataConstrucao(res.getString("dataConstrucao"));
                edificio.setDescricao(res.getString("descricao"));
                edificio.setCoordenadas(res.getString("coordenadas"));
                edificio.setArquiteto(res.getString("arquiteto"));
                
                String temp_sql = "SELECT Ensino.id, Ensino.tipo, Ensino.nome FROM EdificioEnsino JOIN Ensino ON ensino = id WHERE edificio = \'" + edificio.getId() + "\';";
                PreparedStatement temp_pst = connection.prepareStatement(temp_sql);
                ResultSet temp_res = temp_pst.executeQuery();
                while(temp_res.next())
                {
                    Ensino ensino = new Ensino();
                    ensino.setId(temp_res.getInt("id"));
                    ensino.setTipo(temp_res.getString("tipo"));
                    ensino.setNome(temp_res.getString("nome"));
                    
                    edificio.setEnsino(ensino);
                }
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DBConnection.CloseConnection();
        
        return edificio;
    }
    
    public boolean insertEdificio(Edificio edificio)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "INSERT INTO Edificio (id, nome, tipo, sigla, dataConstrucao, descricao, coordenadas, arquiteto)  VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
            
            pst = connection.prepareStatement(sql);
            pst.setString(1, edificio.getId());
            pst.setString(2, edificio.getNome());
            pst.setString(3, edificio.getTipo());
            pst.setString(4, edificio.getSigla());
            pst.setString(5, edificio.getDataConstrucao());
            pst.setString(6, edificio.getDescricao());
            pst.setString(7, edificio.getCoordenadas());
            pst.setString(8, edificio.getArquiteto());
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Remove um Serviço na BD.
     * @param id id do Serviço.
     * @return true, se o registo for removido; false, se o registo não for removido.
     */
    public boolean deleteEdificio(String id, String nome)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "DELETE FROM Edificio WHERE id = '" + id + "' and nome = '" + nome + "';";
            pst = connection.prepareStatement(sql);
            System.out.println(pst.toString());
            pst.execute();
            DBConnection.CloseConnection();
            return true;
        }
        catch(SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean updateEdificio(Edificio edificio)
    {
        try
        {
            connection = DBConnection.OpenConnection();
            sql = "UPDATE Edificio SET tipo=?, sigla=?, dataConstrucao=?, descricao=?, coordenadas=?, arquiteto=? WHERE id = '" + edificio.getId() + "' and nome = '" + edificio.getNome() + "';";
            pst = connection.prepareStatement(sql);
            pst.setString(1, edificio.getTipo());
            pst.setString(2, edificio.getSigla());
            pst.setString(3, edificio.getDataConstrucao());
            pst.setString(4, edificio.getDescricao());
            pst.setString(5, edificio.getCoordenadas());
            pst.setString(6, edificio.getArquiteto());
            System.out.println(pst.toString());
            pst.executeUpdate();
            DBConnection.CloseConnection();
            return true; 
        }
        catch (SQLException ex)
        {
            Logger.getLogger(ColecaoDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }      
    }
    
}
