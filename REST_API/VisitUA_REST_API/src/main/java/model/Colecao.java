package model;

import java.util.*;

/**
 * POJO Coleção.
 */
public class Colecao {
    
    private int id;
    private String nome;
    private String descricao;
    private String doador;
    private Integer colecaoMae;
    private List<Colecao> subColecoes;
    private String localVisita;
    private String coordenadasLocal;

    public Colecao() {
        this.subColecoes = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDoador() {
        return doador;
    }
    
    public String getLocalVisita() {
        return localVisita;
    }
    
    public String getCoordenadasLocal() {
        return coordenadasLocal;
    }

    public void setDoador(String doador) {
        this.doador = doador;
    }
    
    public Integer getColecaoMae() {
        return colecaoMae;
    }

    public void setColecaoMae(Integer colecaoMae) {
        this.colecaoMae = colecaoMae;
    }

    public List<Colecao> getSubColecoes() {
        return subColecoes;
    }

    public void setSubColecao(Colecao subColecao) {
        this.subColecoes.add(subColecao);
    }
    
    public void setLocalVisita(String localVisita) {
        this.localVisita = localVisita;
    }
    
    public void setCoordenadasLocal(String coordenadasLocal) {
        this.coordenadasLocal = coordenadasLocal;
    }
    
}
