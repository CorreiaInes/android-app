package model;

import java.util.*;

/**
 * POJO Edificio.
 */
public class Edificio {
    
    private String id;
    private String nome;
    private String tipo;
    private String sigla;
    private String dataConstrucao;
    private String descricao;
    private String coordenadas;
    private List<Ensino> ensino;
    private String arquiteto;

    public Edificio() {
        ensino = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDataConstrucao() {
        return dataConstrucao;
    }

    public void setDataConstrucao(String dataConstrucao) {
        this.dataConstrucao = dataConstrucao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public List<Ensino> getEnsino() {
        return ensino;
    }

    public void setEnsino(Ensino ensino) {
        this.ensino.add(ensino);
    }

    public String getArquiteto() {
        return arquiteto;
    }

    public void setArquiteto(String arquiteto) {
        this.arquiteto = arquiteto;
    }
    
}
