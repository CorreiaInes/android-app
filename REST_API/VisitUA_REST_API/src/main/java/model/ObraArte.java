package model;

/**
 * POJO Obra de Arte.
 */
public class ObraArte {
    
    private int id;
    private String nome;
    private String local;
    private String dimensoes;
    private String autor;
    private String materiais;
    private String dataColocacaoUA;
    private String coordenadas;

    public ObraArte() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDimensoes() {
        return dimensoes;
    }

    public void setDimensoes(String dimensoes) {
        this.dimensoes = dimensoes;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getMateriais() {
        return materiais;
    }

    public void setMateriais(String materiais) {
        this.materiais = materiais;
    }

    public String getDataColocacaoUA() {
        return dataColocacaoUA;
    }

    public void setDataColocacaoUA(String dataColocacaoUA) {
        this.dataColocacaoUA = dataColocacaoUA;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }
    
}
