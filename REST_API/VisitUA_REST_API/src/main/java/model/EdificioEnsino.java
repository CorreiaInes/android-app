package model;

/**
 * POJO Ensino por Edifício.
 */
public class EdificioEnsino {
    
    private String idEdificio;
    private Integer idEnsino;

    public String getIdEdificio() {
        return idEdificio;
    }

    public void setIdEdificio(String idEdificio) {
        this.idEdificio = idEdificio;
    }

    public Integer getIdEnsino() {
        return idEnsino;
    }

    public void setIdEnsino(Integer idEnsino) {
        this.idEnsino = idEnsino;
    }
    
}
