package controller;

import java.sql.*;
import java.util.logging.*;

/**
 * Efetua a conexão e disconexão à Base de Dados.
 */
public class DBConnection {
    
    // Localmente
    /*private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private static final String HOST = "localhost";
    private static final String PORT = "3306";*/
    
    // Virtual Machine
    private static final String USERNAME = "deti-visitua";
    private static final String PASSWORD = "hw2EeNUstbEo1uT3";
    private static final String HOST = "deti-visitua.ua.pt";
    private static final String PORT = "3306";
    
    private static final String DATABASE = "VisitUA";
    private static final String CLASSNAME = "com.mysql.cj.jdbc.Driver";
    private static final String URL = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DATABASE + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    private static Connection connection;

    public DBConnection()
    {
    
    }
    
    public static Connection OpenConnection()
    {
        try
        {
            Class.forName(CLASSNAME);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            
            System.out.println("Successfully connected to: " + URL);
        }
        catch (ClassNotFoundException | SQLException ex)
        {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }
    
    public static void CloseConnection()
    {
        try
        {
            connection.close();
            
            System.out.println("Successfully disconnected from: " + URL);
        }
        catch (SQLException ex)
        {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
    
}
