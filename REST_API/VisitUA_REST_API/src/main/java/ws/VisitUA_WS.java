package ws;

import com.google.gson.Gson;
import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import dao.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.logging.*;
import javax.imageio.ImageIO;
import model.*;
import org.apache.commons.codec.binary.Base64;

/**
 * REST API.
 */
@Path("visitua")
public class VisitUA_WS {

    /**
     * Creates a new instance of VisitUA_WS
     */
    public VisitUA_WS()
    {
        
    }

    /**
     * Retrieves representation of an instance of ws.VisitUA_WS
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getText() {
        return "VisitUA RESTful Web Service.";
    }
    
    /*
        GET METHODS
    */
    
    // COLEÇÕES
    /**
     * Retorna a imagem correspondente à Coleção com um determinado id.
     * @param id id da Coleção.
     * @return dados em formato JPG.
     */
    @GET
    @Produces("image/jpg")
    @Path("colecoes/imagem/{id}")
    public Response getColecoesImagem(@PathParam("id") Integer id)
    {
        byte[] imgData = null;
        
        ImagensDao imgdao = new ImagensDao();
        
        try
        {
            BufferedImage img = imgdao.getColecoesImagem(id);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "png", baos);
            imgData = baos.toByteArray();
            return Response.ok(new ByteArrayInputStream(imgData)).build();
        }
        catch (Exception ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * Retorna todas as Coleções.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("colecoes")
    public String getColecoes()
    {
        Gson gson = new Gson();
        
        ColecaoDao colecaodao = new ColecaoDao();
        List<Colecao> list = colecaodao.getColecoes();
        
        return gson.toJson(list);
    }
    
    /**
     * Retorna uma Coleção.
     * @param id id da coleção.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("colecoes/{id}")
    public String getColecoes(@PathParam("id") Integer id)
    {
        Gson gson = new Gson();
        
        ColecaoDao colecaodao = new ColecaoDao();
        Colecao colecao = colecaodao.getColecao(id);
        
        return gson.toJson(colecao);
    }
    
    /**
     * Remove uma Coleção.
     * @param id id da coleção.
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/colecoes/{id}")
    public boolean deleteColecoes(@PathParam("id") Integer id)
    {
        ColecaoDao colecaodao = new ColecaoDao();   
        return colecaodao.deleteColecao(id);
    }
    
    // OBRAS DE ARTE
    /**
     * Retorna a imagem correspondente à Obra de Arte com um determinado id.
     * @param id id da Obra de Arte.
     * @return dados em formato JPG.
     */
    @GET
    @Produces("image/jpg")
    @Path("obrasarte/imagem/{id}")
    public Response getObrasArteImagem(@PathParam("id") Integer id)
    {
        byte[] imgData = null;
        
        ImagensDao imgdao = new ImagensDao();
        
        try
        {
            BufferedImage img = imgdao.getObrasArteImagem(id);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "png", baos);
            imgData = baos.toByteArray();
            return Response.ok(new ByteArrayInputStream(imgData)).build();
        }
        catch (Exception ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * Retorna todas as Obras de Arte.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("obrasarte")
    public String getObrasArte()
    {
        Gson gson = new Gson();
        
        ObraArteDao artedao = new ObraArteDao();
        List<ObraArte> list = artedao.getObrasArte();
        
        return gson.toJson(list);
    }
    
    /**
     * Retorna uma Obra de Arte.
     * @param id id da obra de arte.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("obrasarte/{id}")
    public String getObraArte(@PathParam("id") Integer id)
    {
        Gson gson = new Gson();
        
        ObraArteDao artedao = new ObraArteDao();
        ObraArte obraarte = artedao.getObraArte(id);
        
        return gson.toJson(obraarte);
    }
    
    /**
     * Remove uma Obra de Arte.
     * @param id id da Obra de Arte.
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/obrasarte/{id}")
    public boolean deleteObrasArte(@PathParam("id") Integer id)
    {
        ObraArteDao artedao = new ObraArteDao();   
        return artedao.deleteObraArte(id);
    }
    
    // SERVIÇOS
    /**
     * Retorna a imagem correspondente à Coleção com um determinado id.
     * @param id id da Coleção.
     * @return dados em formato JPG.
     */
    @GET
    @Produces("image/jpg")
    @Path("servicos/imagem/{id}")
    public Response getServicosImagem(@PathParam("id") Integer id)
    {
        byte[] imgData = null;
        
        ImagensDao imgdao = new ImagensDao();
        
        try
        {
            BufferedImage img = imgdao.getServicosImagem(id);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "png", baos);
            imgData = baos.toByteArray();
            return Response.ok(new ByteArrayInputStream(imgData)).build();
        }
        catch (Exception ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * Retorna todos os Serviços.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("servicos")
    public String getServicos()
    {
        Gson gson = new Gson();
        
        ServicoDao servicodao = new ServicoDao();
        List<Servico> list = servicodao.getServicos();
        
        return gson.toJson(list);
    }
    
    /**
     * Retorna um Serviço.
     * @param id id do serviço.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("servicos/{id}")
    public String getServico(@PathParam("id") Integer id)
    {
        Gson gson = new Gson();
        
        ServicoDao servicodao = new ServicoDao();
        Servico servico = servicodao.getServico(id);
        
        return gson.toJson(servico);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/servicos/{id}")
    public boolean deleteServico(@PathParam("id") Integer id)
    {
        ServicoDao servicodao = new ServicoDao();   
        return servicodao.deleteServico(id);
    }
    
    // EDIFICIOS
    /**
     * Retorna a imagem correspondente ao Edifício com um determinado id.
     * @param id id da Edifício.
     * @return dados em formato JPG.
     */
    @GET
    @Produces("image/jpg")
    @Path("edificios/imagem/{id}")
    public Response getEdificiosImagem(@PathParam("id") String id)
    {
        byte[] imgData = null;
        
        ImagensDao imgdao = new ImagensDao();
        
        try
        {
            BufferedImage img = imgdao.getEdificioImagem(id);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "png", baos);
            imgData = baos.toByteArray();
            return Response.ok(new ByteArrayInputStream(imgData)).build();
        }
        catch (Exception ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * Retorna todos os Edifícios.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("edificios")
    public String getEdificios()
    {
        Gson gson = new Gson();
        
        EdificioDao edificiodao = new EdificioDao();
        List<Edificio> list = edificiodao.getEdificios();
        
        return gson.toJson(list);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("edificios/{id}/{nome}")
    public String getEdificio(@PathParam("id") String id, @PathParam("nome") String nome)
    {
        Gson gson = new Gson();
        
        EdificioDao edificiodao = new EdificioDao();
        Edificio edificio = edificiodao.getEdificio(id, nome);
        
        return gson.toJson(edificio);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/edificios/{id}/{nome}")
    public boolean deleteEdificio(@PathParam("id") String id, @PathParam("nome") String nome)
    {
        EdificioDao edificiodao = new EdificioDao();   
        return edificiodao.deleteEdificio(id, nome);
    }
    
    // REFEITÓRIOS
    /**
     * Retorna todos os Refeitórios.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("refeitorios")
    public String getRefeitorios()
    {
        Gson gson = new Gson();
        
        RefeitorioDao refeitoriodao = new RefeitorioDao();
        List<Refeitorio> list = refeitoriodao.getRefeitorios();
        
        return gson.toJson(list);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("refeitorios/{id}")
    public String getRefeitorio(@PathParam("id") Integer id)
    {
        Gson gson = new Gson();
        
        RefeitorioDao refeitoriodao = new RefeitorioDao();
        Refeitorio refeitorio = refeitoriodao.getRefeitorio(id);
        
        return gson.toJson(refeitorio);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/refeitorios/{id}")
    public boolean deleteRefeitorio(@PathParam("id") String id)
    {
        RefeitorioDao refeitoriodao = new RefeitorioDao(); 
        return refeitoriodao.deleteRefeitorio(id);
    }
    
    
    // PARQUES PÚBLICOS
    /**
     * Retorna todos os Refeitórios.
     * @return dados em formato JSON.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("parquespublicos")
    public String getParquesPublicos()
    {
        Gson gson = new Gson();
        
        ParquePublicoDao parquepublicosdao = new ParquePublicoDao();
        List<ParquePublico> list = parquepublicosdao.getParquesPulbicos();
        
        return gson.toJson(list);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("parquespublicos/{id}")
    public String getParquePublico(@PathParam("id") Integer id)
    {
        Gson gson = new Gson();
        
        ParquePublicoDao parquepublicosdao = new ParquePublicoDao();
        ParquePublico parquepublico = parquepublicosdao.getParquePublico(id);
        
        return gson.toJson(parquepublico);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/parquespublicos/{id}")
    public boolean deleteParquePublico(@PathParam("id") String id)
    {
        ParquePublicoDao parquepublicosdao = new ParquePublicoDao();
        return parquepublicosdao.deleteParquePublico(id);
    }
    
    // ENSINO
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("ensinos")
    public String getEnsinos()
    {
        Gson gson = new Gson();
        
        EnsinoDao ensinodao = new EnsinoDao();
        List<Ensino> list = ensinodao.getEnsinos();
        
        return gson.toJson(list);
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("ensinos/{id}")
    public String getEnsino(@PathParam("id") Integer id)
    {
        Gson gson = new Gson();
        
        EnsinoDao ensinodao = new EnsinoDao();
        Ensino ensino = ensinodao.getEnsino(id);
        
        return gson.toJson(ensino);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/ensinos/{id}")
    public boolean deleteEnsino(@PathParam("id") Integer id)
    {
        EnsinoDao ensinodao = new EnsinoDao();
        return ensinodao.deleteEnsino(id);
    }
    
    // ENSINO POR EDIFÍCIO
    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Path("edificiosensinos")
    public String getEdificiosEnsinos()
    {
        Gson gson = new Gson();
        
        EdificioEnsinoDao edificioensinodao = new EdificioEnsinoDao();
        List<EdificioEnsino> list = edificioensinodao.getEdificioEnsinos();
        
        return gson.toJson(list);
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("delete/edificiosensinos/{idEdificio}/{idEnsino}")
    public boolean deleteEdificioEnsino(@PathParam("idEdificio") String idEdificio, @PathParam("idEnsino") Integer idEnsino)
    {
        EdificioEnsinoDao edificioensinodao = new EdificioEnsinoDao();
        return edificioensinodao.deleteEnsino(idEdificio, idEnsino);
    }
    
    /*
        POST METHODS
    */
    
    // COLEÇÕES
    /**
     * Insere uma nova Coleção na Base de Dados.
     * @param data String JSON com os dados a inserir
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/colecoes")
    public boolean putColecao(String data)
    {
        Gson gson = new Gson();
        
        Colecao colecao = (Colecao) gson.fromJson(data, Colecao.class);
        ColecaoDao colecaodao = new ColecaoDao();
        
        return colecaodao.insertColecao(colecao);
    }
    
    /**
     * Atualiza uma Coleção com uma imagem.
     * @param id da Coleção.
     * @param imgData String com a imagem codificada em Base64.
     * @return true, se o registo for atualizado; false, se o registo não for atualizado.
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/imagem/colecoes/{id}")
    public boolean uploadColecaoImagem(@PathParam("id") Integer id, String imgData)
    {
        try
        {
            byte[] byteArray = Base64.decodeBase64(imgData);
            ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
            BufferedImage img = ImageIO.read(bis);
            bis.close();
            
            ImagensDao imgdao = new ImagensDao();
            
            return imgdao.uploadColecaoImagem(id, img);
        }
        catch (IOException ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    /**
     * Atualiza uma Coleção.
     * @param data String JSON com os dados a inserir
     * @return true, se o registo for atualizado; false, se o registo não for atualizado.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/colecoes")
    public boolean updateColecoes(String data)
    {
        Gson gson = new Gson();
        
        Colecao colecao = (Colecao) gson.fromJson(data, Colecao.class);
        ColecaoDao colecaodao = new ColecaoDao();
        
        return colecaodao.updateColecao(colecao);
    }
    
    // OBRAS DE ARTE
    /**
     * Atualiza uma Obra de Arte com uma imagem.
     * @param id da Obra de Arte.
     * @param imgData String com a imagem codificada em Base64.
     * @return true, se o registo for atualizado; false, se o registo não for atualizado.
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/imagem/obrasarte/{id}")
    public boolean uploadObraArteImagem(@PathParam("id") Integer id, String imgData)
    {
        try
        {
            byte[] byteArray = Base64.decodeBase64(imgData);
            ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
            BufferedImage img = ImageIO.read(bis);
            bis.close();
            
            ImagensDao imgdao = new ImagensDao();
            
            return imgdao.uploadObraArteImagem(id, img);
        }
        catch (IOException ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    /**
     * Insere uma nova Obra de Arte na Base de Dados.
     * @param data String JSON com os dados a inserir
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/obrasarte")
    public boolean putObraArte(String data)
    {
        Gson gson = new Gson();
        
        ObraArte obraarte = (ObraArte) gson.fromJson(data, ObraArte.class);
        ObraArteDao artedao = new ObraArteDao();
        
        return artedao.insertObraArte(obraarte);
    }
    
    /**
     * Atualiza uma Obra de Arte.
     * @param data String JSON com os dados a inserir
     * @return true, se o registo for atualizado; false, se o registo não for atualizado.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/obrasarte")
    public boolean updateObraArte(String data)
    {
        Gson gson = new Gson();
        
        ObraArte obraarte = (ObraArte) gson.fromJson(data, ObraArte.class);
        ObraArteDao artedao = new ObraArteDao();
        
        return artedao.updateObraArte(obraarte);
    }
    
    // SERVIÇOS
    /**
     * Insere um novo Serviço na Base de Dados.
     * @param data String JSON com os dados a inserir
     * @return true, se o registo for inserido; false, se o registo não for inserido.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/servicos")
    public boolean putServico(String data)
    {
        Gson gson = new Gson();
        
        Servico servico = (Servico) gson.fromJson(data, Servico.class);
        ServicoDao servicodao = new ServicoDao();
        
        return servicodao.insertServico(servico);
    }
    
    /**
     * Atualiza uma Serviço com uma imagem.
     * @param id da Serviço.
     * @param imgData String com a imagem codificada em Base64.
     * @return true, se o registo for atualizado; false, se o registo não for atualizado.
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/imagem/servicos/{id}")
    public boolean uploadServicoImagem(@PathParam("id") Integer id, String imgData)
    {
        try
        {
            byte[] byteArray = Base64.decodeBase64(imgData);
            ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
            BufferedImage img = ImageIO.read(bis);
            bis.close();
            
            ImagensDao imgdao = new ImagensDao();
            
            return imgdao.uploadServicoImagem(id, img);
        }
        catch (IOException ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/servicos")
    public boolean updateServico(String data)
    {
        Gson gson = new Gson();
        
        Servico servico = (Servico) gson.fromJson(data, Servico.class);
        ServicoDao servicodao = new ServicoDao();
        
        return servicodao.updateServico(servico);
    }
    
    // EDIFÍCIOS
    /**
     * Atualiza um Edifício com uma imagem.
     * @param id da Edifício.
     * @param imgData String com a imagem codificada em Base64.
     * @return true, se o registo for atualizado; false, se o registo não for atualizado.
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/imagem/edificios/{id}")
    public boolean uploadEdificiosImagem(@PathParam("id") String id, String imgData)
    {
        try
        {
            byte[] byteArray = Base64.decodeBase64(imgData);
            ByteArrayInputStream bis = new ByteArrayInputStream(byteArray);
            BufferedImage img = ImageIO.read(bis);
            bis.close();
            
            ImagensDao imgdao = new ImagensDao();
            
            return imgdao.uploadEdificioImagem(id, img);
        }
        catch (IOException ex)
        {
            Logger.getLogger(VisitUA_WS.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/edificios")
    public boolean putEdificio(String data)
    {
        Gson gson = new Gson();
        
        Edificio edificio = (Edificio) gson.fromJson(data, Edificio.class);
        EdificioDao edificiodao = new EdificioDao();
        
        return edificiodao.insertEdificio(edificio);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/edificios")
    public boolean updateEdifico(String data)
    {
        Gson gson = new Gson();
        
        Edificio edificio = (Edificio) gson.fromJson(data, Edificio.class);
        EdificioDao edificiodao = new EdificioDao();
        
        return edificiodao.updateEdificio(edificio);
    }
    
    // REFEITÓRIOS
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/refeitorios")
    public boolean putRefeitorio(String data)
    {
        Gson gson = new Gson();
        
        Refeitorio refeitorio = (Refeitorio) gson.fromJson(data, Refeitorio.class);
        RefeitorioDao refeitoriodao = new RefeitorioDao();
        
        return refeitoriodao.insertRefeitorio(refeitorio);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/refeitorios")
    public boolean updateRefeitorio(String data)
    {
        Gson gson = new Gson();
        
        Refeitorio refeitorio = (Refeitorio) gson.fromJson(data, Refeitorio.class);
        RefeitorioDao refeitoriodao = new RefeitorioDao();
        
        return refeitoriodao.updateEdificio(refeitorio);
    }
    
    // PARQUES PÚBLICOS
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/parquespublicos")
    public boolean putParquePublico(String data)
    {
        Gson gson = new Gson();
        
        ParquePublico parquepublico = (ParquePublico) gson.fromJson(data, ParquePublico.class);
        ParquePublicoDao parquepublicodao = new ParquePublicoDao();
        
        return parquepublicodao.insertParquePublico(parquepublico);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("update/parquespublicos")
    public boolean updateParquePublico(String data)
    {
        Gson gson = new Gson();
        
        ParquePublico parquepublico = (ParquePublico) gson.fromJson(data, ParquePublico.class);
        ParquePublicoDao parquepublicodao = new ParquePublicoDao();
        
        return parquepublicodao.updateParquePublico(parquepublico);
    }
    
    // ENSINOS
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/ensinos")
    public boolean putEnsino(String data)
    {
        Gson gson = new Gson();
        
        Ensino ensino = (Ensino) gson.fromJson(data, Ensino.class);
        EnsinoDao ensinodao = new EnsinoDao();
        
        return ensinodao.insertEnsino(ensino);
    }
    
    // ENSINO POR EDIFÍCIO
    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("insert/edificiosensinos")
    public boolean putEdificioEnsino(String data)
    {
        Gson gson = new Gson();
        
        EdificioEnsino edificioensino = (EdificioEnsino) gson.fromJson(data, EdificioEnsino.class);
        EdificioEnsinoDao edificioensinodao = new EdificioEnsinoDao();
        
        return edificioensinodao.insertEnsino(edificioensino);
    }
    
}
