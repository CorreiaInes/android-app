-- Inserts na Base de Dados MySQL dos dados sobre as Coleções, Obras de Arte Públicas, serviços e Edifícios da UA.


-- COLEÇÕES

INSERT INTO Colecao (nome, descricao, doador, colecaoMae, localVisita, coordenadasLocal) 
	VALUES 
		(
			N'Cartazes', 
			NULL, 
			NULL, 
			NULL,
			NULL,
			NULL
		), 
		(
			N'Cartazes Madeira Luís',
			N'Estes cartazes datam entre os anos 1960 a 2000 e são exclusivamente portugueses. Estes abrangem diversas categorias como cartazes políticos, comerciais, de concertos, de congressos, desporto, de cinema, de circo, de teatro, de educação, património, etc. A maioria dos documentos desta coleção são cartazes políticos, relativos ao 25 de abril, às variadas eleições, manifestações e greves. Pode encontrar mais informações desta coleção em: http://sinbad.ua.pt/.',
			N'Francisco Madeira Luís', 
			1,
			N'Estes cartazes encontram-se digitalizados e registados na plataforma SiNBAD, proporcionando a sua visualização pública. Estes estão armazenados na reserva museológica.',
			N'40.631198, -8.659716'
		),
		(
			N'Coleção Universidade de Aveiro',
			N'A coleção “Universidade de Aveiro” inclui os cartazes produzidos pelas institucionais da UA e as suas diversas unidades. Datam desde dos anos 1980 até ao presente. Dado que a recolha destes cartazes não é exaustiva, haverá exemplares em falta.', 
			NULL, 
			1,
			N'Estes cartazes encontram-se digitalizados e registados na plataforma SiNBAD, proporcionando a sua visualização pública. Alguns destes estão expostos na Biblioteca da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Jazz',
			N'A coleção “Jazz” reúne cartazes da temática Jazz e são maioritariamente portugueses.',
			N'José Duarte', 
			1,
			N'Estes cartazes encontram-se digitalizados e registados na plataforma SiNBAD, proporcionando a sua visualização pública. Estes estão armazenados na reserva museológica.',
			N'40.631198, -8.659716'
		),
		(
			N'Cerâmica arqueológica',
			N'Coleção constituída por diversas peças cerâmicas, sendo grande parte proveniente das olarias de Aveiro, que datam desde o séc. XV ao séc. XVII. Estas últimas são essencialmente peças de cerâmica de uso comum, de pastas vermelhas, algumas com decoração brunida vertical ou cruzada. Pertence a esta coleção também um conjunto de materiais arqueológicos provenientes da intervenção arqueológica realizada na Agra do Crasto, que segundo a natureza dos vestígios detetados, datam do 2o milénio a.C. (Idade do Bronze) ou mesmo de meados do 3o milénio, considerando a ocupação calcolítica.', 
			NULL, 
			NULL,
			N'Esta coleção está armazenada na reserva museológica da Universidade de Aveiro',
			'40.631198, -8.659716'
		),
		(
			N'Cerâmica contemporânea', 
			N'Coleção composta por cerca de 3000 objetos provenientes de diversas unidades industriais do país (Sacavém; Vista Alegre; Coimbra, Lisboa, Sacavém, Caldas da Rainha) e abordam o período da segunda metade do século XIX e primeira metade do século XX. A coleção é composta essencialmente por cerâmica de equipamento, grande parte objetos utilitários que dão resposta às necessidades quotidianas (louças para alimentos e refeições), peças decorativas, moldes e também alguns objetos de cerâmica de revestimento.', 
			N'Francisco Madeira Luís', 
			NULL,
			N'Esta coleção está maioritariamente armazenada na reserva museológica da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Discos', 
			NULL, 
			NULL, 
			NULL,
			NULL,
			NULL
		),
		(
			N'Discos Goma-laca 78rpm de Música Portuguesa',
			N'Trata-se de uma vasta coleção constituída por cerca de 6000 discos (incluindo inúmeros duplicados). A maioria são gravações de fado, mas também predominam temas de música tradicional Portuguesa, música para teatro de revista, monólogos, gravações referentes à implantação da República em Portugal, ópera, entre outros géneros. As datas das edições remontam ao início do século XX, indo até meados deste século. Os discos mais antigos desta coleção datam mesmo de 1900.',
			N'José Moças', 
			7,
			N'Mediateca - Estúdio de Som',
			N'40.631821, -8.658801'
		),
		(
			N'Discos Goma-laca de Jazz',
			N'Composta por cerca de 600 discos. Ainda se encontram em fase de tratamento, pelo que se mantêm dentro dos álbuns criados pelo colecionador.', 
			NULL, 
			7,
			N'Mediateca - Estúdio de Som',
			N'40.631821, -8.658801'
		),
		(
			N'Discos Goma-laca de Música Diversa',
			N'Composta por cerca de 600 discos. Ainda se encontram em fase de tratamento, pelo que se mantêm dentro dos álbuns criados pelo colecionador.', 
			NULL, 
			7,
			N'Mediateca - Estúdio de Som',
			N'40.631821, -8.658801'
		),
		(
			N'Discos Vinil LP de Jazz',
			N'Composta por mais de 2000 discos.',
			N'José Duarte', 
			7,
			N'Mediateca - Estúdio de Som',
			N'40.631821, -8.658801'
		),
		(	N'Dicos Vinil LP de Música Clássica',
			N'Reúne cerca de 2500 discos.',
			N'Manuel Dias da Fonseca e Prof. João Pedro Oliveira', 
			7,
			N'Mediateca - Estúdio de Som',
			N'40.631821, -8.658801'
		),
		(
			N'Gravura', 
			NULL, 
			NULL, 
			NULL,
			NULL,
			NULL
		),
		(
			N'Sociedade Cooperativa de Gravadores Portugueses',
			N'Coleção de 257 gravuras da Sociedade Cooperativa de Gravadores Portugueses (SCGP), pioneira no fomento e reabilitação da arte da gravura em Portugal no séc. XX. Inclui artistas portugueses bem conhecidos como Júlio Pomar, Almada Negreiros e Bartolomeu Cid dos Santos.',
			N'Francisco Madeira Luís', 
			13,
			N'Biblioteca da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Hélène de Beauvoir',
			N'Constituída por 7 gravuras.', 
			NULL, 
			13,
			N'Biblioteca da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Outras Gravuras',
			N'39 gravuras de vários autores, entre os quais Júlio Resende e Cândido Teles.', 
			NULL, 
			13,
			N'Biblioteca da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Guarda-jóias de madeira',
			N'Trata-se de uma coleção de 90 pequenas caixas de madeira, de formatos diversos e construídas com diferentes tipos de madeira, havendo algumas com embutidos de metal e madrepérola. Foram construídas e doadas pelo Engo Joaquim Capela e encontram-se em vitrines na Biblioteca.',
			N'Engº Joaquim Capela', 
			NULL,
			N'Vitrines da Biblioteca da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Instrumentos Musicais',
			N'A coleção de instrumentos musicais foi doada à Universidade de Aveiro em dezembro de 2013 pelo "violeiro" Joaquim Domingos Capela. Da sua coleção pessoal, que perfaz um conjunto de 135 instrumentos, doou à UA 23 peças. Neste conjunto encontra-se um violino de 1⁄4 que foi o primeiro instrumento musical que construiu apenas com nove anos de idade.',
			N'Joaquim Domingos Capela', 
			NULL,
			N'Cofre da Biblioteca da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Pintura', 
			NULL, 
			NULL, 
			NULL,
			NULL,
			NULL
		),
		(
			N'Hélène de Beauvoir',
			N'Reúne 66 pinturas a óleo e 5 aguarelas da autora e foi doada à Universidade de Aveiro em 1995.', 
			NULL, 
			19,
			N'Biblioteca da Universidade de Aveiro',
			N'40.631198, -8.659716'
		),
		(
			N'Universidade de Aveiro',
			N'Constituída por 86 obras de pintura de vários artistas, entre eles, Cândido Teles, Artur Fino, Hélder Bandarra, Mário Silva, Jeremias Bandarra, Zé Penicheiro, Artur Bual, Cruzeiro Seixas, Júlio Resende e Tereza Trigalhos.', 
			NULL, 
			19,
			N'Reitoria da Universidade de Aveiro',
			N'40.631363, -8.657402'
		),
		(
			N'Vidros',
			N'A coleção de vidros foi doada à UA em 2001 e é constituída por mais de 5 mil peças (incluindo duplicados). São peças de vidro vulgar de fabrico português e de natureza utilitária. A coleção contém ainda algumas peças de luxo. O âmbito temporal abrange a segunda metade do séc. XIX e a primeira metade do séc. XX, havendo no entanto alguns objetos dos finais do séc. XVIII e princípios do séc. XIX, assim como outros posteriores a 1950.',
			N'Francisco Madeira Luís', 
			NULL,
			N'Reserva museológica',
			N'40.631198, -8.659716'
		),
		(
			N'Escultura',
			N'A Universidade de Aveiro possui uma coleção de 36 esculturas de vários autores. Na sua maioria, são peças de grande dimensão e estão distribuídas pelo campus. Deste conjunto, 18 esculturas encontram-se ao ar livre, no Campus, estando as restantes dentro de edifícios da Reitoria, Serviços e Departamentos.', 
			NULL, 
			NULL,
			N'Estas esculturas encontram-se espalhadas pelo campus. Para mais informações consulte a página sobre Arte Pública na secção do Museu na aplicação.',
			NULL
		)
		;


		
-- OBRAS DE ARTE
INSERT INTO ObraArte (nome, local_, dimensoes, autor, materiais, dataColocacaoUA, coordenadas) 
	VALUES 
		(
			N'Animal Alma', 
			N'Jardim do Espelho de Água entre os edifícios DMAC e CESAM', 
			N'335x216x108cm', 
			N'Manuel Patinha', 
			N'Aço galvanizado', 
			N'2000', 
			N'40.6345683, -8.6578643'
		),
		(
			N'Estátuas de Pedra',
			N'Jardim junto à entrada do edifício da Reitoria.', 
			NULL,
			N'Paulo Neves',
			N'Calcário',
			N'2001',
			N'40.631398, -8.657646'
		),
		(
			N'Calandra',
			N'Átrio do edifício da Biblioteca da Universidade.',
			N'80x175x80cm',
			N'Manuel Patinha',
			N'Aço galvanizado',
			N'2000',
			N'40.631097, -8.659648'
		),
		(
			N'Painel Timor',
			N'Escadaria de entrada do edifício Departamento de Educação e Psicologia.', 
			NULL,
			N'Crianças das escolas do pré-primário e 1.º ciclo, da Região de Aveiro',
			N'Azulejos e tintas para azulejo', 
			NULL,
			N'40.632012, -8.658139'
		),
		(
			N'Voar mais alto',
			N'Muro lateral do CIFOP em frente à entrada do edifício da Reitoria.',
			N'15,6x5,5m',
			N'Zé Penicheiro',
			N'Painel em cerâmica',
			N'2004',
			N'40.631652, -8.658299'
		),
		(
			N'Places Under Sky',
			N'Jardim lateral junto ao edifício da Reitoria.',
			N'500x740x890cm',
			N'Panaite Chifu',
			N'Madeira',
			N'2004',
			N'40.631532, -8.656975'
		),
		(
			N'Un Sueño Soñado en la Escalera',
			N'Entrada do edifício do Complexo Pedagógico.',
			N'636x138x15cm',
			N'Paco Pestana',
			N'Madeira',
			N'2004',
			N'40.629555, -8.655771'
		),
		(
			N'Os Anjos também têm Asas',
			N'Entrada do edifício do Complexo de Cantinas do Crasto.',
			N'Peça pequena 360x75x75; Peça grande 490x75x80cm',
			N'Paulo Neves',
			N'Madeira',
			N'2004',
			N'40.624489, -8.656837'
		),
		(
			N'Tensão Bio-Gravítica III',
			N'Fachada Este do edifício do Complexo Pedagógico.',
			N'390x90x27cm',
			N'Xico Lucena',
			N'Granito',
			N'2009',
			N'40.629852, -8.655115'
		),
		(
			N'Onírica Serenidade na Esperança…',
			N'Fachada Principal do edifício da Reitoria.',
			N'95x210x85cm',
			N'Xico Lucena',
			N'Granito',
			N'2009',
			N'40.631240, -8.657724'
		),
		(
			N'Grande Enlace',
			N'Átrio de entrada do edifício da Reitoria.', 
			NULL,
			N'Manuel Patinha',
			N'Aço e Vidro Espelhado', 
			NULL,
			N'40.631403, -8.657424'
		),
		(
			N'Título desconhecido',
			N'Junto ao jardim lateral do edifício do Departamento de Matemática.',
			N'230x190x120cm',
			N'Volker Schnüttgen',
			N'Mármore de Estremoz',
			N'2010',
			N'40.630152, -8.658905'
		),
		(
			N'Título desconhecido',
			N'No jardim entre o edifício do Complexo Pedagógico e o edifício do Laboratório Central de Análises.',
			N'120x220x180cm',
			N'Isabel Pinheiro',
			N'Mármore de Estremoz',
			N'2010',
			N'40.629200, -8.655676'
		),
		(
			N'Título desconhecido',
			N'Na Alameda Central entre o edifício do Departamento de Geociências e o edifício do Departamento de Química.',
			N'270x150x120cm',
			N'Paulo Neves',
			N'Mármore de Estremoz',
			N'2010',
			N'40.6297212, -8.6566234'
		),
		(
			N'1995.Sapo',
			N'No início do corredor lateral do edifício da Reitoria.',
			N'270x83x36cm',
			N'Paulo Neves',
			N'Mármore',
			N'22 de Outubro de 2014',
			N'40.6310385, -8.6580765'
		),
		(
			N'Título desconhecido',
			N'Hall da entrada principal do Departamento de Geociências.',
			N'290x730cm',
			N'Francisco Laranjo',
			N'Barro (materiais e produtos cerâmicos)',
			N'1995',
			N' 40.629653, -8.656982'
		),
		(
			N'Título desconhecido',
			N'Hall de entrada do Departamento de Ciências Sociais, Políticas e do Território.',
			N'730x163cm',
			N'Milú Sardinha',
			N'Barro (materiais e produtos cerâmicos)',
			N'1992',
			N'40.629904, -8.658232'
		),
		(
			N'Título desconhecido',
			N'Hall de entrada do Departamento de Ambiente e Ordenamento.',
			N'60x152cm',
			N'Zé Augusto',
			N'Barro (materiais e produtos cerâmicos)',
			N'1988',
			N'40.632680, -8.659042'
		),
		(
			N'Depósito de Água',
			N'Junto à entrada do Complexo Pedagógico, Científico e Tecnológico.', 
			NULL,
			N'Álvaro Siza Vieira', 
			NULL,
			N'1989',
			N'40.629037, -8.655398'
		)
		;



-- SERVIÇOS
INSERT INTO Servico (local_, designacao, coordenadas, horario) 
	VALUES 
		(
			N'Piso 0 do edifício sede dos SASUA.',
			N'LojaUA e Papelaria',
			N'40.630709, -8.659129',
			N'09:00h-17:30h (Seg. a Sex.)'
		),
		(
			N'Piso 0 do edifício sede dos SASUA.',
			N'Livraria',
			N'40.631458, -8.658950',
			N'09:00h - 17:30h (Seg. a Sex.)'
		),
		(
			N'Piso 0 do edifício sede dos SASUA.',
			N'Parafarmácia Unifarma',
			N'40.630709, -8.659129',
			N'10:00h-13:30h, 14:00h-17:00h (Seg. a Sex.)'
		),
		(
			N'Piso 0 do edifício sede dos SASUA.',
			N'Ótica Ergovisão',
			N'40.630730, -8.659088', 
			NULL
		),
		(
			N'Piso 0 do edifício sede dos SASUA.',
			N'Espaço da Bicicleta',
			N'40.630608, -8.658855', 
			NULL
		),
		(
			N'Piso 0 do edifício sede dos SASUA.',
			N'Caixa Geral de Depósitos',
			N'40.631069, -8.659337',
			N'09:30–12:00, 13:00–15:30 (Seg. a Sex.)'
		)
		;



-- ENSINO
INSERT INTO Ensino (id, tipo, nome) 
	VALUES 
		(
			1,
			N'Licenciatura',
			N'Biologia'
		),
		(	
			2,
			N'Licenciatura',
			N'Biologia e Geologia'
		),
		(	
			3,
			N'Mestrado',
			N'Biologia Aplicada'
		),
		(	
			4,
			N'Mestrado',
			N'Biologia Marinha'
		),
		(	
			5,
			N'Mestrado',
			N'Biologia Molecular e Celular'
		),
		(	
			6,
			N'Mestrado',
			N'Ecologia Aplicada'
		),
		(	
			7,
			N'Mestrado',
			N'Microbiologia'
		),
		(	
			8,
			N'Mestrado',
			N'Toxicologia e Ecotoxicologia'
		),
		(	
			9,
			N'Doutoramento',
			N'Biologia'
		),
		(	
			10,
			N'Doutoramentos',
			N'Biologia e Ecologia das Alterações Globais'
		),
		(	
			11,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia do Ambiente'
		),
		(	
			12,
			N'Mestrado',
			N'Sistemas Energéticos Sustentáveis'
		),
		(	
			13,
			N'Mestrado',
			N'Gestão e Políticas Ambientais'
		),
		(	
			14,
			N'Mestrado',
			N'Estudos Ambientais'
		),
		(	
			15,
			N'Doutoramento',
			N'Ciências e Engenharia do Ambiente'
		),
		(	
			16,
			N'Doutoramento',
			N'Sistemas Energéticos e Alterações Climáticas'
		),
		(	
			17,
			N'Doutoramento',
			N'Território, Riscos e Políticas Públicas'
		),
		(	
			18,
			N'Licenciatura',
			N'Ciências Biomédicas'
		),
		(	
			19,
			N'Mestrado',
			N'Biomedecina Molecular'
		),
		(	
			20,
			N'Mestrado',
			N'Gestão da Investigação Clínica'
		),
		(	
			21,
			N'Mestrado',
			N'Tecnologias da Imagem Médica'
		),
		(	
			22,
			N'Doutoramento',
			N'Biomedecina'
		),
		(	
			23,
			N'Licenciatura',
			N'Administração Pública'
		),
		(	
			24,
			N'Mestrado',
			N'Administração Pública'
		),
		(	
			25,
			N'Mestrado',
			N'Planeamento Regional e Urbano'
		),
		(	
			26,
			N'Mestrado',
			N'Ciência Política'
		),
		(	
			27,
			N'Mestrado',
			N'Estudos Chineses'
		),
		(	
			28,
			N'Doutoramento',
			N'Políticas Públicas'
		),
		(	
			29,
			N'Doutoramento',
			N'Ciência Política'
		),
		(	
			30,
			N'Doutoramento',
			N'E-Planeamento'
		),
		(	
			31,
			N'Licenciatura',
			N'Design'
		),
		(	
			32,
			N'Licenciatura',
			N'Música'
		),
		(	
			33,
			N'Licenciatura',
			N'Novas Tecnologias da Comunicação'
		),
		(
			34,
			N'Mestrado',
			N'Música'
		),
		(
			35,
			N'Mestrado',
			N'Ensino de Música'
		),
		(
			36,
			N'Mestrado',
			N'Design'
		),
		(	
			37,
			N'Mestrado',
			N'Criação Artística Contemporânea'
		),
		(	
			38,
			N'Mestrado',
			N'Comunicação Multimédia'
		),
		(	
			39,
			N'Doutoramento',
			N'Música'
		),
		(
			40,
			N'Doutoramento',
			N'Design'
		),
		(
			41,
			N'Doutoramento',
			N'Informação e Comunicação em Plataformas Digitais'
		),
		(	
			42,
			N'Licenciatura',
			N'Gestão'
		),
		(
			43,
			N'Licenciatura',
			N'Economia'
		),
		(
			44,
			N'Licenciatura',
			N'Turismo'
		),
		(
			45,
			N'Mestrado',
			N'Economia'
		),
		(
			46,
			N'Mestrado',
			N'Gestão'
		),
		(
			47,
			N'Licenciatura com Mestrado Integrado',
			N'Engenharia e Gestão Industrial'
		),
		(
			48,
			N'Mestrado',
			N'Gestão e Planeamento em Turismo'
		),
		(
			49,
			N'Doutoramento',
			N'Ciências Económicas e Empresariais'
		),
		(
			50,
			N'Doutoramento',
			N'Contabilidade'
		),
		(
			51,
			N'Doutoramento',
			N'Engenharia e Gestão Industrial'
		),
		(
			52,
			N'Doutoramento',
			N'Marketing e Estratégia'
		),
		(
			53,
			N'Doutoramento',
			N'Turismo'
		),
		(
			54,
			N'Licenciatura',
			N'Educação Básica'
		),
		(
			55,
			N'Licenciatura',
			N'Psicologia'
		),
		(
			56,
			N'Mestrado Académico',
			N'Educação e Formação'
		),
		(
			57,
			N'Mestrado Académico',
			N'Gerontologia Aplicada'
		),
		(
			58,
			N'Mestrado Especializante',
			N'Educação pré-escolar e Ensino no ceb'
		),
		(
			59,
			N'Mestrado Especializante',
			N'Ensino de 1º ceb de matemática e ciências naturais no 2º ceb'
		),
		(
			60,
			N'Mestrado Especializante',
			N'Ensino do 1º ciclo do ensino básico de português e história e geografia de Portugal no 2º ceb'
		),
		(
			61,
			N'Mestrado Especializante',
			N'Ensino de biologia e geologia no 3º ciclo do ensino básico e no ensino secundário'
		),
		(
			62,
			N'Mestrado Especializante',
			N'Ensino de inglês e de língua estrangeria(alemão/espanhol/francês) no 3º 
			ciclo do Ensino Básico e no Ensino Secundário'
		),
		(
			63,
			N'Mestrado Especializante',
			N'Ensino de Inglês no 1º Ciclo do Ensino Básico'
		),
		(
			64,
			N'Mestrado Especializante',
			N'Ensino de Português e de Língua Estrangeira no 3o Ciclo do Ensino Básico e 
			no ensino Secundário, na especialidade de Alemão ou Francês ou Espanhol'
		),
		(
			65,
			N'Mestrado Especializante',
			N'Ensino de Matemática no 3º Ciclo do Ensino Básico e no Ensino Secundário'
		),
		(
			66,
			N'Mestrado Especializante',
			N'Psicologia da Saúde e Reabilitação Neuropsicológica'
		),
		(
			67,
			N'Doutoramento',
			N'Educação'
		),
		(
			68,
			N'Doutoramento',
			N'Gerontologia e Geriatria'
		),
		(
			69,
			N'Doutoramento',
			N'Multimédia em Educação'
		),
		(
			70,
			N'Doutoramento',
			N'Psicologia'
		),
		(
			71,
			N'Licenciatura',
			N'Engenharia Informática'
		),
		(
			72,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia De Computadores e Telemática'
		),
		(
			73,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia Eletrónica e Telecomunicações Biomédica'
		),
		(
			74,
			N'Mestrado',
			N'Engenharia Informática'
		),
		(
			76,
			N'Doutoramento',
			N'Engenharia Eletrotécnica'
		),
		(
			77,
			N'Doutoramento',
			N'Engenharia Informática'
		),
		(
			78,
			N'Curso de Formação Avançada',
			N'Projeto de Sistemas Rádio'
		),
		(
			79,
			N'Curso de Formação Avançada',
			N'Computação Móvel'
		),
		(
			80,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia de Materiais e dispositivos biomédicos'
		),
		(
			81,
			N'Curso de Especialização',
			N'Temas de engenharia cerâmica'
		),
		(
			82,
			N'Doutoramento',
			N'Nanociências e Nanotecnolocias'
		),
		(
			83,
			N'Doutoramento',
			N'Ciência e Engenharia de Materiais'
		),
		(
			84,
			N'Licenciatura',
			N'Reabilitação do Património'
		),
		(
			85,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia Civil'
		),
		(
			86,
			N'Doutoramento',
			N'Engenharia Civil'
		),
		(
			87,
			N'Curso de Especialização',
			N' Riscos e Reabilitação Sustentável'
		),
		(
			88,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia Mecânica'
		),
		(
			89,
			N'Mestrado',
			N'Engenharia de Automação Industrial'
		),
		(
			90,
			N'Mestrado',
			N'Engenharia e Design de Produto'
		),
		(
			91,
			N'Doutoramento',
			N'Engenharia Mecânica'
		),
		(
			92,
			N'Curso de Formação Avançada',
			N'Eficiência Energética e Energias Renováveis'
		),
		(
			93,
			N'Licenciatura',
			N'Ciências do Mar'
		),
		(
		
			94,
			N'Licenciatura',
			N'Física'
		),
		(
			95,
			N'Licenciatura',
			N'Meteorologia, Oceanografia e Geofísica'
		),
		(	
			96,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia Biomédica'
		),
		(
			97,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia Computacional'
		),
		(
			98,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia Física'
		),
		(
			99,
			N'Mestrado',
			N'Física'
		),
		(
			100,
			N'Mestrado',
			N'Ciências do mar e da Atmosfera'
		),
		(
			101,
			N'Mestrado',
			N'Materiais e dispositivos Biomédicos'
		),
		(
			102,
			N'Doutoramento',
			N'Física'
		),
		(
			103,
			N'Doutoramento',
			N'Engenharia Física'
		),
		(
			104,
			N'Doutoramento',
			N'Ciência, tecnologia e gestão do mapa'
		),
		(
			105,
			N'Doutoramento',
			N'História das Ciências e Educação Científica'
		),
		(
			106,
			N'Licenciatura',
			N'Engenharia Geológica'
		),
		(
			107,
			N'Licenciatura',
			N'Geologia'
		),
		(
			108,
			N'Mestrado',
			N'Engenharia Geológica'
		),
		(
			109,
			N'Mestrado',
			N'Geomateriais e recursos geológicos'
		),
		(
			110,
			N'Mestrado',
			N'Geociências'
		),
		(
			111,
			N'Licenciatura',
			N'Línguas e estudos editoriais'
		),
		(
			112,
			N'Licenciatura',
			N'Línguas, literaturas e culturas'
		),
		(
			113,
			N'Licenciatura',
			N'Línguas e relações empresariais'
		),
		(
			114,
			N'Licenciatura',
			N'Tradução'
		),
		(
			115,
			N'Mestrado',
			N'Estudos Editoriais'
		),
		(
			116,
			N'Mestrado',
			N'Línguas, Literaturas e Culturas'
		),
		(
			117,
			N'Mestrado',
			N'Línguas e Relações Empresarias'
		),
		(
			118,
			N'Licenciatura',
			N'Matemática'
		),
		(
			119,
			N'Mestrado',
			N'Matemática e Aplicações'
		),
		(
			120,
			N'Mestrado',
			N'Matemática e Professores'
		),
		(
			121,
			N'Doutoramento',
			N'Matemática'
		),
		(
			122,
			N'Doutoramento',
			N'Matemática Aplicada'
		),
		(
			123,
			N'Licenciatura',
			N'Bioquímica'
		),
		(
			124,
			N'Licenciatura',
			N'Biotecnologia'
		),
		(
			125,
			N'Licenciatura',
			N'Química'
		),
		(
			126,
			N'Mestrado',
			N'Química'
		),
		(
			127,
			N'Mestrado',
			N'Bioquímica'
		),
		(
			128,
			N'Mestrado',
			N'Biotecnologia'
		),
		(
			129,
			N'Licenciatura com Mestrado integrado',
			N'Engenharia Química'
		),
		(
			130,
			N'Doutoramento',
			N'Química'
		),
		(
			131,
			N'Doutoramento',
			N'Engenharia Química'
		),
		(
			132,
			N'Doutoramento',
			N'Engenharia de refinação, petroquímica e química'
		),
		(
			133,
			N'Doutoramento',
			N'Ciência e tecnologia alimentar e nutrição'
		),
		(
			134,
			N'Doutoramento',
			N'Química sustentável ciência e tecnologia de polímeros'
		),
		(
			135,
			N'Doutoramento',
			N'Biorrefinarias'
		),
		(
			136,
			N'Licenciatura',
			N'Enfermagem'
		),
		(
			137,
			N'Licenciatura',
			N'Fisioterapia'
		),
		(
			138,
			N'Licenciatura',
			N'Gerontologia'
		),
		(
			139,
			N'Licenciatura',
			N'Imagem Médica e Radioterapia'
		),
		(
			140,
			N'Licenciatura',
			N'Terapia da Fala'
		),
		(	
			141,
			N'Mestrado',
			N'Enfermagem da Saúde Familiar'
		),
		(
			142,
			N'Mestrado',
			N'Fisioterapia'
		),
		(
			143,
			N'Curso de Especialização',
			N'Enfermagem do Trabalho'
		),
		(	
			144,
			N'Curso de Especialização',
			N'Fisioterapia Respiratória do Adulto'
		),
		(
			145,
			N'Curso de Especialização',
			N'Fisioterapia Musculosquelética'
		),
		(	
			146,
			N'Curso de Especialização',
			N'Perturbações da Linguagem e da Comunicação na Criança'
		),
		(	
			147,
			N'Curso de Especialização',
			N'Perturbações Neurológicas da Comunicação e da Deglutição no Adulto e no Idoso'
		),
		(	
			148,
			N'Mestrado',
			N'Geoinformática'
		),
		(	
			149,
			N'Mestrado',
			N'Gestão Comercial'
		),
		(	
			150,
			N'Licenciatura',
			N'Engenharia Eletrotécnica'
		),
		(	
			151,
			N'Licenciatura',
			N'Tecnologias de Informação'
		),
		(	
			152,
			N'Licenciatura',
			N'Gestão Comercial'
		),
		(	
			153,
			N'Licenciatura',
			N'Gestão da Qualidade'
		),
		(	
			154,
			N'Licenciatura',
			N'Gestão Pública e Autárquica'
		),
		(
			155,
			N'Licenciatura',
			N'Secretariado e Comunicação Empresarial'
		),
		(	
			156,
			N'Curso Técnico Superior Profissional',
			N'Gestão de PME'),
		(	
			157,
			N'Curso Técnico Superior Profissional',
			N'Instalações Elétricas e Automação'
		),
		(	
			158,
			N'Curso Técnico Superior Profissional',
			N'Manutenção Industrial'
		),
		(	
			159,
			N'Curso Técnico Superior Profissional',
			N'Programação de Sistemas de Informação'
		),
		(	
			160,
			N'Curso Técnico Superior Profissional',
			N'Redes e Sistemas Informáticos'
		),
		(	
			161,
			N'Curso Técnico Superior Profissional',
			N'Tecnologia Mecânica'
		),
		(	
			162,
			N'Licenciatura',
			N'Tecnologia e Sistemas de Produção'
		),
		(	
			163,
			N'Licenciatura',
			N'Design de Produto e Tecnologia'
		),
		(	
			164,
			N'Curso Técnico Superior Profissional',
			N'Automação Robótica e Informática Industrial'
		),
		(	
			165,
			N'Curso Técnico Superior Profissional',
			N'Desenvolvimento de Software'
		),
		(	
			166,
			N'Curso Técnico Superior Profissional',
			N'Design de Produto'
		),
		(	
			167,
			N'Curso Técnico Superior Profissional',
			N'Gestão de Processos Industriais'
		),
		(	
			168,
			N'Curso Técnico Superior Profissional',
			N'Projeto de Moldes'
		),
		(	
			169,
			N'Curso Técnico Superior Profissional',
			N'Sistemas Mecatrónicos e de Produção'
		),
		(	
			170,
			N'Licenciatura',
			N'Contabilidade'
		),
		(	
			171,
			N'Licenciatura',
			N'Marketing'
		),
		(	
			172,
			N'Licenciatura',
			N'Finanças'
		),
		(	
			173,
			N'Mestrado',
			N'Contabilidade'
		),
		(	
			174,
			N'Mestrado',
			N'Marketing'
		),
		(	
			175,
			N'Mestrado',
			N'Finanças'
		),
		(	
			176,
			N'Mestrado',
			N'Contabilidade Pública'
		),
		(	
			177,
			N'Curso de Especialização',
			N'Contabilidade e Auditoria'
		),
		(	
			178,
			N'Curso de Especialização',
			N'Contabilidade e Fiscalidade'
		),
		(	
			179,
			N'Curso de Especialização',
			N'Contabilidade Pública'
		),
		(	
			180,
			N'Curso Técnico Superior Profissional',
			N'Banca e Seguros'
		),
		(	
			181,
			N'Curso Técnico Superior Profissional',
			N'Gestão Aplicada ao Desenvolvimento de Produtos Turísticos'
		),
		(	
			182,
			N'Curso Técnico Superior Profissional',
			N'Gestão de Vendas e Marketing'
		),
		(	
			183,
			N'Mestrado',
			N'Multimédia em Educação'
		)
		;



-- EDIFICIOS
INSERT INTO Edificio (id, nome, tipo, sigla, dataConstrucao, descricao, coordenadas, arquiteto) 
	VALUES 
		(
			N'1', 
			N'Associação para a Formação Profissional e Investigação/Unidade Integrada de Formação Continuada/Incubadora de Empresas da Universidade de Aveiro', 
			N'Unidade Orgânica', 
			N'UNAVE/UINFOC/IEUA', 
			N'1977', 
			N'UNAVE: A UNAVE–Associação para a Formação Profissional e Investigação da Universidade de Aveiro (UA), é, desde 1986, a unidade de interface da Universidade de Aveiro com a sociedade para a aprendizagem ao longo de vida (university lifelong learning). Nos termos estatutários, é uma pessoa coletiva de direito privado, sem fins lucrativos. 
			UINFOC: A Unidade Integrada de Formação Continuada (UINFOC) constitui-se como um centro de dinamização, coordenação científica, pedagógica e administrativa, e de apoio a atividades de formação e investigação sobre a formação ao longo da vida (FALV), de públicos tradicionais e não tradicionais, na Universidade de Aveiro (UA). 
			IEUA: A Incubadora de Empresas da Universidade de Aveiro (IEUA), criada em 1996, é uma plataforma onde os empreendedores encontram as condições necessárias para concretizarem, validarem e promoverem os seus projetos.',
			N'40.636067, -8.657726',
			NULL
		),
		(
			N'2', 
			N'Departamento de Línguas e Culturas', 
			N'Departamento', 
			N'DLC', 
			N'1980', 
			N'O Departamento ocupa um edifício com uma área de 4320 m2, que inclui um laboratório de línguas, um anfiteatro de 107 lugares, salas de aulas, de vídeo, de multimédia e uma sala de leitura.', 
			N'40.635184, -8.657965',
			N'Firmino Trabulo'
		),
		(
			N'3', 
			N'Instituto de Materiais de Aveiro/Tecnologia Mecânica e Automação/Centro de Estudos do Ambiente e do Mar/ Instituto Confúcio ', 
			N'Unidade Orgânica', 
			N'CICECO/TEMA/CESAM/IC-UA', 
			N'2002', 
			N'CICECO: O Laboratório Associado CICECO – Instituto de Materiais de Aveiro, denominado anteriormente CICECO - Centro de Investigação em Materiais Cerâmicos e Compósitos, criado em Março de 2002 na Universidade de Aveiro, tem como missão desenvolver conhecimento científico e tecnológico necessário para a produção e transformação inovadora de materiais cerâmicos, híbridos orgânicos-inorgânicos e materiais para um desenvolvimento sustentável. O CICECO é hoje o maior instituto português em matéria de Engenharia e Ciência dos Materiais. 
			TEMA: A missão do Centro de Tecnologia Mecânica e Automação (TEMA) é primado pela excelência científica e tecnológica, de acordo com as necessidades da sociedade Portuguesa e as suas indústrias. Devido à sua natureza fundamentada na engenharia mecânica, o Centro de Tecnologia Mecânica e Automação (TEMA), é um pólo de investigação estratégico e relevante para a comunidade, em especial para as empresas e indústrias. 
			CESAM: A missão do CESAM, um Laboratório Associado da Universidade de Aveiro, é desenvolver uma pesquisa internacional líder em meio ambiente, com ênfase especial nas áreas costeiras e marinhas, com o objetivo de contribuir fortemente para a formulação e implementação de programas nacionais e europeus e estratégias para o desenvolvimento sustentável. 
			IC-UA: O Instituto Confúcio da Universidade de Aveiro (IC-UA), além de oferecer todos os níveis de língua (iniciação, intermédio, avançado), vários cursos específicos de língua e de cultura chinesa e cursos de preparação para os exames de nível, dinamiza também atividades culturais para assinalar os principais momentos do calendário chinês. O Instituto Confúcio da Universidade de Aveiro (IC-UA) foi inaugurado a 23 de abril de 2015, com a presença de diversas personalidades nacionais e estrangeiras.', 
			N'40.630027, -8.656334',
			N'Firmino Alberto Trabulo'
		),
		(
			N'4', 
			N'Departamento de Eletrónica, Telecomunicações e Informática', 
			N'Departamento', 
			N'DETI', 
			N'1974', 
			N'O Departamento de Eletrónica, Telecomunicações e Informática (DETI) foi fundado em 1974, com o nome de Departamento de Eletrónica e Telecomunicações, tendo sido um dos primeiros departamentos a iniciar atividade após a criação da Universidade de Aveiro em 1973. Em 2006 foi alterada a sua designação por forma a espelhar a atividade existente no Departamento na área da Informática. Alinhadas com o DETI funcionam duas Unidades de Investigação, o IEETA (Instituto de Engenharia Eletrónica e Telemática de Aveiro) e o Pólo de Aveiro do IT (Instituto de Telecomunicações), cujas atividades são, essencialmente, asseguradas por docentes e investigadores ligados ao DETI.', 
			N'40.633213, -8.659511',
			N'Rebello de Andrade e Espírito Santo'
		),
		(
			N'5', 
			N'Departamento de Educação e Psicologia', 
			N'Departamento', 
			N'DEP', 
			N'1994', 
			N'O Departamento de Educação e Psicologia é uma unidade orgânica de ensino e investigação do subsistema de ensino universitário que, inserido na estrutura orgânica da Universidade como sua unidade constitutiva, corresponde às áreas científicas de Educação e Psicologia. O Departamento de Educação e Psicologia, no seu âmbito de atuação e no respeito da natureza e especificidades do subsistema de ensino superior em que se insere, contribui para a realização da missão da Universidade e assegura a consecução das respetivas atribuições legais, designadamente pela prestação do serviço público de ensino superior. Nos termos dos Estatutos da Universidade e para além do ensino e investigação que o caracterizam como unidade orgânica, o Departamento de Educação e Psicologia promove ainda, no seu âmbito de atuação, a transferência de conhecimento e de tecnologia para a sociedade, bem como a dinamização de atividades culturais e humanistas em prol e estreita interação com a comunidade envolvente.', 
			N'40.631969, -8.658856',
			N'José Maria Lopo Prata'
		),
		(
			N'6',
			N'Refeitório de Santiago/Serviços de Ação Social',
			N'Refeitório/Serviços',
			N'SASUA',
			N'1987',
			N'O Refeitório de Santiago é constituído por duas salas de 400 lugares cada e situa-se no edifício central dos Serviços de Acção Social. 
			As refeições são pagas através de senhas, previamente adquiridas em máquinas automáticas localizadas no átrio do refeitório e piso 0 do edifício sede e diversos outros locais. 
			Preço das refeições: Estudante: 2.45€; Funcionário e utente credenciado: 4.10€; Visitante: 5.00€. 
			Horário: Almoço 12h00 – 14h30, Jantar 18h30 – 20h30 (Seg a Sex); Almoço 13h00 – 14h30, Jantar 19h00 – 20h30 (Sab a Dom). 
			Equipa de coordenação: 
			Nutricionista: Dr.ª Manuela Ferreira
			Encarregado: Yolanda Oliveira; 
			SASUA: A missão dos SASUA é contribuir para a formação integral dos estudantes, enquanto desígnio constitutivo do projeto educativo da UA, proporcionando apoios sociais aos alunos, por forma a garantir a igualdade de oportunidades no acesso e na frequência bem-sucedida do ensino superior, em contexto académico de cidadania ativa.',
			N'40.630607, -8.659113',
			N'Rebello de Andrade e Espírito Santo'
		),
			(
			N'7', 
			N'Departamento de Ambiente e Ordenamento', 
			N'Departamento', 
			N'DAO', 
			N'1990', 
			N'Em 1979 é criado o Departamento de Ambiente. Com o surgimento da Licenciatura em Planeamento Regional e Urbano, em 1983, passa a designar-se como Departamento de Ambiente e Ordenamento (DAO), refletindo, assim, áreas dominantes em termos de Ensino, Investigação e Relação Universidade-Sociedade. Em 2005 a Licenciatura em Planeamento Regional e Urbano é transferida para a Secção Autónoma das Ciências Sociais Jurídicas e Políticas, mantendo-se o nome do departamento.', 
			N'40.632670, -8.659037',
			N'Firmino Alberto Trabulo'
		),
		(
			N'8', 
			N'Departamento de Biologia', 
			N'Departamento', 
			N'DBio', 
			N'1991', 
			N'O Departamento de Biologia teve origem no Sector de Biologia da Universidade de Aveiro, nascido com a contratação dos primeiros docentes em 1974. De início principalmente vocacionado para atividades de Ensino, Investigação e Extensão em áreas da Biologia Ambiental, abre-se agora, um novo ciclo para o Departamento de Biologia, com a consolidação de novas áreas nos domínios da Biologia Molecular e Celular.', 
			N'40.633966, -8.659463',
			N'José Carlos Loureiro'
		),
		(
			N'9', 
			N'Departamento de Engenharia de Materiais e Cerâmica', 
			N'Departamento', 
			N'DEMac', 
			N'1992', 
			N'Faz parte da missão do DEMaC desenvolver investigação de ponta com reconhecimento internacional na área científica de Ciência e Engenharia de Materiais, sustentar uma formação atualizada de excelência aos seus alunos e transferir conhecimento para a indústria nacional apoiando o seu esforço de competitividade no espaço global.', 
			N'40.634054, -8.658785',
			N'Alcino Soutinho'
		),
		(
			N'10', 
			N'Departamento de Economia, Gestão, Engenharia Industrial e Turismo', 
			N'Departamento', 
			N'DEGEIT', 
			N'1992', 
			N'O DEGEIT compreende quatro áreas principais de ensino e pesquisa: economia, gestão, engenharia industrial e turismo. ', 
			N'40.630978, -8.656761',
			N'Pedro e Luís Ramalho'
		),
		(
			N'11', 
			N'Departamento de Matemática', 
			N'Departamento', 
			N'DMat', 
			N'1993', 
			N'O Departamento de Matemática (DMat) foi criado em 1976 pouco tempo depois da fundação da Universidade de Aveiro. Este departamento encontra-se integrado num campus universitário harmonioso, verdadeira galeria da moderna arquitetura portuguesa. O DMat está sedeado desde 1993 num edifício com aproximadamente  4500 m2, com linhas exteriores envolventes e elegantes, delimitando um espaço acolhedor que se deixa invadir pela luz proveniente de uma cúpula.',
			N'40.630359, -8.658211',
			N'José Maria Lopo Prata'
		),
		(
			N'12', 
			N'Departamento de Ciências Sociais, Políticas e do Território', 
			N'Departamento', 
			N'DCSPT', 
			N'1992', 
			N'O Departamento de Ciências Sociais, Políticas e do Território (DCSPT) tem como missão o desenvolvimento do ensino, da investigação e da cooperação com a sociedade em torno do domínio das Políticas Públicas, na interseção das ciências sociais, ciências políticas e ciências do território, com o propósito de se tornar uma referência académica e científica neste domínio. A multidisciplinaridade caraterística do DCSPT está bem patente na atividade de produção científica. A qual, bem enquadrada no campo das políticas públicas, abrange setores tão diversos como a habitação, ensino superior, saúde, ciência e tecnologia, inovação, desenvolvimento regional, urbanismo, administração central e local/regional, ambiente, Estado social, população, partidos políticos, etc.', 
			N'40.629904, -8.658232',
			N'José Maria Lopo Prata'
		),
		(
			N'13', 
			N'Departamento de Física', 
			N'Departamento', 
			N'DFis', 
			N'1994', 
			N'O Departamento de Fisica é a unidade orgânica de ensino e investigação da UA na área de conhecimento da Física contribuindo para a missão da Universidade através de atividades de ensino, nos três ciclos de estudo, investigação e Desenvolvimento e ligação à Sociedade. Tendo em vista a criação de conhecimento, formação de profissionais altamente qualificados, transferência de tecnologia e conhecimento, promoção da formação e divulgação da Física para públicos alargados. No âmbito da sua atuação, promove ainda a cooperação com outras instituições de ensino e investigação em Física, qualidade, inovação e criatividade, uma atitude atenta a aspetos éticos e de sustentabilidade ambiental.', 
			N'40.630218, -8.656713',
			N'Alfredo Matos Correia'
		),
		(
			N'14', 
			N'Laboratório Central de Análises', 
			N'Laboratório', 
			N'LCA', 
			N'1993', 
			N'O Laboratório Central de Análises é uma unidade executiva da Universidade de Aveiro responsável pela execução e fornecimento de serviços de análise solicitados por entidades internas e externas à UA.', 
			N'40.629249, -8.655969',
			N' Gomes da Silva e HP – Hidrotécnica Portuguesa'
		),
		(
			N'15', 
			N'Departamento de Química', 
			N'Departamento', 
			N'DQ', 
			N'1993', 
			N'O Departamento de Química da Universidade de Aveiro (DQUA) é atualmente uma referência a nível nacional e europeu na sua área científica. O DQUA é reconhecido pela qualidade do seu ensino e da sua formação pós-graduada, bem como pela excelência da sua investigação. Também é o Departamento de Química com maior produção científica por docente em Portugal, apresentando uma publicação científica média semelhante aos valores de referência europeus (acima de 3 artigos SCI/docente/ano). A investigação no DQUA está enquadrada em dois Laboratórios Associados (CICECO e CESAM) e numa Unidade de Investigação (QOPNA), todos classificados com “Excelente” pela FCT. A investigação é suportada por um grande número de projetos nacionais e europeus, com financiamento público e privado (indústria), e por um moderno e avançado parque de infraestruturas científicas.',
			N'40.630021, -8.655720',
			N'Alcino Soutinho'
		),
		(
			N'16', 
			N'Departamento de Geociências', 
			N'Departamento', 
			N'DGeo', 
			N'1993', 
			N'Quando se constituiu a Universidade de Aveiro em meados da década de 70, foram selecionadas áreas prioritárias a desenvolver, entre as quais se contavam as Ciências da Natureza e do Ambiente. O Grupo Interdisciplinar de Estudos de Ambiente (Física, Química, Biologia e Geologia) lançou o ensino nestas áreas e criou o núcleo de Economia Mineral e Recursos Minerais, tendo como objetivo prioritário, a investigação da Bacia do Vouga. Foram selecionados como domínios merecedores de particular atenção a Geoeconomia (que abarcaria Geoquímica, Geofísica e Recursos Minerais), a Geologia Marinha e a Sedimentologia de depósitos recentes. O desenvolvimento da área de Geologia (ensino e investigação) veio nos finais da década de 70 a traduzir-se, de acordo com o modelo organizativo da Universidade, na criação do Departamento de Geociências, o primeiro em Portugal com um contexto integrador das várias áreas ligadas às Ciências da Terra, incluindo as Ciências de Engenharia e as Ciências Geofísicas.', 
			N'40.629653, -8.656982',
			'Eduardo Souto Moura'
		),
		(
			N'17', 
			N'Biblioteca', 
			N'Biblioteca', 
			NULL, 
			N'1995', 
			N'Localizada no centro do Campus Universitário da UA, a Biblioteca da Universidade de Aveiro constitui-se como um agradável local para leitura, estudo e pesquisa acessível a toda a comunidade académica. Nela são disponibilizados os necessários recursos informativos que servem de suporte ao ensino, aprendizagem e à investigação na Universidade de Aveiro.', 
			N'40.631097, -8.659648',
			N'Álvaro Siza Vieira'
		),
		(
			N'19', 
			N'Instituto de Telecomunicações', 
			N'Unidade Orgânica', 
			N'IT', 
			N'1993', 
			N'Instituto de Telecomunicações (IT) é uma instituição privada sem fins lucrativos, de utilidade pública, resultante da associação de cinco instituições com experiência e tradição em investigação e desenvolvimento em Telecomunicações: Instituto Superior Técnico (IST); Universidade de Aveiro (UA); Faculdade de Ciências e Tecnologia da Universidade de Coimbra (FCTUC); Portugal Telecom Inovação, S.A. (PTIn); Siemens, S.A.; A missão do IT é criar e difundir o conhecimento científico no domínio das telecomunicações, o que pressupõe o desenvolvimento de actividades de investigação fundamental e aplicada num contexto internacional, como meio para elevar o nível de ensino e formação, graduado e pós-graduado, e para incrementar a competitividade da indústria portuguesa e dos operadores de telecomunicações. As actividades do IT centram-se em três pólos, em Aveiro, no Campus da Universidade de Aveiro, em Coimbra, no Polo II da Universidade de Coimbra e em Lisboa, no IST, bem como um laboratório externo na Universidade da Covilhã e uma delegação em Leiria no Instituto Politécnico. As principais áreas de investigação são: Comunicações sem fios; Comunicações Ópticas; Redes e Multimédia; Ás quais se acrescenta a área horizontal de Ciências Básicas e Tecnologias de Suporte. Em Novembro de 2001 foi atribuído ao Instituto de Telecomunicações o estatuto de Laboratório Associado.', 
			N'40.633970, -8.659769',
			N'José Maria Lopo Prata'
		),
		(
			N'20', 
			N'Escola Superior de Tecnologia e Gestão de Águeda', 
			N'Escola Superior', 
			N'ESTGA', 
			N'1997', 
			N'A Escola Superior de Tecnologia e Gestão de Águeda (ESTGA), da Universidade de Aveiro, assume-se, desde a sua fundação em 1997, como um centro de Ensino Superior único em Portugal. Localizada em pleno centro da cidade de Águeda, a ESTGA, ciente da dinâmica da região em que se insere, propõe uma oferta formativa que visa responder às necessidades de públicos com interesses diversificados e que se concretiza em cursos técnicos superiores profissionais, cursos de licenciatura e de mestrado.', 
			N'40.574504, -8.444123',
			NULL
		),
		(
			N'21', 
			N'Departamento de Comunicação e Arte', 
			N'Departamento', 
			N'DECA', 
			N'1996', 
			N'Neste Departamento estão sediadas competências muito diversificadas nas áreas de Ciências e Tecnologias da Comunicação, Design, Estudos de Arte e Música, que se organizam transversalmente, entre si e com outras competências externas, para oferecerem um conjunto muito atraente e inovador de cursos de formação inicial e pós-graduada.', 
			N'40.628918, -8.656398',
			N'Jorge Kol de Carvalho'
		),
		(
			N'22', 
			N'Departamento de Engenharia Mecânica', 
			N'Departamento', 
			N'DEM', 
			N'1996', 
			N'O DEM é responsável pelo ensino de graduação e pós-graduação na área científica de Engenharia Mecânica. A aposta inicial em dois ramos de formação do curso de Licenciatura em Engenharia Mecânica – Automação e Tecnologia – correspondeu não só aos reais anseios da indústria da região de Aveiro, bem como às necessidades de empresas internacionais. Numa segunda fase, renovado o plano de estudos da licenciatura, continua a indústria a recrutar os engenheiros mecânicos qualificados para solucionar os seus problemas. Os licenciados em Engenharia Mecânica pela Universidade de Aveiro foram, nesta data e de acordo com o Observatório do Emprego e Formação Profissional, os primeiros, a nível nacional, a integrar-se no mercado de trabalho. Uma percentagem elevada dos Licenciados em Engenharia Mecânica pela UA atingiram, num curto espaço de tempo, posições de liderança e de responsabilidade nas organizações e empresas onde desempenham a sua atividade profissional.', 
			N'40.6347499, -8.65902',
			N'Adalberto Dias'
		),
		(
			N'23', 
			N'Complexo Pedagógico, Científico e Tecnológico', 
			N'Departamento', 
			N'CP', 
			N'2000', 
			N'Prémio: Prémio de Arquitetura e Urbanismo do Município de Aveiro na categoria de “Obra de Qualidade Excecional”, 2005.', 
			N'40.629703, -8.655437',
			N'Victor Figueiredo'
		),
		(
			N'24', 
			N'Instituto de Engenharia Eletrónica e Telemática de Aveiro', 
			N'Unidade Orgânica', 
			N'IEETA', 
			N'1994', 
			N'O Instituto de Engenharia Eletrónica e Telemática de Aveiro - IEETA é uma associação científica e técnica sem fins lucrativos, tendo como missão a investigação multidisciplinar e desenvolvimento avançado em Eletrónica e Telemática, integrados na comunidade de investigação cientifica internacional e contribuindo para o desenvolvimento tecnológico e social nacionais. ',
			N'40.633150, -8.660186',
			N'Bernardo Ferrão'
		),
		(
			N'25', 
			N'Edifício Central e da Reitoria', 
			N'Edificio Central', 
			NULL, 
			N'2000', 
			N'Prémio: Prémio de Arquitetura e Urbanismo do Município de Aveiro na categoria de “Obra de Qualidade Excecional”, 2005. Serviços de Gestão Académica; Serviços de Gestão Técnica e Logística; Serviços de Gestão de Recursos Humanos e Financeiros; Serviços de Comunicação, Imagem e Relações Públicas.', 
			N'40.631403, -8.657424',
			N'Gonçalo Byrne e Manuel Aires Mateus'
		),
		(
			N'26', 
			N'Departamento de Biologia: Herbário e Laboratórios de Ensino', 
			N'Laboratório', 
			NULL, 
			N'2003', 
			NULL, 
			N'40.633512, -8.659841',
			N'Joaquim Oliveira e Nuno Portas'
		),
		(
			N'27', 
			N'Serviços de Tecnologias de Informação e Comunicação', 
			N'Unidade Orgânica', 
			N'sTIC', 
			N'2004', 
			N' Missão: fornecer serviços avançados de TIC que suportem a atividade da comunidade académica. Visão: ser uma referência interna e externa da prestação de serviços de TIC no ensino superior.
', 
			N'40.630046, -8.658699',
			N'José Maria Lopo Prata'
		),
		(
			N'28', 
			N'Departamento de Engenharia Civil', 
			N'Departamento', 
			N'DECivil', 
			N'2004', 
			N'Com base no curso de Engenharia Civil da Universidade de Aveiro (UA) que entrou em funcionamento no ano letivo de 1996/1997, o Departamento de Engenharia Civil (DECivil) é a Unidade Orgânica da estrutura organizativa da UA à qual corresponde a respetiva área de conhecimento. Para além do ensino e investigação, o DECivil promove ainda a transferência de conhecimento e de tecnologia para a sociedade. Ao longo dos anos tem vindo a consolidar a sua posição a nível nacional e internacional em várias áreas em que os seus docentes se têm destacado e desenvolvido investigação de excelência.', 
			N'40.629637, -8.657240',
			N'Joaquim Oliveira'
		),
		(
			N'29',
			N'Complexo de Laboratórios Tecnológicos',
			N'Laboratório',
			N'CLT',
			N'2007',
			NULL,
			N'40.630251, -8.656074',
			N'José Rebello de Andrade e Luís Fernandes'
		),	
		(
			N'30', 
			N'Departamento de Ciências Médicas', 
			N'Departamento', 
			N'DCM', 
			NULL, 
			N'Este departamento é localizado na Escola Superior de Saúde da Universidade de Aveiro.', 
			N'40.623625, -8.657652',
			N'Paulo Cirne e Bruno Dray'
		),
		(
			N'30', 
			N'Escola Superior de Saúde da Universidade de Aveiro', 
			N'Escola Superior', 
			N'ESSUA', 
			NULL, 
			N'Integrada na Universidade de Aveiro, a Escola Superior de Saúde de Aveiro administra os cursos de formação inicial de Enfermagem, Fisioterapia, Terapia da Fala, Radiologia e Gerontologia, oferecendo e colaborando em formação pós-graduada ao nível do 2o ciclo e em formação de 3o ciclo. Os cursos têm sido dos mais procurados pelos alunos nos concursos de acesso ao ensino superior, providenciando a área de pós graduação a possibilidade de formar e atualizar profissionais de saúde com novos conhecimentos e competências e de promover a inovação e investigação. Hoje com um novo edifício, possuí condições privilegiadas, de ensino, investigação e prestação de cuidados diferenciada colocando como filosofia subjacente à organização de toda a atividade da ESSUA e a centralidade no doente.', 
			N'40.635069, -8.657416',
			N'Paulo Cirne e Bruno Dray'
		),
		(
			N'31', 
			N'Fábrica Centro Ciência Viva', 
			N'Centro Educativo', 
			NULL, 
			N'2013', 
			N'A Fábrica Centro Ciência Viva de Aveiro, aberta ao público desde o dia 1 de julho de 2004, está instalada no edifício da antiga Companhia Aveirense de Moagens. Resulta de uma parceria entre a Universidade de Aveiro e a Ciência Viva - Agência Nacional para a Cultura Científica e Tecnológica, tendo como grande objetivo a divulgação de ciência e a promoção da cultura científica e tecnológica. A sua programação inclui atividades diversificadas, em diferentes formatos, permitindo vários níveis de interação com públicos de todas as idades.',
			N'40.638466, -8.657439',
			NULL
		),
		(
			N'32', 
			N'Complexo Interdisciplinar de Ciências Físicas Aplicadas à Nanotecnolocia e à Oceanografia', 
			N'Complexo', 
			N'CICFANO', 
			NULL, 
			NULL,
			N'40.630838, -8.656563',
			NULL
		),
		(
			N'33', 
			N'Edifício das Comunicações Óticas, Comunicações Rádio e Robótica', 
			N'Unidade Orgânica', 
			NULL, 
			N'2013', 
			N'Edifício onde podemos encontrar a equipa CAMBADA – campeã mundial em 2008.',
			NULL,
			N'Joaquim Oliveira'
		),
		(
			N'34', 
			N'Escola Superior De Design, Gestão e Tecnologia da Produção Aveiro-Norte', 
			N'Escola Superior', 
			N'ESAN', 
			N'2004', 
			N'A Escola Superior de Design, Gestão e Tecnologias da Produção de Aveiro - Norte (ESAN) foi criada a 8 de outubro de 2004, através da promulgação do Decreto-Lei 217/2004. Esta unidade orgânica da Universidade de Aveiro dedica-se ao ensino e à investigação, pertencendo ao subsistema de ensino Politécnico. A ESAN promove ainda “a transferência para a sociedade do conhecimento e da tecnologia, bem como a dinamização de atividades culturais e humanistas em prol e estreita interação com a comunidade envolvente”.', 
			N'40.862009, -8.476646',
			NULL
		),
		(
			N'35', 
			N'Instituto Superior de Contabilidade e Administração da Universidade de Aveiro', 
			N'Escola Superior', 
			N'ISCA-UA', 
			NULL, 
			N'O Instituto Superior de Contabilidade e Administração da Universidade de Aveiro tem a sua origem no ano letivo de 1965/66 com a criação da Escola Média de Comércio de Aveiro, escola particular destinada a formar contabilistas. No entanto, os seus alunos tinham que realizar exames no Instituto Comercial do Porto. Este, no ano letivo de 1970/71, foi autorizado, pelo Dec. Lei n. 440/71, a criar a secção de Aveiro, o que foi concretizado com a integração desta secção no ensino oficial.', 
			N'40.630558, -8.653333',
			NULL
		),
		(
			N'40', 
			N'Complexo das Ciências da Comunicação e Imagem', 
			N'Departamento', 
			N'CCCI', 
			N'2015', 
			NULL, 
			N'40.629228, -8.656846',
			NULL
		),
		(
			N'B',
			N'Residências dos Estudantes',
			N'Residência',
			NULL,
			N'1998',
			NULL,
			N'40.632342, -8.655567',
			N'Adalberto Dias'
		),
		(
			N'C',
			N'Residência dos Docentes',
			N'Residência',
			NULL,
			N'1994',
			NULL,
			N'40.637184, -8.657687',
			N'António José Matias'
		),
		(
			N'D',
			N'Instituto do Ambiente e Desenvolvimento',
			N'Instituto',
			N'IDAD',
			N'1980',
			N' O IDAD - Instituto do Ambiente e Desenvolvimento, é uma associação   científica e técnica, sem fins lucrativos e de utilidade pública, que atua ao nível do apoio integrado às necessidades AMBIENTAIS do mundo das EMPRESAS e das ORGANIZAÇÕES. Fundado em 1993, o IDAD conta com uma equipa multidisciplinar altamente competente em diversas áreas de atividade o que, aliado ao entrosamento com a Universidade de Aveiro, lhe tem permitido alcançar excelentes resultados de eficácia e de reconhecimento nos serviços prestados.',
			N'40.636463, -8.658479',
			N'Firmino Alberto Trabulo'
		),
		(
			N'E', 
			N'Pavilhão Aristides-Hall', 
			N'Pavilhão Polidesportivo', 
			NULL, 
			N'1994', 
			N'O Pavilhão Desportivo Dr. Aristides Hall foi em 1994 e tem uma área total de 3.284m2 e compreende vários espaços para a prática desportiva: Nave Central; Sala Polivalente; Sala de Treino Físico - Sauna; Courts de Squash; Serviços Comuns.', 
			N'40.629942, -8.654132',
			N' João Almeida e Victor Carvalho'
		),
		(
			N'F',
			N'Snack/Self-service/Restaurante Universitário',
			N'Refeitório',
			NULL,
			N'1996',
			N'O edifício do Snack-Bar / Self-Service é constituído por uma sala de refeitório e uma cafetaria. As refeições são pagas em dinheiro, por Multibanco ou através de cheque. Horário: 12h00h – 14h30h (Seg. a Sex.). O Restaurante Universitário situa-se no piso 1 do Snack-Bar / Self-Service e integra uma sala de refeições e uma esplanada. Aberto a qualquer utente, funciona diariamente com serviços de buffet. O serviço é personalizado, aceitando-se reservas para grupos que visitem ou trabalhem na universidade. Horário 12h30h – 14h30h (Seg. a Sex.)',
			N'40.631284, -8.655385',
			N'José Maria Lopo Prata'
		),
		(
			N'G',
			N'Centro de Infância Arte e Qualidade',
			N'CIAQ',
			NULL,
			N'1992',
			N' O Centro de Infância Arte e Qualidade (CIAQ) é uma Instituição Particular de Solidariedade Social (IPSS) fundada em 1980, situada no Campus da Universidade de Aveiro, e que recebe crianças de toda a cidade e periferias. Estas respostas funcionam atualmente em dois edifícios, o seu edifício sede e o pólo 2 no pavilhão I da Universidade de Aveiro. No edifício sede funcionam as respostas sociais de Creche e Pré-Escolar (2 salas). Está integrado no Campus Universitário de Aveiro junto ao Pavilhão Aristides Hall. No pólo 2, localizado no extremo Norte do Campus (por trás da cadeia), encontram-se mais três salas de Pré-Escolar e as 4 salas do CATL. Este é um edifício totalmente recuperado para se adaptar às funções que hoje serve, proporcionando salas de dimensões excelentes e espaços exteriores amplos e agradáveis.',
			N'40.630862, -8.654859',
			N'José Maria Lopo Prata'
		),
		(
			N'I',
			N'Grupo Experimental de Teatro da Universidade de Aveiro',
			N'Artes',
			N'GRETUA',
			NULL,
			N' Um grupo de alunos com um grande desejo de fugir à rotina dos estudos teve a brilhante ideia de criar um grupo de teatro que, em 1980, apresenta a sua primeira peça. Desde então, o GrETUA - Grupo Experimental de Teatro da Universidade de Aveiro - tem vindo a desenvolver diversas atividades, como oficinas, workshops, cursos de iniciação, teatros de rua e outras produções teatrais.',
			N'40.636978, -8.658037',
			N'Firmino Trabulo'
		),
		(
			N'L',
			N'Ponte Pedonal do Crasto',
			N'Ponte',
			NULL,
			N'2001',
			NULL,
			N'40.627057, -8.656871',
			N'João Carrilho da Graça'
		),
		(
			N'M',
			N'Complexo de Refeitórios do Crasto',
			N'Refeitório',
			NULL,
			N'2000',
			N'Preços para visitantes devidamente autorizados: Ementa “Normal”, Dieta ou Vegetariana - 5,00€; Ementa “Opção” -  6,00€. Horário: 12h00h – 14h30h (Seg. a Sex.), 13h00h – 14h30h (Sáb. e Dom.).',
			N'40.624553, -8.656821',
			N'Manuel e Francisco Aires Mateus'
		),		
		(
			N'N',
			N'Casa do Estudante/Sede da Associação Académica da Universidade de Aveiro/Bar do Estudante',
			N'Casa do Estudante',
			N'BE/AAUAv',
			N'2001',
			N' Na UA a representação máxima dos estudantes nos órgãos de gestão da instituição, cabe à Associação Académica da UA (AAUAv) e aos vários núcleos associativos filiados na AAUAv. Além de assegurarem a componente de integração dos estudantes na academia e de cooperação com outras entidades estudantis, os grupos associativos da UA constituem um polo dinamizador de atividades em áreas tão distintas, que vão desde a realização de iniciativas culturais e desportivas, à prestação de serviços de diversa ordem. A AAUAv, sedeada na Casa do Estudante, é ainda a responsável pela organização das festas académicas que assinalam o início (Semana de Integração) e o fecho (Enterro do Ano) das atividades letivas. ',
			N'40.623984, -8.657553',
			N'João Almeida e Victor Carvalho'
		),
		(
			N'S',
			N'Marinha Santiago da Fonte',
			N'Marinha',
			NULL,
			NULL,
			N' A Marinha de Santiago da Fonte, com produção de sal por método artesanal, foi adquirida pela Universidade de Aveiro em 1993 e é uma das sete que restam em laboração (das cerca de 270 que existiam na ria nos anos 50) a produzir sal marinho artesanal. A Marinha, construída em área de sapal, é um espaço de grande importância para aves e outros seres vivos, com condições de alimentação e abrigo vantajosas.',
			N'40.628738, -8.660520',
			NULL
		);



-- EDIFICIOS ENSINOS
INSERT INTO EdificioEnsino (edificio, ensino) 
	VALUES 
		('8', 1),
		('8', 2),
		('8', 3),
		('8', 4),
		('8', 5),
		('8', 6),
		('8', 7),
		('8', 8),
		('8', 9),
		('8', 10),
		('7', 11),
		('7', 12),
		('7', 13),
		('7', 14),
		('7', 15),
		('7', 16),
		('7', 17),
		('30', 18),
		('30', 19),
		('30', 20),
		('30', 21),
		('30', 22),
		('12', 23),
		('12', 24),
		('12', 25),
		('12', 26),
		('12', 27),
		('12', 28),
		('12', 29),
		('12', 30),
		('21', 31),
		('21', 32),
		('21', 33),
		('21', 34),
		('21', 35),
		('21', 36),
		('21', 37),
		('21', 38),
		('21', 39),
		('21', 40),
		('21', 41),
		('21', 183),
		('10', 42),
		('10', 43),
		('10', 44),
		('10', 45),
		('10', 46),
		('10', 47),
		('10', 48),
		('10', 49),
		('10', 50),
		('10', 51),
		('10', 52),
		('10', 53),
		('10', 12),
		('5', 54),
		('5', 55),
		('5', 56),
		('5', 57),
		('5', 58),
		('5', 59),
		('5', 60),
		('5', 61),
		('5', 62),
		('5', 63),
		('5', 64),
		('5', 65),
		('5', 66),
		('5', 67),
		('5', 68),
		('5', 69),
		('5', 70),
		('4', 71),
		('4', 72),
		('4', 73),
		('4', 74),
		('4', 21),
		('4', 76),
		('4', 77),
		('4', 78),
		('4', 79),
		('4', 89),
		('9', 80),
		('9', 81),
		('9', 82),
		('9', 83),
		('28', 84),
		('28', 85),
		('28', 86),
		('28', 87),
		('22', 88),
		('22', 89),
		('22', 90),
		('22', 91),
		('22', 92),
		('13', 93),
		('13', 94),
		('13', 95),
		('13', 96),
		('13', 97),
		('13', 98),
		('13', 99),
		('13', 100),
		('13', 101),
		('13', 102),
		('13', 103),
		('13', 104),
		('13', 105),
		('16', 106),
		('16', 107),
		('16', 108),
		('16', 109),
		('16', 110),
		('2', 111),
		('2', 112),
		('2', 113),
		('2', 114),
		('2', 115),
		('2', 116),
		('2', 117),
		('11', 118),
		('11', 119),
		('11', 120),
		('11', 121),
		('11', 122),
		('15', 123),
		('15', 124),
		('15', 125),
		('15', 126),
		('15', 127),
		('15', 128),
		('15', 129),
		('15', 130),
		('15', 131),
		('15', 132),
		('15', 133),
		('15', 134),
		('15', 135),
		('30', 136),
		('30', 137),
		('30', 138),
		('30', 139),
		('30', 140),
		('30', 141),
		('30', 142),
		('30', 143),
		('30', 144),
		('30', 145),
		('30', 146),
		('30', 147),
		('20', 148),
		('20', 149),
		('20', 150),
		('20', 151),
		('20', 152),
		('20', 153),
		('20', 154),
		('20', 155),
		('20', 156),
		('20', 157),
		('20', 158),
		('20', 159),
		('20', 160),
		('20', 161),
		('34', 166),
		('34', 167),
		('34', 168),
		('34', 169),
		('35', 170),
		('35', 171),
		('35', 172),
		('35', 173),
		('35', 174),
		('35', 175),
		('35', 176),
		('35', 177),
		('35', 178),
		('35', 179),
		('35', 180),
		('35', 181),
		('35', 182)
		;
		


INSERT INTO Refeitorio (nome, coordenadas)
	VALUES
		(N'Refeitório do Crasto', '40.624481, -8.656843'),
		(N'Refeitório de Santiago', '40.630688, -8.659102'),
		(N'Snack-Bar/Self', '40.631323, -8.655492'),
		(N'Restaurante Universitário', '40.631323, -8.655492'),
		(N'Refeitório da ESTGA', '40.574560, -8.444141'),
		(N'Refeitório da ESAN', '40.861969, -8.476571')
		;



INSERT INTO ParquePublico (nome, coordenadas)
	VALUES
		(N'Parque do Autocarro Bar', '40.634503, -8.656914'),
		(N'Parque do Complexo Pedagógico', '40.629374, -8.654715'),
		(N'Parque atrás do DMAT/DEM/DECivil', '40.629598, -8.658584'),
		(N'Parque da Rua da Pêga', '40.633811, -8.661225')
		;
