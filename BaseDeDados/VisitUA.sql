-- Base de Dados MySQL para armazenar dados sobre as Coleções, Obras de Arte Públicas, serviços e Edifícios, Refeitórios e Parques Públicos da UA.

CREATE TABLE Colecao
(
	id			INT				NOT NULL	AUTO_INCREMENT,
	imagem		LONGBLOB,
	nome		VARCHAR(100)	NOT NULL,
	descricao	VARCHAR(2000),
	doador		VARCHAR(100),
	localVisita	VARCHAR(500),
	coordenadasLocal VARCHAR(50),
	colecaoMae	INT,
	
	PRIMARY KEY(id),
	FOREIGN KEY (colecaoMae) REFERENCES Colecao(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE ObraArte
(
	id				INT				NOT NULL	AUTO_INCREMENT,
	imagem			LONGBLOB,
	nome			VARCHAR(100),
	local_			VARCHAR(500),
	dimensoes		VARCHAR(2000),
	autor			VARCHAR(100),
	materiais		VARCHAR(500),
	dataColocacaoUA	VARCHAR(30),
	coordenadas		VARCHAR(50),
	
	PRIMARY KEY(id)
);

CREATE TABLE Servico
(
	id			INT				NOT NULL	AUTO_INCREMENT,
	imagem		LONGBLOB,
	local_		VARCHAR(500),
	designacao	VARCHAR(100)	NOT NULL,
	coordenadas	VARCHAR(50),
	horario		VARCHAR(50),
	
	PRIMARY KEY(id)
);

CREATE TABLE Edificio
(
	id				VARCHAR(50)		NOT NULL,
	imagem			LONGBLOB,
	nome			VARCHAR(100)	NOT NULL,
	tipo			VARCHAR(100),
	sigla			VARCHAR(50),
	dataConstrucao	VARCHAR(30),
	descricao		VARCHAR(2000),
	coordenadas		VARCHAR(50),
	arquiteto		VARCHAR(100),
	
	PRIMARY KEY(id, nome)
);

CREATE TABLE Ensino
(
	id			INT 			UNIQUE NOT NULL,
	tipo		VARCHAR(100)	NOT NULL,
	nome		VARCHAR(100)	NOT NULL,
	
	PRIMARY KEY(nome, tipo)
);

CREATE TABLE EdificioEnsino
(
	edificio	VARCHAR(50)		NOT NULL,
	ensino		INT				NOT NULL,
	
	PRIMARY KEY(edificio, ensino),
	FOREIGN KEY(edificio) REFERENCES Edificio(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY(ensino) REFERENCES Ensino(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Refeitorio
(
	id			INT				NOT NULL	AUTO_INCREMENT,
	nome		VARCHAR(100)	NOT NULL,
	coordenadas	VARCHAR(50),
	
	PRIMARY KEY(id)
);

CREATE TABLE ParquePublico
(
	id			INT				NOT NULL	AUTO_INCREMENT,
	nome		VARCHAR(100)	NOT NULL,
	coordenadas	VARCHAR(50),
	
	PRIMARY KEY(id)
);
