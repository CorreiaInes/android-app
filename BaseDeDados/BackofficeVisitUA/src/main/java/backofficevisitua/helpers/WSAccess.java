package backofficevisitua.helpers;

import java.io.*;
import java.net.*;

public class WSAccess {
    
    public static String sendGet(String url) throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        
        con.setRequestMethod("GET");
        
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        
        int responseCode = con.getResponseCode();
        System.out.println("\nSending GET request to URL: " + url);
        System.out.println("Response Code: " + responseCode);
        
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        
        while( (inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();
        
        return response.toString();
    }
    
    public static String sendPost(String url, String urlParameters) throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        
        con.setRequestMethod("POST");
        
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept_Language", "pt;q=0.8");
        con.setRequestProperty("Content-Type", "application/json;charset=utf-8");
        
        con.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes(urlParameters);
        out.flush();
        out.close();
        
        int responseCode = con.getResponseCode();
        System.out.println("\nSending POST request to URL: " + url);
        System.out.println("Post parameters: " + urlParameters);
        System.out.println("Response Code: " + responseCode);
        
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        
        while( (inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();
        
        return response.toString();
    }
    
    public static String sendPostImage(String url, String urlParameters) throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        
        con.setRequestMethod("POST");
        
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept_Language", "pt;q=0.8");
        con.setRequestProperty("Content-Type", "multipart/form-data");
        
        con.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        out.writeBytes(urlParameters);
        out.flush();
        out.close();
        
        int responseCode = con.getResponseCode();
        System.out.println("\nSending POST (image) request to URL: " + url);
        System.out.println("Post parameters: " + urlParameters);
        System.out.println("Response Code: " + responseCode);
        System.out.println("Response Error Stream: " + con.getErrorStream());
        
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        
        while( (inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();
        
        return response.toString();
    }
    
}
