package backofficevisitua.helpers;

/**
 * Ficheiro de constantes.
 */
public class Constants {
  
    /*
        URL base para o efetuar pedidos à API.
    */
    public static final String BASE_URL = "http://deti-visitua.ua.pt:8080/VisitUA_REST_API-1.0-SNAPSHOT/webresources/visitua/";
    
}
