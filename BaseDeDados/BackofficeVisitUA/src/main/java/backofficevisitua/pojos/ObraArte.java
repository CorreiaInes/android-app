package backofficevisitua.pojos;

import java.sql.Blob;

/**
 * POJO Obra de Arte.
 */
public class ObraArte {
    
    private int id;
    private String nome;
    private String local;
    private String dimensoes;
    private String autor;
    private String materiais;
    private String dataColocacaoUA;
    private String coordenadas;
    
    private boolean editable;

    public ObraArte() {
    }
    
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if(nome.equals(""))
            nome = null;
        this.nome = nome;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        if(local.equals(""))
            local = null;
        this.local = local;
    }

    public String getDimensoes() {
        return dimensoes;
    }

    public void setDimensoes(String dimensoes) {
        if(dimensoes.equals(""))
            dimensoes = null;
        this.dimensoes = dimensoes;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        if(autor.equals(""))
            autor = null;
        this.autor = autor;
    }

    public String getMateriais() {
        return materiais;
    }

    public void setMateriais(String materiais) {
        if(materiais.equals(""))
            materiais = null;
        this.materiais = materiais;
    }

    public String getDataColocacaoUA() {
        return dataColocacaoUA;
    }

    public void setDataColocacaoUA(String dataColocacaoUA) {
        if(dataColocacaoUA.equals(""))
            dataColocacaoUA = null;
        this.dataColocacaoUA = dataColocacaoUA;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        if(coordenadas.equals(""))
            coordenadas = null;
        this.coordenadas = coordenadas;
    }

    @Override
    public String toString() {
        return "ObraArte{" + "id=" + id + ", nome=" + nome + ", local=" + local + ", dimensoes=" + dimensoes + ", autor=" + autor + ", materiais=" + materiais + ", dataColocacaoUA=" + dataColocacaoUA + ", coordenadas=" + coordenadas + '}';
    }
    
    public String jsonString()
    {
        String retVal = "{";
        
        if(nome == null)
            retVal += "nome=null, ";
        else
            retVal += "nome='" + nome + "', ";
        
        if(local == null)
            retVal += "local=null, ";
        else
            retVal += "local='" + local + "', ";
        
        if(dimensoes == null)
            retVal += "dimensoes=null, ";
        else
            retVal += "dimensoes='" + dimensoes + "', ";
        
        if(autor == null)
            retVal += "autor=null, ";
        else
            retVal += "autor='" + autor + "', ";
        
        if(materiais == null)
            retVal += "materiais=null, ";
        else
            retVal += "materiais='" + materiais + "', ";
        
        if(dataColocacaoUA == null)
            retVal += "dataColocacaoUA=null, ";
        else
            retVal += "dataColocacaoUA='" + dataColocacaoUA + "', ";
        
        if(coordenadas == null)
            retVal += "coordenadas=null";
        else
            retVal += "coordenadas='" + coordenadas + "'";
        
        retVal += "}";
        return retVal;
    }
    
    public String jsonStringWithId()
    {
        String retVal = "{id=" + id + ", ";
        
        if(nome == null)
            retVal += "nome=null, ";
        else
            retVal += "nome='" + nome + "', ";
        
        if(local == null)
            retVal += "local=null, ";
        else
            retVal += "local='" + local + "', ";
        
        if(dimensoes == null)
            retVal += "dimensoes=null, ";
        else
            retVal += "dimensoes='" + dimensoes + "', ";
        
        if(autor == null)
            retVal += "autor=null, ";
        else
            retVal += "autor='" + autor + "', ";
        
        if(materiais == null)
            retVal += "materiais=null, ";
        else
            retVal += "materiais='" + materiais + "', ";
        
        if(dataColocacaoUA == null)
            retVal += "dataColocacaoUA=null, ";
        else
            retVal += "dataColocacaoUA='" + dataColocacaoUA + "', ";
        
        if(coordenadas == null)
            retVal += "coordenadas=null";
        else
            retVal += "coordenadas='" + coordenadas + "'";
        
        retVal += "}";
        return retVal;
    }
    
}
