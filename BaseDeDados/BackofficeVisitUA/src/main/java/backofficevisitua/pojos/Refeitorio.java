package backofficevisitua.pojos;

/**
 * POJO Refeitório.
 */
public class Refeitorio {
    
    private Integer id;
    private String nome;
    private String coordenadas;
    
    private boolean editable;
    
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        if(coordenadas.equals(""))
            coordenadas=null;
        this.coordenadas = coordenadas;
    }

    @Override
    public String toString() {
        return "Refeitorio{" + "id=" + id + ", nome=" + nome + ", coordenadas=" + coordenadas + '}';
    }
    
    public String jsonString()
    {
        String retVal = "{nome='" + nome + "', ";
        
        if(coordenadas == null)
            retVal += "coordenadas=null";
        else
            retVal += "coordenadas='" + coordenadas + "'";
        
        retVal += "}";
        
        return retVal;
    }
    
    public String jsonStringWithId()
    {
        String retVal = "{id=" + id  + ", nome='" + nome + "', ";
        
        if(coordenadas == null)
            retVal += "coordenadas=null";
        else
            retVal += "coordenadas='" + coordenadas + "'";
        
        retVal += "}";
        
        return retVal;
    }
    
}
