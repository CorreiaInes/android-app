package backofficevisitua.pojos;

import java.util.*;

/**
 * POJO Edificio.
 */
public class Edificio {
    
    private String id;
    private String nome;
    private String tipo;
    private String sigla;
    private String dataConstrucao;
    private String descricao;
    private String coordenadas;
    private List<Ensino> ensino;
    private String arquiteto;
    
    private boolean editable;

    public Edificio() {
        ensino = new ArrayList<>();
    }
    
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        if(tipo.equals(""))
            tipo = null;
        this.tipo = tipo;
    }
    
    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        if(sigla.equals(""))
            sigla = null;
        this.sigla = sigla;
    }

    public String getDataConstrucao() {
        return dataConstrucao;
    }

    public void setDataConstrucao(String dataConstrucao) {
        if(dataConstrucao.equals(""))
            dataConstrucao = null;
        this.dataConstrucao = dataConstrucao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        if(descricao.equals(""))
            descricao = null;
        this.descricao = descricao;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        if(coordenadas.equals(""))
            coordenadas = null;
        this.coordenadas = coordenadas;
    }

    public List<Ensino> getEnsino() {
        return ensino;
    }

    public void setEnsino(Ensino ensino) {
        this.ensino.add(ensino);
    }
    
    public void removeEnsino(Ensino ensino) {
        this.ensino.remove(ensino);
    }

    public String getArquiteto() {
        return arquiteto;
    }

    public void setArquiteto(String arquiteto) {
        if(arquiteto.equals(""))
            arquiteto = null;
        this.arquiteto = arquiteto;
    }

    @Override
    public String toString() {
        return "Edificio{" + "id=" + id + ", nome=" + nome + ", tipo=" + tipo + ", sigla=" + sigla + ", dataConstrucao=" + dataConstrucao + ", descricao=" + descricao + ", coordenadas=" + coordenadas + ", ensino=" + ensino + ", arquiteto=" + arquiteto + '}';
    }
    
    public String getEnsinoString()
    {
        String retVal = "";
        
        for(Ensino e : ensino)
        {
            retVal += e.getTipo() + " " + e.getNome() + " | ";
        }
        
        return retVal;
    }
    
    public String jsonStringWithId()
    {
        String retVal = "{id=" + id + ", nome='" + nome + "', ";
        
        if(tipo == null)
            retVal += "tipo=null, ";
        else
            retVal += "tipo='" + tipo + "', ";
        
        if(sigla == null)
            retVal += "sigla=null, ";
        else
            retVal += "sigla='" + sigla + "', ";
        
        if(dataConstrucao == null)
            retVal += "dataConstrucao=null, ";
        else
            retVal += "dataConstrucao='" + dataConstrucao + "', ";
        
        if(descricao == null)
            retVal += "descricao=null, ";
        else
            retVal += "descricao='" + descricao + "', ";
        
        if(coordenadas == null)
            retVal += "coordenadas=null, ";
        else
            retVal += "coordenadas='" + coordenadas + "', ";
        
        if(arquiteto == null)
            retVal += "arquiteto=null";
        else
            retVal += "arquiteto='" + arquiteto + "'";
        
        retVal += "}";
        return retVal;
    }
    
}
