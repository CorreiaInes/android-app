package backofficevisitua.pojos;

/**
 * POJO Coleção.
 */
public class Colecao {
    
    private Integer id;
    private String nome;
    private String descricao;
    private String doador;
    private Integer colecaoMae;
    private String localVisita;
    private String coordenadasLocal;
    
    private boolean editable;
    
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        if(descricao.equals(""))
            descricao = null;
        this.descricao = descricao;
    }

    public String getDoador() {
        return doador;
    }
    
    public String getLocalVisita() {
        return localVisita;
    }
    
    public String getCoordenadasLocal() {
        return coordenadasLocal;
    }

    public void setDoador(String doador) {
        if(doador.equals(""))
            doador = null;
        this.doador = doador;
    }
    
    public Integer getColecaoMae() {
        return colecaoMae;
    }

    public void setColecaoMae(Integer colecaoMae) {
        if(colecaoMae != null && colecaoMae == 0)
            this.colecaoMae = null;
        else
            this.colecaoMae = colecaoMae;
    }
    
    public void setLocalVisita(String localVisita) {
        if(localVisita.equals(""))
            localVisita = null;
        this.localVisita = localVisita;
    }
    
    public void setCoordenadasLocal(String coordenadasLocal) {
        if(coordenadasLocal.equals(""))
            coordenadasLocal = null;
        this.coordenadasLocal = coordenadasLocal;
    }

    @Override
    public String toString() {
        return "Colecao{" + "id=" + id + ", nome=" + nome + ", descricao=" + descricao + ", doador=" + doador + ", colecaoMae=" + colecaoMae + ", localVisita=" + localVisita + ", coordenadasLocal=" + coordenadasLocal + '}';
    }
    
    public String jsonString()
    {
        String retVal = "{nome='" + nome + "', ";
        
        if(descricao == null)
            retVal += "descricao=null, ";
        else
            retVal += "descricao='" + descricao + "', ";
        
        if(doador == null)
            retVal += "doador=null, ";
        else
            retVal += "doador='" + doador + "', ";
        
        if(colecaoMae == null)
            retVal += "colecaoMae=null, ";
        else
            retVal += "colecaoMae='" + colecaoMae + "', ";
        
        if(localVisita == null)
            retVal += "localVisita=null, ";
        else
            retVal += "localVisita='" + localVisita + "', ";
        
        if(coordenadasLocal == null)
            retVal += "coordenadasLocal=null";
        else
            retVal += "coordenadasLocal='" + coordenadasLocal + "'";
        
        retVal += "}";
        return retVal;
    }
    
    public String jsonStringWithId()
    {
        String retVal = "{id=" + id + ", nome='" + nome + "', ";
        
        if(descricao == null)
            retVal += "descricao=null, ";
        else
            retVal += "descricao='" + descricao + "', ";
        
        if(doador == null)
            retVal += "doador=null, ";
        else
            retVal += "doador='" + doador + "', ";
        
        if(colecaoMae == null)
            retVal += "colecaoMae=null, ";
        else
            retVal += "colecaoMae=" + colecaoMae + ", ";
        
        if(localVisita == null)
            retVal += "localVisita=null, ";
        else
            retVal += "localVisita='" + localVisita + "', ";
        
        if(coordenadasLocal == null)
            retVal += "coordenadasLocal=null";
        else
            retVal += "coordenadasLocal='" + coordenadasLocal + "'";
        
        retVal += "}";
        return retVal;
    }
    
}
