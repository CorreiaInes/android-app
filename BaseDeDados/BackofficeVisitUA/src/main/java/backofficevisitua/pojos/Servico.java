package backofficevisitua.pojos;

/**
 * POJO Serviço.
 */
public class Servico {
    
    private int id;
    private String local;
    private String designacao;
    private String coordenadas;
    private String horario;
    
    private boolean editable;

    public Servico() {
    }
    
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        if(local.equals(""))
            local = null;
        this.local = local;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        if(coordenadas.equals(""))
            coordenadas = null;
        this.coordenadas = coordenadas;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        if(horario.equals(""))
            horario = null;
        this.horario = horario;
    }

    @Override
    public String toString() {
        return "Servico{" + "id=" + id + ", local=" + local + ", designacao=" + designacao + ", coordenadas=" + coordenadas + ", horario=" + horario + '}';
    }
    
    public String jsonString()
    {
        String retVal = "{";
        
        if(designacao == null)
            retVal += "designacao=null, ";
        else
            retVal += "designacao='" + designacao + "', ";
        
        if(local == null)
            retVal += "local=null, ";
        else
            retVal += "local='" + local + "', ";
        
        if(coordenadas == null)
            retVal += "coordenadas=null, ";
        else
            retVal += "coordenadas='" + coordenadas + "', ";
        
        if(horario == null)
            retVal += "horario=null";
        else
            retVal += "horario='" + horario + "'";
        
        retVal += "}";
        return retVal;
    }
    
    public String jsonStringWithId()
    {
        String retVal = "{id=" + id + ", ";
        
        if(designacao == null)
            retVal += "designacao=null, ";
        else
            retVal += "designacao='" + designacao + "', ";
        
        if(local == null)
            retVal += "local=null, ";
        else
            retVal += "local='" + local + "', ";
        
        if(coordenadas == null)
            retVal += "coordenadas=null, ";
        else
            retVal += "coordenadas='" + coordenadas + "', ";
        
        if(horario == null)
            retVal += "horario=null";
        else
            retVal += "horario='" + horario + "'";
        
        retVal += "}";
        return retVal;
    }
    
}
