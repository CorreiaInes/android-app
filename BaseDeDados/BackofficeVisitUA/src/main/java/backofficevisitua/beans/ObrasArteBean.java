package backofficevisitua.beans;

import backofficevisitua.helpers.Constants;
import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.ObraArte;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import org.primefaces.json.*;

@Named(value = "obrasArteBean")
@SessionScoped
public class ObrasArteBean implements Serializable {
    
    private List<ObraArte> obras;
    
    // Parâmetros para a adição de uma nova Obra de Arte
    private String nome;
    private String local;
    private String dimensoes;
    private String autor;
    private String materiais;
    private String dataColocacaoUA;
    private String coordenadas;

    @PostConstruct
    public void init()
    {
        getObrasArteFromWS();
    }
    
    private void getObrasArteFromWS()
    {
        obras = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "obrasarte");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ObraArte obra = (ObraArte) gson.fromJson(jsonObject.toString(), ObraArte.class);
                //System.out.println(obra);
                obras.add(obra);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<ObraArte> getObras() {
        return obras;
    }

    public void setObras(List<ObraArte> obras) {
        this.obras = obras;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDimensoes() {
        return dimensoes;
    }

    public void setDimensoes(String dimensoes) {
        this.dimensoes = dimensoes;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getMateriais() {
        return materiais;
    }

    public void setMateriais(String materiais) {
        this.materiais = materiais;
    }

    public String getDataColocacaoUA() {
        return dataColocacaoUA;
    }

    public void setDataColocacaoUA(String dataColocacaoUA) {
        this.dataColocacaoUA = dataColocacaoUA;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }
    
    public String saveAction()
    { 
        for (ObraArte obra : obras)
        {
            obra.setEditable(false);
            //System.out.println(obra);
            try
            {
                // Guardar as alterações na BD
                WSAccess.sendPost(Constants.BASE_URL + "update/obrasarte", obra.jsonStringWithId());
            }
            catch (Exception ex)
            {
                Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public String editAction(ObraArte obra)
    {
      obra.setEditable(true);
      return null;
    }
    
    public String deleteAction(ObraArte obra)
    {
        try
        {
            // Remover da BD
            WSAccess.sendGet(Constants.BASE_URL + "delete/obrasarte/" + obra.getId());
            obras.remove(obra);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "obrasarte";
    }
    
    public String addAction()
    {
        ObraArte obra = new ObraArte();
        obra.setNome(nome);
        obra.setLocal(local);
        obra.setDimensoes(dimensoes);
        obra.setAutor(autor);
        obra.setMateriais(materiais);
        obra.setDataColocacaoUA(dataColocacaoUA);
        obra.setCoordenadas(coordenadas);
        
        System.out.println(obra.jsonString());
        
        try
        {
            // Inserir na BD
            WSAccess.sendPost(Constants.BASE_URL + "insert/obrasarte", obra.jsonString());
            // Atualizar a lista a partir da BD para acertar os ids
            obras.removeAll(obras);
            getObrasArteFromWS();
            setNome(null);
            setLocal(null);
            setDimensoes(null);
            setAutor(null);
            setMateriais(null);
            setDataColocacaoUA(null);
            setCoordenadas(null);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "obrasarte";
    }
    
}