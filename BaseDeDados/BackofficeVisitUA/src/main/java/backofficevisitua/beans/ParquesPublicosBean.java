package backofficevisitua.beans;

import backofficevisitua.helpers.Constants;
import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.ParquePublico;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import org.primefaces.json.*;

@Named(value = "parquesPublicosBean")
@SessionScoped
public class ParquesPublicosBean implements Serializable {
    
    private List<ParquePublico> parques;
    
    // Parâmetros para a adição de um novo Parque Público
    private String nome;
    private String coordenadas;

    @PostConstruct
    public void init()
    {
        getParquesFromWS();
    }
    
    private void getParquesFromWS()
    {
        parques = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "parquespublicos");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ParquePublico parque = (ParquePublico) gson.fromJson(jsonObject.toString(), ParquePublico.class);
                //System.out.println(parque);
                parques.add(parque);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<ParquePublico> getParques() {
        return parques;
    }

    public void setParques(List<ParquePublico> parques) {
        this.parques = parques;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }
    
    public String saveAction()
    { 
        for (ParquePublico parque : parques)
        {
            parque.setEditable(false);
            //System.out.println(parque);
            try
            {
                // Guardar as alterações na BD
                WSAccess.sendPost(Constants.BASE_URL + "update/parquespublicos", parque.jsonStringWithId());
            }
            catch (Exception ex)
            {
                Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public String editAction(ParquePublico parque)
    {
      parque.setEditable(true);
      return null;
    }
    
    public String deleteAction(ParquePublico parque)
    {
        try
        {
            // Remover da BD
            WSAccess.sendGet(Constants.BASE_URL + "delete/parquespublicos/" + parque.getId());
            parques.remove(parque);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "parquespublicos";
    }
    
    public String addAction()
    {
        ParquePublico parque = new ParquePublico();
        parque.setNome(nome);
        parque.setCoordenadas(coordenadas);
        
        System.out.println(parque.jsonString());
        
        try
        {
            // Inserir na BD
            WSAccess.sendPost(Constants.BASE_URL + "insert/parquespublicos", parque.jsonString());
            // Atualizar a lista a partir da BD para acertar os ids
            parques.removeAll(parques);
            getParquesFromWS();
            setNome(null);
            setCoordenadas(null);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "parquespublicos";
    }
    
}
