package backofficevisitua.beans;

import backofficevisitua.helpers.Constants;
import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.*;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import org.primefaces.context.RequestContext;
import org.primefaces.json.*;
import org.apache.commons.codec.binary.Base64;

@Named(value = "imagensBean")
@SessionScoped
public class ImagensBean implements Serializable {
    
    private List<ObraArte> obras;
    private List<Edificio> edificios;
    private List<Colecao> colecoes;
    private List<Servico> servicos;
    
    private ObraArte obra; 
    private String obra_img_url;
    
    private Edificio edificio; 
    private String edificio_img_url;
    
    private Colecao colecao; 
    private String colecao_img_url;
    
    private Servico servico; 
    private String servico_img_url;
    
    private Part obra_img_file;
    private Part edificio_img_file;
    private Part colecao_img_file;
    private Part servico_img_file;
    
    @PostConstruct
    public void init()
    {
        getObrasArteFromWS();
        getEdificiosFromWS();
        getColecoesFromWS();
        getServicosFromWS();
    }
    
    private void getObrasArteFromWS()
    {
        obras = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "obrasarte");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ObraArte obra = (ObraArte) gson.fromJson(jsonObject.toString(), ObraArte.class);
                //System.out.println(obra);
                obras.add(obra);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void getEdificiosFromWS()
    {
        edificios = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "edificios");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Edificio edificio = (Edificio) gson.fromJson(jsonObject.toString(), Edificio.class);
                //System.out.println(edificio);
                edificios.add(edificio);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void getColecoesFromWS()
    {
        colecoes = new ArrayList<>();
        try
        {
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "colecoes");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                parseColecaoAddToList(jsonObject, null);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void parseColecaoAddToList(JSONObject jsonObject, Integer colecaoMae)
    {
        Gson gson = new Gson();
        Colecao colecao = (Colecao) gson.fromJson(jsonObject.toString(), Colecao.class);
        colecao.setColecaoMae(colecaoMae);
        //System.out.println(colecao);
        colecoes.add(colecao);
        JSONArray subColecoes = jsonObject.getJSONArray("subColecoes");
        for(int j = 0; j < subColecoes.length(); j++)
        {
            JSONObject temp = subColecoes.getJSONObject(j);
            parseColecaoAddToList(temp, colecao.getId());
        }
    }
    
    private void getServicosFromWS()
    {
        servicos = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "servicos");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Servico servico = (Servico) gson.fromJson(jsonObject.toString(), Servico.class);
                //System.out.println(servico);
                servicos.add(servico);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<ObraArte> getObras() {
        return obras;
    }
    
    public void setObras(List<ObraArte> obras) {
        this.obras = obras;
    }

    public ObraArte getObra() {
        return obra;
    }

    public void setObra(ObraArte obra) {
        this.obra = obra;
    }

    public String getObra_img_url() {
        if(obra != null)
        {
            obra_img_url = Constants.BASE_URL + "obrasarte/imagem/" + obra.getId();
            System.out.println("Obra Image URL: " + obra_img_url);
        }
        return obra_img_url;
    }

    public void setObra_img_url(String obra_img_url) {
        this.obra_img_url = obra_img_url;
    }
    
    public Map<String, ObraArte> getAvailableObras()
    {
        Map<String, ObraArte> available = new TreeMap<>();
        for(ObraArte o : obras)
            available.put(o.getId() + ": " + o.getNome(), o);
        return available;
    }
    
    public List<Edificio> getEdificios() {
        return edificios;
    }

    public void setEdificios(List<Edificio> edificios) {
        this.edificios = edificios;
    }

    public Edificio getEdificio() {
        return edificio;
    }

    public void setEdificio(Edificio edificio) {
        this.edificio = edificio;
    }

    public String getEdificio_img_url() {
        if(edificio != null)
        {
            edificio_img_url = Constants.BASE_URL + "edificios/imagem/" + edificio.getId();
            System.out.println("Edificio Image URL: " + edificio_img_url);
        }
        return edificio_img_url;
    }

    public void setEdificio_img_url(String edificio_img_url) {
        this.edificio_img_url = edificio_img_url;
    }
    
    public Map<String, Edificio> getAvailableEdificios()
    {
        Map<String, Edificio> available = new TreeMap<>();
        for(Edificio e : edificios)
            available.put(e.getId() + ": " + e.getNome(), e);
        return available;
    }
    
    public Map<String, Colecao> getAvailableColecoes()
    {
        Map<String, Colecao> available = new TreeMap<>();
        for(Colecao c : colecoes)
            available.put(c.getId() + ": " + c.getNome(), c);
        return available;
    }
    
    public Map<String, Servico> getAvailableServicos()
    {
        Map<String, Servico> available = new TreeMap<>();
        for(Servico s : servicos)
            available.put(s.getId() + ": " + s.getDesignacao(), s);
        return available;
    }

    public List<Colecao> getColecoes() {
        return colecoes;
    }

    public void setColecoes(List<Colecao> colecoes) {
        this.colecoes = colecoes;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }

    public Colecao getColecao() {
        return colecao;
    }

    public void setColecao(Colecao colecao) {
        this.colecao = colecao;
    }

    public String getColecao_img_url() {
        if(colecao != null)
        {
            colecao_img_url = Constants.BASE_URL + "colecoes/imagem/" + colecao.getId();
            System.out.println("Colecao Image URL: " + colecao_img_url);
        }
        return colecao_img_url;
    }

    public void setColecao_img_url(String colecao_img_url) {
        this.colecao_img_url = colecao_img_url;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public String getServico_img_url() {
        if(servico != null)
        {
            servico_img_url = Constants.BASE_URL + "servicos/imagem/" + servico.getId();
            System.out.println("Servico Image URL: " + servico_img_url);
        }
        return servico_img_url;
    }

    public void setServico_img_url(String servico_img_url) {
        this.servico_img_url = servico_img_url;
    }
    
    public Part getObra_img_file() {
        return obra_img_file;
    }

    public void setObra_img_file(Part obra_img_file) {
        this.obra_img_file = obra_img_file;
    }

    public String uploadObraImg()
    {
        try
        {
            InputStream imgInput = obra_img_file.getInputStream();
            Files.copy(imgInput, new File("./obra_temp_file.png").toPath(), StandardCopyOption.REPLACE_EXISTING);
            
            BufferedImage img = ImageIO.read(new File("./obra_temp_file.png"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            String imgData = Base64.encodeBase64String(imgBytes);
            
            WSAccess.sendPostImage(Constants.BASE_URL + "insert/imagem/obrasarte/" + obra.getId(), imgData);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            RequestContext.getCurrentInstance().execute("alert('Erro ao carregar a imagem.');");
        }
        
        getObra_img_url();
        return "imagens.xhtml?faces-redirect=true";
    }

    public Part getEdificio_img_file() {
        return edificio_img_file;
    }

    public void setEdificio_img_file(Part edificio_img_file) {
        this.edificio_img_file = edificio_img_file;
    }
    
    public String uploadEdificioImg()
    {
        try
        {
            InputStream imgInput = edificio_img_file.getInputStream();
            Files.copy(imgInput, new File("./edificio_temp_file.png").toPath(), StandardCopyOption.REPLACE_EXISTING);
            
            BufferedImage img = ImageIO.read(new File("./edificio_temp_file.png"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            String imgData = Base64.encodeBase64String(imgBytes);
            baos.close();
            
            WSAccess.sendPostImage(Constants.BASE_URL + "insert/imagem/edificios/" + edificio.getId(), imgData);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            RequestContext.getCurrentInstance().execute("alert('Erro ao carregar a imagem.');");
        }
        
        getEdificio_img_url();
        return "imagens.xhtml?faces-redirect=true";
    }

    public Part getColecao_img_file() {
        return colecao_img_file;
    }

    public void setColecao_img_file(Part colecao_img_file) {
        this.colecao_img_file = colecao_img_file;
    }
    
    public String uploadColecaoImg()
    {
        try
        {
            InputStream imgInput = colecao_img_file.getInputStream();
            Files.copy(imgInput, new File("./colecao_temp_file.png").toPath(), StandardCopyOption.REPLACE_EXISTING);
            
            BufferedImage img = ImageIO.read(new File("./colecao_temp_file.png"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            String imgData = Base64.encodeBase64String(imgBytes);
            baos.close();
            
            WSAccess.sendPostImage(Constants.BASE_URL + "insert/imagem/colecoes/" + colecao.getId(), imgData);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            RequestContext.getCurrentInstance().execute("alert('Erro ao carregar a imagem.');");
        }
        
        getEdificio_img_url();
        return "imagens.xhtml?faces-redirect=true";
    }

    public Part getServico_img_file() {
        return servico_img_file;
    }

    public void setServico_img_file(Part servico_img_file) {
        this.servico_img_file = servico_img_file;
    }
    
    public String uploadServicoImg()
    {
        try
        {
            InputStream imgInput = servico_img_file.getInputStream();
            Files.copy(imgInput, new File("./servico_temp_file.png").toPath(), StandardCopyOption.REPLACE_EXISTING);
            
            BufferedImage img = ImageIO.read(new File("./servico_temp_file.png"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", baos);
            byte[] imgBytes = baos.toByteArray();
            String imgData = Base64.encodeBase64String(imgBytes);
            baos.close();
            
            WSAccess.sendPostImage(Constants.BASE_URL + "insert/imagem/servicos/" + servico.getId(), imgData);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            RequestContext.getCurrentInstance().execute("alert('Erro ao carregar a imagem.');");
        }
        
        getEdificio_img_url();
        return "imagens.xhtml?faces-redirect=true";
    }
    
}
