package backofficevisitua.beans;

import backofficevisitua.helpers.Constants;
import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.Ensino;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import org.primefaces.json.*;

@Named(value = "ensinosBean")
@SessionScoped
public class EnsinosBean implements Serializable {
    
    private List<Ensino> ensinos;
    
    // Parâmetros para a adição de um novo Ensino
    private String nome;
    private String tipo;

    @PostConstruct
    public void init()
    {
        getEnsinosFromWS();
    }
    
    private void getEnsinosFromWS()
    {
        ensinos = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "ensinos");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Ensino ensino = (Ensino) gson.fromJson(jsonObject.toString(), Ensino.class);
                //System.out.println(ensino);
                ensinos.add(ensino);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> getTipos()
    {
        List<String> tipos = new ArrayList<>();
        for(Ensino e : ensinos)
            if(e.getTipo() != null && !tipos.contains(e.getTipo()))
                tipos.add(e.getTipo());
        return tipos;
    }
    
    public List<Ensino> getEnsinos() {
        return ensinos;
    }

    public void setEnsinos(List<Ensino> ensinos) {
        this.ensinos = ensinos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public String deleteAction(Ensino ensino)
    {
        try
        {
            // Remover da BD
            WSAccess.sendGet(Constants.BASE_URL + "delete/ensinos/" + ensino.getId());
            ensinos.remove(ensino);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "ensinos";
    }
    
    public String addAction()
    {
        Ensino ensino = new Ensino();
        ensino.setNome(nome);
        ensino.setTipo(tipo);
        
        System.out.println(ensino.jsonString());
        
        try
        {
            // Inserir na BD
            WSAccess.sendPost(Constants.BASE_URL + "insert/ensinos", ensino.jsonString());
            // Atualizar a lista a partir da BD para acertar os ids
            ensinos.removeAll(ensinos);
            getEnsinosFromWS();
            setNome(null);
            setTipo(null);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "ensinos";
    }
    
}
