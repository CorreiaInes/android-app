package backofficevisitua.beans;

import backofficevisitua.helpers.Constants;
import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.Servico;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import org.primefaces.json.*;

@Named(value = "servicosBean")
@SessionScoped
public class ServicosBean implements Serializable {
    
    private List<Servico> servicos;
    
    // Parâmetros para a adição de um novo Serviço
    private String local;
    private String designacao;
    private String coordenadas;
    private String horario;

    @PostConstruct
    public void init()
    {
        getServicosFromWS();
    }
    
    private void getServicosFromWS()
    {
        servicos = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "servicos");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Servico servico = (Servico) gson.fromJson(jsonObject.toString(), Servico.class);
                //System.out.println(servico);
                servicos.add(servico);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
    
    public String saveAction()
    { 
        for (Servico servico : servicos)
        {
            servico.setEditable(false);
            //System.out.println(servico);
            try
            {
                // Guardar as alterações na BD
                WSAccess.sendPost(Constants.BASE_URL + "update/servicos", servico.jsonStringWithId());
            }
            catch (Exception ex)
            {
                Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public String editAction(Servico servico)
    {
      servico.setEditable(true);
      return null;
    }
    
    public String deleteAction(Servico servico)
    {
        try
        {
            // Remover da BD
            WSAccess.sendGet(Constants.BASE_URL + "delete/servicos/" + servico.getId());
            servicos.remove(servico);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "servicos";
    }
    
    public String addAction()
    {
        Servico servico = new Servico();
        servico.setLocal(local);
        servico.setCoordenadas(coordenadas);
        servico.setDesignacao(designacao);
        servico.setHorario(horario);
        System.out.println(servico.jsonString());
        
        try
        {
            // Inserir na BD
            WSAccess.sendPost(Constants.BASE_URL + "insert/servicos", servico.jsonString());
            // Atualizar a lista a partir da BD para acertar os ids
            servicos.removeAll(servicos);
            getServicosFromWS();
            setLocal(null);
            setCoordenadas(null);
            setDesignacao(null);
            setHorario(null);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "servicos";
    }
    
}
