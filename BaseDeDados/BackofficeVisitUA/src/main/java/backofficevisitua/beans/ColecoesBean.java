package backofficevisitua.beans;

import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.Colecao;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import org.primefaces.json.*;
import backofficevisitua.helpers.Constants;

@Named(value = "colecoesBean")
@SessionScoped
public class ColecoesBean implements Serializable {
    
    private List<Colecao> colecoes;
    
    // Parâmetros para a adição de uma nova Coleção
    private String nome;
    private String descricao;
    private String doador;
    private Integer colecaoMae;
    private String localVisita;
    private String coordenadasLocal;

    @PostConstruct
    public void init()
    {
        getColecoesFromWS();
    }
    
    private void getColecoesFromWS()
    {
        colecoes = new ArrayList<>();
        try
        {
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "colecoes");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                parseColecaoAddToList(jsonObject, null);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void parseColecaoAddToList(JSONObject jsonObject, Integer colecaoMae)
    {
        Gson gson = new Gson();
        Colecao colecao = (Colecao) gson.fromJson(jsonObject.toString(), Colecao.class);
        colecao.setColecaoMae(colecaoMae);
        //System.out.println(colecao);
        colecoes.add(colecao);
        JSONArray subColecoes = jsonObject.getJSONArray("subColecoes");
        for(int j = 0; j < subColecoes.length(); j++)
        {
            JSONObject temp = subColecoes.getJSONObject(j);
            parseColecaoAddToList(temp, colecao.getId());
        }
    }
    
    public List<Colecao> getColecoes() {
        return colecoes;
    }

    public Map<String, Integer> getColecoesIds()
    {
        Map<String, Integer> ids = new TreeMap<>();
        ids.put("Nenhuma", 0);
        for(Colecao c : colecoes)
            ids.put(c.getNome(), c.getId());
        return ids;
    }
    
    public Map<String, Integer> getOtherColecoesIds(Colecao colecao)
    {
        Map<String, Integer> ids = new TreeMap<>();
        ids.put("Nenhuma", 0);
        for(Colecao c : colecoes)
            if(c.getId() != colecao.getId())
                ids.put(c.getNome(), c.getId());
        return ids;
    }

    public void setColecoes(List<Colecao> colecoes) {
        this.colecoes = colecoes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDoador() {
        return doador;
    }

    public void setDoador(String doador) {
        this.doador = doador;
    }

    public Integer getColecaoMae() {
        return colecaoMae;
    }

    public void setColecaoMae(Integer colecaoMae) {
        if(colecaoMae == 0)
            this.colecaoMae = null;
        else
            this.colecaoMae = colecaoMae;
    }

    public String getLocalVisita() {
        return localVisita;
    }

    public void setLocalVisita(String localVisita) {
        this.localVisita = localVisita;
    }

    public String getCoordenadasLocal() {
        return coordenadasLocal;
    }

    public void setCoordenadasLocal(String coordenadasLocal) {
        this.coordenadasLocal = coordenadasLocal;
    }

    public String saveAction()
    { 
        for (Colecao colecao : colecoes)
        {
            colecao.setEditable(false);
            //System.out.println(servico);
            try
            {
                // Guardar as alterações na BD
                WSAccess.sendPost(Constants.BASE_URL + "update/colecoes", colecao.jsonStringWithId());
            }
            catch (Exception ex)
            {
                Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public String editAction(Colecao colecao)
    {
      colecao.setEditable(true);
      return null;
    }
    
    public String deleteAction(Colecao colecao)
    {
        try
        {
            // Remover da BD
            WSAccess.sendGet(Constants.BASE_URL + "delete/colecoes/" + colecao.getId());
            colecoes.remove(colecao);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "colecoes";
    }
    
    public String addAction()
    {
        Colecao colecao = new Colecao();
        colecao.setNome(nome);
        colecao.setDescricao(descricao);
        colecao.setDoador(doador);
        colecao.setColecaoMae(colecaoMae);
        colecao.setLocalVisita(localVisita);
        colecao.setCoordenadasLocal(coordenadasLocal);
        System.out.println(colecao.jsonString());
        
        try
        {
            // Inserir na BD
            WSAccess.sendPost(Constants.BASE_URL + "insert/colecoes", colecao.jsonString());
            // Atualizar a lista a partir da BD para acertar os ids
            colecoes.removeAll(colecoes);
            getColecoesFromWS();
            setNome(null);
            setDescricao(null);
            setDoador(null);
            setColecaoMae(null);
            setLocalVisita(null);
            setCoordenadasLocal(null);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "colecoes";
    }
    
}
