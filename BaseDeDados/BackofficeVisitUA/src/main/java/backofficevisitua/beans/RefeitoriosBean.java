package backofficevisitua.beans;

import backofficevisitua.helpers.Constants;
import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.Refeitorio;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import org.primefaces.json.*;

@Named(value = "refeitoriosBean")
@SessionScoped
public class RefeitoriosBean implements Serializable {
    
    private List<Refeitorio> refeitorios;
    
    // Parâmetros para a adição de um novo Refeitório
    private String nome;
    private String coordenadas;

    @PostConstruct
    public void init()
    {
        getRefeitoriosFromWS();
    }
    
    private void getRefeitoriosFromWS()
    {
        refeitorios = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "refeitorios");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Refeitorio refeitorio = (Refeitorio) gson.fromJson(jsonObject.toString(), Refeitorio.class);
                //System.out.println(refeitorio);
                refeitorios.add(refeitorio);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Refeitorio> getRefeitorios() {
        return refeitorios;
    }

    public void setRefeitorios(List<Refeitorio> refeitorios) {
        this.refeitorios = refeitorios;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }
    
    public String saveAction()
    { 
        for (Refeitorio refeitorio : refeitorios)
        {
            refeitorio.setEditable(false);
            //System.out.println(refeitorio);
            try
            {
                // Guardar as alterações na BD
                WSAccess.sendPost(Constants.BASE_URL + "update/refeitorios", refeitorio.jsonStringWithId());
            }
            catch (Exception ex)
            {
                Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public String editAction(Refeitorio refeitorio)
    {
      refeitorio.setEditable(true);
      return null;
    }
    
    public String deleteAction(Refeitorio refeitorio)
    {
        try
        {
            // Remover da BD
            WSAccess.sendGet(Constants.BASE_URL + "delete/refeitorios/" + refeitorio.getId());
            refeitorios.remove(refeitorio);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "refeitorios";
    }
    
    public String addAction()
    {
        Refeitorio refeitorio = new Refeitorio();
        refeitorio.setNome(nome);
        refeitorio.setCoordenadas(coordenadas);
        
        System.out.println(refeitorio.jsonString());
        
        try
        {
            // Inserir na BD
            WSAccess.sendPost(Constants.BASE_URL + "insert/refeitorios", refeitorio.jsonString());
            // Atualizar a lista a partir da BD para acertar os ids
            refeitorios.removeAll(refeitorios);
            getRefeitoriosFromWS();
            setNome(null);
            setCoordenadas(null);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "refeitorios";
    }
    
}
