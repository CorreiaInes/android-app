package backofficevisitua.beans;

import backofficevisitua.helpers.Constants;
import backofficevisitua.helpers.WSAccess;
import backofficevisitua.pojos.*;
import java.util.*;
import java.util.logging.*;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.*;
import com.google.gson.Gson;
import org.apache.commons.httpclient.util.URIUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.json.*;

@Named(value = "edificiosBean")
@SessionScoped
public class EdificiosBean implements Serializable {
    
    private List<Edificio> edificios;
    
    // Parâmetros para adicionar ou remover um Ensino a um Edifício
    private Ensino newEnsino;
    private Edificio edificioAdd;
    
    private Ensino oldEnsino;
    private Edificio edificioRemove;
    
    // Parâmetros para a adição de um novo Edifício
    private String id;
    private String nome;
    private String tipo;
    private String sigla;
    private String dataConstrucao;
    private String descricao;
    private String coordenadas;
    private String arquiteto;
    
    @PostConstruct
    public void init()
    {
        getEdificiosFromWS();
    }
    
    private void getEdificiosFromWS()
    {
        List<Ensino> allEnsinos = getAllEnsinos();
        edificios = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "edificios");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Edificio edificio = (Edificio) gson.fromJson(jsonObject.toString(), Edificio.class);
                
                // Obter os ensinos para cada Edifício
                String ensinosEdificiosJson = WSAccess.sendGet(Constants.BASE_URL + "edificiosensinos");
                JSONArray ensinosEdificiosJsonArray = new JSONArray(ensinosEdificiosJson);
                for (int j = 0; j < ensinosEdificiosJsonArray.length(); j++)
                {
                    JSONObject ensinosEdificiosJsonObject = ensinosEdificiosJsonArray.getJSONObject(j);
                    EdificioEnsino edificioEnsino = (EdificioEnsino) gson.fromJson(ensinosEdificiosJsonObject.toString(), EdificioEnsino.class);
                    if(edificio.getId().equals(edificioEnsino.getIdEdificio()))
                    {
                        for(Ensino e : allEnsinos)
                            if(e.getId() == edificioEnsino.getIdEnsino())
                                edificio.setEnsino(e);
                    }
                }
                
                System.out.println(edificio);
                edificios.add(edificio);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private List<Ensino> getAllEnsinos()
    {
        List<Ensino> allEnsinos = new ArrayList<>();
        try
        {
            Gson gson = new Gson();
            String jsonString = WSAccess.sendGet(Constants.BASE_URL + "ensinos");
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Ensino e = (Ensino) gson.fromJson(jsonObject.toString(), Ensino.class);
                //System.out.println(e);
                allEnsinos.add(e);
            }
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return allEnsinos;
    }
    
    public Map<String, Ensino> getAddAvailableEnsino()
    {
        List<Integer> ensinosIdsEdificio = new ArrayList<>();
        for(Ensino e : edificioAdd.getEnsino())
            ensinosIdsEdificio.add(e.getId());
        
        Map<String, Ensino> available = new TreeMap<>();
        List<Ensino> all = getAllEnsinos();
        for(Ensino e : all)
        {
            if(!ensinosIdsEdificio.contains(e.getId()))
                available.put(e.getTipo() + " " + e.getNome(), e);
        }
        
        return available;
    }
    
    public Map<String, Ensino> getRemoveAvailableEnsino()
    {
        List<Integer> ensinosIdsEdificio = new ArrayList<>();
        for(Ensino e : edificioRemove.getEnsino())
            ensinosIdsEdificio.add(e.getId());
        
        Map<String, Ensino> available = new TreeMap<>();
        List<Ensino> all = getAllEnsinos();
        for(Ensino e : all)
        {
            if(ensinosIdsEdificio.contains(e.getId()))
                available.put(e.getTipo() + " " + e.getNome(), e);
        }
        
        return available;
    }
    
    public Map<String, Edificio> getAvailableEdificios()
    {
        Map<String, Edificio> available = new TreeMap<>();
        List<Edificio> all = getEdificios();
        for(Edificio e : all)
        {
            available.put(e.getNome(), e);
        }
        
        return available;
    }

    public Ensino getNewEnsino() {
        return newEnsino;
    }

    public void setNewEnsino(Ensino newEnsino) {
        this.newEnsino = newEnsino;
    }
    
    public Ensino getOldEnsino() {
        return oldEnsino;
    }

    public void setOldEnsino(Ensino oldEnsino) {
        this.oldEnsino = oldEnsino;
    }

    public Edificio getEdificioAdd() {
        return edificioAdd;
    }

    public void setEdificioAdd(Edificio edificio) {
        this.edificioAdd = edificio;
    }
    
    public Edificio getEdificioRemove() {
        return edificioRemove;
    }

    public void setEdificioRemove(Edificio edificio) {
        this.edificioRemove = edificio;
    }

    public List<Edificio> getEdificios() {
        return edificios;
    }

    public void setEdificios(List<Edificio> edificios) {
        this.edificios = edificios;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDataConstrucao() {
        return dataConstrucao;
    }

    public void setDataConstrucao(String dataConstrucao) {
        this.dataConstrucao = dataConstrucao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(String coordenadas) {
        this.coordenadas = coordenadas;
    }

    public String getArquiteto() {
        return arquiteto;
    }

    public void setArquiteto(String arquiteto) {
        this.arquiteto = arquiteto;
    }
    
    public String addEnsinoToEdificio()
    {
        EdificioEnsino ee = new EdificioEnsino();
        ee.setIdEdificio(edificioAdd.getId());
        ee.setIdEnsino(newEnsino.getId());
        try
        {
            WSAccess.sendPost(Constants.BASE_URL + "insert/edificiosensinos", ee.jsonString());
        }
        catch (Exception ex)
        {
            Logger.getLogger(EdificiosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        edificios.removeAll(edificios);
        getEdificiosFromWS();
        setEdificioAdd(null);
        setNewEnsino(null);
        return "edificios";
    }
    
    public String removeEnsinoFromEdificio()
    {
        try
        {
            WSAccess.sendGet(Constants.BASE_URL + "delete/edificiosensinos/" + edificioRemove.getId() + "/" + oldEnsino.getId());
        }
        catch (Exception ex)
        {
            Logger.getLogger(EdificiosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        edificios.removeAll(edificios);
        getEdificiosFromWS();
        setEdificioRemove(null);
        setOldEnsino(null);
        return "edificios";
    }
    
    public String saveAction()
    { 
        for (Edificio edificio : edificios)
        {
            edificio.setEditable(false);
            //System.out.println(edificioAdd);
            
            try
            {
                // Guardar as alterações na BD
                WSAccess.sendPost(Constants.BASE_URL + "update/edificios", edificio.jsonStringWithId());
            }
            catch (Exception ex)
            {
                Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
    
    public String editAction(Edificio edificio)
    {
      edificio.setEditable(true);
      return null;
    }
    
    public String deleteAction(Edificio edificio)
    {
        try
        {
            // Remover da BD
            String url = Constants.BASE_URL + "delete/edificios/" + edificio.getId() + "/" + URIUtil.encodeWithinQuery(edificio.getNome());
            WSAccess.sendGet(url);
            edificios.remove(edificio);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "edificios";
    }
    
    private boolean areIdAndNomeTaken()
    {
        for(Edificio e : edificios)
            if(e.getId().equals(id) && e.getNome().equals(nome))
                return true;
        return false;
    }
    
    public String addAction()
    {
        if(areIdAndNomeTaken())
            RequestContext.getCurrentInstance().execute("alert('\"alert('O par Id=" + id + " e Nome=" + nome + " não está disponível.');");
        
        Edificio edificio = new Edificio();
        edificio.setId(id);
        edificio.setNome(nome);
        edificio.setTipo(tipo);
        edificio.setSigla(sigla);
        edificio.setDataConstrucao(dataConstrucao);
        edificio.setDescricao(descricao);
        edificio.setCoordenadas(coordenadas);
        edificio.setArquiteto(arquiteto);
        System.out.println(edificio.jsonStringWithId());
        
        try
        {
            // Inserir na BD
            WSAccess.sendPost(Constants.BASE_URL + "insert/edificios", edificio.jsonStringWithId());
            // Atualizar a lista a partir da BD para acertar os ids
            edificios.removeAll(edificios);
            getEdificiosFromWS();
            setId(null);
            setNome(null);
            setTipo(null);
            setSigla(null);
            setDataConstrucao(null);
            setDescricao(null);
            setCoordenadas(null);
            setArquiteto(null);
        }
        catch (Exception ex)
        {
            Logger.getLogger(ServicosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "edificios";
    }
    
}
