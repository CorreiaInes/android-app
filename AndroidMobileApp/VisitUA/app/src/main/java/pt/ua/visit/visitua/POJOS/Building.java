package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * POJO Edifício.
 */
public class Building implements Serializable {

    private String id;          // Número de identificação
    private String type;        // Tipo
    private String name;        // Nome
    private String initials;    // Inicias
    private String date;        // Data de construção
    private String description; // Descrição
    private String coordinates; // Coordenadas
    private String architect;   // Nome do arquiteto
    private List<Area> areas;   // Lista de Áreas de Ensino

    public Building() {
        areas = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getArchitect() {
        return architect;
    }

    public void setArchitect(String architect) {
        this.architect = architect;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setArea(Area area) {
        this.areas.add(area);
    }

    @Override
    public String toString() {
        return name;
    }

    public String toStringAll() {
        return "Building{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", initials='" + initials + '\'' +
                ", date='" + date + '\'' +
                ", description='" + description + '\'' +
                ", coordinates='" + coordinates + '\'' +
                ", area=" + areas +
                '}';
    }

}
