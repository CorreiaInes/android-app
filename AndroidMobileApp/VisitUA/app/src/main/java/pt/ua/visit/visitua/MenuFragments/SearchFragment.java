package pt.ua.visit.visitua.MenuFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pt.ua.visit.visitua.OtherFragments.BuildingsFragment;
import pt.ua.visit.visitua.OtherFragments.SearchByPhotoFragment;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página de Pesquisa.
 */
public class SearchFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    /**
     * Retorna uma instância de SearchFragment.
     * @return instância de SearchFragment.
     */
    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_search, container, false);

        Button searchbyidbutton = view.findViewById(R.id.searchbyidbutton);

        // Listener invocado ao clicar no botão de Pesquisa por Texto
        searchbyidbutton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar o Utilizador para a Página dos Edifícios
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, BuildingsFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button searchbyphotobutton = view.findViewById(R.id.searchbyphotobutton);

        // Listener invocado ao clicar no botão de Pesquisa por Fotografia
        searchbyphotobutton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar o Utilizador para a Página de Pesquisa por Fotografia
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, SearchByPhotoFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

}
