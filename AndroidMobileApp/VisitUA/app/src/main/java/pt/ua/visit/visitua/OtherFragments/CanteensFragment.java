package pt.ua.visit.visitua.OtherFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.POJOS.Canteen;
import pt.ua.visit.visitua.POJOS.Refectory;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a Página dos Refeitórios.
 */
public class CanteensFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém os dados do WS das Ementas
    private getMeals getMeals;

    // Tarefa que executa em 2º plano e que obtém as coordenadas dos Refeitórios a partir do WS dos Refeitórios
    private getRefectories getRefectories;

    // Lista de POJO Refeitório
    private List<Refectory> refectories = new ArrayList<>();

    // Componentes que servem para cancelar a tarefa getMeals,
    // se esta demorar mais de GET_MEALS_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de CanteensFragment.
     * @return instância de CanteensFragment
     */
    public static CanteensFragment newInstance() {
        CanteensFragment fragment = new CanteensFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_canteens, container, false);

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS das Ementas e do WS dos Refeitórios
        getMeals = new getMeals();
        getRefectories = new getRefectories();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getMeals, getActivity(), getString(R.string.taskCancelerText));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);
        getRefectories.execute(this.getActivity());
        getMeals.execute(this.getActivity());

        return view;
    }

    /**
     * Tarefa que obtém os dados das Ementas a partir do WS, em 2º plano.
     */
    private class getMeals extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(CanteensFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS das Ementas
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.ementasua));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }

            // Senão, processar a resposta
            processMealsTaskResults(jsonResponse);
        }
    }

    /**
     * Tarefa que executa em 2º plano e que obtém os dados do WS dos Refeitórios
     */
    private class getRefectories extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(CanteensFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS das Ementas
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.refeitorios));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processRefectoriesTaskResults(jsonResponse);
        }
    }

    private void processRefectoriesTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        refectories = parser.parseRefectories(results);
    }

    private void processMealsTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        final List<Canteen> canteens = parser.parseCanteenAndMeal(results);

        if(canteens.size() == 0 || refectories.size() == 0)
        {
            // Indicar que a tarefa falhou
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        // Obter uma lista com os nomes dos Refeitórios
        final List<String> canteensNames = new ArrayList<>();
        for(Canteen canteen:  canteens)
        {
            if(canteen.getName() != null){
                canteensNames.add(canteen.getName());
            }
            for(Refectory r : refectories)
            {
                if(r.getName().equals(canteen.getName()))
                    canteen.setCoordinates(r.getCoordinates());
            }
        }

        // Inserir os nomes dos Refeitórios na lista de Refeitórios
        final ArrayAdapter<String> canteensAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, canteensNames);
        ListView canteensListView = view.findViewById(R.id.canteenList);
        canteensListView.setAdapter(canteensAdapter);

        // Listener ativado quando um Refeitório é selecionado
        canteensListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View item, int position, long rowId){
                        Canteen canteen = null;
                        // Obter o Refeitório selecionado com base na sua posição na lista
                        for(Canteen c : canteens){
                            if(c.getName().equalsIgnoreCase(canteensAdapter.getItem(position))){
                                canteen = c;
                            }
                        }

                        // Redirecionar para o Fragmento com as Refeições servidas no Refeitório selecionado
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_layout, MealFragment.newInstance(canteen));
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
        );
    }
}