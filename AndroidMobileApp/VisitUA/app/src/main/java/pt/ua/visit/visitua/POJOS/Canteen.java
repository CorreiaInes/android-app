package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * POJO Cantina.
 */
public class Canteen implements Serializable {

    private String name;           // Nome
    private List<Meal> meals;      // Lista de Refeições
    private String coordinates;    // Coordenadas

    public Canteen()
    {
        meals = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeal(Meal meal){
        meals.add(meal);
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "Canteen{" +
                "name='" + name + '\'' +
                ", meals=" + meals +
                ", coordinates=" + coordinates +
                '}';
    }

}