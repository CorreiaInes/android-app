package pt.ua.visit.visitua.HelperClasses;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

/**
 * Classe que cancela tarefas.
 */
public class TaskCanceler implements Runnable {

    // Tarefa a cancelas
    private AsyncTask task;

    // Atividade que invocou a tarefa
    private Activity activity;

    // Mensagem a transmitir após o cancelamento da tarefa
    private String message;

    public TaskCanceler(AsyncTask task, Activity activity, String message) {
        this.task = task;
        this.activity = activity;
        this.message = message;
    }

    @Override
    public void run() {
        if (task.getStatus() == AsyncTask.Status.RUNNING )
            task.cancel(true);
        if(message != null)
            Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }
}
