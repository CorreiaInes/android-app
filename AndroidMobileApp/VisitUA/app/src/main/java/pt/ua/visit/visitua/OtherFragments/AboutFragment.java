package pt.ua.visit.visitua.OtherFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página Sobre a Aplicação.
 */
public class AboutFragment extends Fragment {

    /**
     * Retorna uma instância de AboutFragment.
     * @return instância de AboutFragment.
     */
    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        return view;
    }
}