package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;

/**
 * POJO Obras de Arte.
 */
public class Art implements Serializable {

    private Integer id;         // Número de identificação
    private String name;        // Nome
    private String place;       // Localização
    private String author;      // Nome do Autor
    private String materials;   // Materiais utilizados
    private String size;        // Tamanho
    private String date;        // Data
    private String coordinates; // Coordenadas

    public Art() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getMaterials() {
        return materials;
    }

    public void setMaterials(String materials) {
        this.materials = materials;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }


    @Override
    public String toString() {
        return name;
    }

    public String toStringAll() {
        return "Art{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", place='" + place + '\'' +
                ", author='" + author + '\'' +
                ", materials='" + materials + '\'' +
                ", size='" + size + '\'' +
                ", date='" + date + '\'' +
                ", coordinates='" + coordinates + '\'' +
                '}';
    }

}
