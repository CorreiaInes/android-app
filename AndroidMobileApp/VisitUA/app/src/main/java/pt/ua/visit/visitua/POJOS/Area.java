package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;

/**
 * POJO Área de Ensino.
 */
public class Area implements Serializable {

    private String id;      // Número de identificação
    private String type;    // Tipo
    private String name;    // Nome

    public Area(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Area{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

}
