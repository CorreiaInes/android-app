package pt.ua.visit.visitua.OtherFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import pt.ua.visit.visitua.Activities.HomeActivity;
import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.POJOS.Turn;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a Página dos Serviços Académicos.
 */
public class AcademicServicesFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    // Lista de Senhas
    private List<Turn> turns;

    // Tarefa que executa em 2º plano e que obtém os dados do WS das Senhas
    private getTurns getTurns;

    // Componentes que servem para cancelar a tarefa getTurns,
    // se esta demorar mais de GET_TURNS_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    // Temporizador utilizado no refresh da Página dos Serviços Académicos
    private Timer timer;

    /**
     * Retorna uma instância de AcademicServicesFragment.
     * @return instância de AcademicServicesFragment.
     */
    public static AcademicServicesFragment newInstance() {
        AcademicServicesFragment fragment = new AcademicServicesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_academic_services, container, false);

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS das Senhas
        getTurns = new getTurns();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getTurns, getActivity(), getString(R.string.taskCancelerText));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);
        getTurns.execute(this.getActivity());

        // Inicializar o temporizador
        timer = new Timer();

        return view;
    }

    // Cancelar o refresh automático quando o fragmento é pausado
    @Override
    public void onPause()
    {
        super.onPause();
        timer.cancel();
    }


    /**
     * Tarefa que executa em 2º plano e que obtém os dados do WS das Senhas
     */
    private class getTurns extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(AcademicServicesFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS das Senhas
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.servicosacademicos));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    private void processTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        turns = parser.parseTurns(results);

        Calendar today = Calendar.getInstance();

        // Se não forem retornadas Senhas e se estivermos fora do horário dos SA
        if(turns.size() == 0 && (   today.DAY_OF_WEEK == Calendar.SATURDAY
                ||  today.DAY_OF_WEEK == Calendar.SUNDAY
                ||  today.HOUR_OF_DAY > 16
                ||  today.HOUR_OF_DAY < 9
                || (today.HOUR_OF_DAY >= 16 && today.MINUTE > 0)))
        {
            Toast.makeText(getActivity(), getString(R.string.academicServicesClosed), Toast.LENGTH_LONG).show();
            return;
        }
        else if(turns.size() == 0)
        {
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        // Demonstrar resultados na UI
        LinearLayout turns_layout = view.findViewById(R.id.turns);
        for(Turn t : turns)
        {
            Log.d("TURN", t.toString());

            LinearLayout turn_layout = new LinearLayout(getContext());
            turn_layout.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams turn_layout_lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            turn_layout_lp.setMargins(10, 10, 10, 35);
            turn_layout.setLayoutParams(turn_layout_lp);

            final Button letterButton = new Button(getContext());
            letterButton.setTextSize(20);
            if(t.getLetter() != null)
                letterButton.setText(t.getLetter());
                letterButton.setBackgroundResource(R.drawable.button_style);

            // Listener ativado quando se clica no botão de um tipo (letra) de Senha
            letterButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // Redirecionar o Utilizador para a Página da Senha
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    for(final Turn turn : turns)
                    {
                        if(turn.getLetter().equals(letterButton.getText()))
                        {
                            transaction.replace(R.id.home_layout, TurnFragment.newInstance(turn));
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    }
                }
            });;
            turn_layout.addView(letterButton);

            if(t.getNumber() != null)
            {
                TextView numberView = new TextView(getContext());
                String sourceString = "  " + t.getNumber() + " |  ";
                numberView.setText(sourceString);
                numberView.setTextSize(20);
                numberView.setTextColor(Color.BLACK);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(110, RelativeLayout.LayoutParams.WRAP_CONTENT);
                numberView.setLayoutParams(params);
                numberView.setGravity(Gravity.CENTER_VERTICAL);
                turn_layout.addView(numberView);
            }

            if(t.getDesignation() != null)
            {
                TextView designationView = new TextView(getContext());
                designationView.setText(t.getDesignation());
                designationView.setTextSize(15);
                designationView.setTextColor(Color.BLACK);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                designationView.setLayoutParams(params);
                designationView.setGravity(Gravity.CENTER_VERTICAL);
                turn_layout.addView(designationView);
            }

            turns_layout.addView(turn_layout);

            // Atualizar o fragmento de REFRESH_INTERVAL em REFRESH_INTERVAL milisegundos
            timer.scheduleAtFixedRate(new TimerTask()
            {
                @Override
                public void run()
                {
                    ((HomeActivity)getActivity()).refreshASFragment();
                }
            }, /* Intervalo de tempo até começar a executar a TimerTask */ REFRESH_INTERVAL, /* Intervalo de tempo entre cada execução */ REFRESH_INTERVAL);
        }
    }

}
