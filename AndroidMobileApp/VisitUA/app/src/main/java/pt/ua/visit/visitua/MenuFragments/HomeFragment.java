package pt.ua.visit.visitua.MenuFragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pt.ua.visit.visitua.HelperClasses.DateConverter;
import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.OtherFragments.CollectionFragment;
import pt.ua.visit.visitua.OtherFragments.CollectionListFragment;
import pt.ua.visit.visitua.POJOS.Collection;
import pt.ua.visit.visitua.POJOS.News;
import pt.ua.visit.visitua.R;

import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.TASK_TIMEOUT;

/**
 * Fragmento que suporta a Página Inicial
 */
public class HomeFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    // Classe utilizada para converter a data no formato português
    private DateConverter dateConverter;

    // View correspondente ao layout das notícias
    private LinearLayout newsLayout;

    // Tarefa que executa em 2º plano e que obtém os dados do WS das Notícias
    private HomeFragment.getNews getNews;

    // Componentes que servem para cancelar a tarefa getNews,
    // se esta demorar mais de GET_NEWS_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de HomeFragment.
     * @return instância de HomeFragment.
     */
    public static HomeFragment newInstance()
    {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        newsLayout = view.findViewById(R.id.news);

        dateConverter = new DateConverter();

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS das Notícias
        getNews = new HomeFragment.getNews();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getNews, getActivity(), getString(R.string.taskCancelerText));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);

        // Invocar a tarefa que obtém os dados do WS das Notícias
        getNews.execute(this.getActivity());

        // View correspondente ao botão que redireciona para a página do Facebook da UA
        ImageView img = (ImageView)view.findViewById(R.id.fbImgView);

        // View correspondente ao botão que redireciona para a página do Twitter da UA
        ImageView img2 = (ImageView)view.findViewById(R.id.ttImgView);

        // View correspondente ao botão que redireciona para a página do Instagram da UA
        ImageView img3 = (ImageView)view.findViewById(R.id.igImgView);

        // View correspondente ao botão que redireciona para a página do Linkedin da UA
        ImageView img4 = (ImageView)view.findViewById(R.id.lkImgView);

        // View correspondente ao botão que redireciona para a página do Youtube da UA
        ImageView img5 = (ImageView)view.findViewById(R.id.ytImgView);


        // Listener invocado ao clicar no botão do Facebook
        img.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.facebook)));
                getActivity().startActivity(intent);
            }
        });

        // Listener invocado ao clicar no botão do Twitter
        img2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.twitter)));
                getActivity().startActivity(intent);
            }
        });

        // Listener invocado ao clicar no botão do Instagram
        img3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.instagram)));
                getActivity().startActivity(intent);
            }
        });

        // Listener invocado ao clicar no botão do Linkedin
        img4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.linkedin)));
                getActivity().startActivity(intent);
            }
        });

        // Listener invocado ao clicar no botão do Youtube
        img5.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.youtube)));
                getActivity().startActivity(intent);
            }
        });

        return view;
    }

    /**
     * Tarefa que executa em 2º plano e que obtém os dados do WS das Notícias.
     */
    private class getNews extends AsyncTask<Context, Void, String> {

        ProgressBar progressBar = view.findViewById(R.id.progressBarNews);

        @Override
        protected void onPreExecute(){ }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS das Coleções
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.noticiasua));
            return jsonResponse;
        }

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressBar.setVisibility(View.INVISIBLE);
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressBar.setVisibility(View.INVISIBLE);
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    /**
     * Processa o resultado da tarefa getNews.
     * @param results dados JSON.
     */
    @SuppressLint("ClickableViewAccessibility")
    private void processTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        final List<News> newsList = parser.parseNews(results);

        if(newsList.size() == 0)
        {
            // Indicar que a tarefa falhou
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        // Obter uma lista com as notícias
        for( News news : newsList)
        {
            // Obter e demonstrar o título da notícia
            if(news.getTitle() != null){
                TextView title = new TextView(getContext());
                String sourceString = "<b>" +  news.getTitle() + "</b>" + getString(R.string.newLineHTML);
                title.setText(Html.fromHtml(sourceString));
                title.setTextSize(18);
                title.setTextColor(Color.BLACK);
                newsLayout.addView(title);
            }

            // Obter e demonstrar a descrição da notícia
            if (news.getDescription() != null){
                TextView info = new TextView(getContext());
                info.setText(news.getDescription());
                info.setTextSize(15);
                info.setTextColor(Color.BLACK);
                newsLayout.addView(info);
            }

            // Obter e demonstrar a data da notícia
            if(news.getDate() != null){
                TextView date = new TextView(getContext());
                String sourceString = null;
                try {
                    sourceString = "<b>" +  getString(R.string.newsDate) + "</b>" + dateConverter.convertDatePT(news.getDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                date.setText(Html.fromHtml(sourceString));
                date.setTextSize(15);
                date.setTextColor(Color.BLACK);
                newsLayout.addView(date);
            }

            // Obter e demonstrar o link da notícia
            if(news.getLink() != null){
                Button linkButton = new Button(getContext());
                linkButton.setText(getString(R.string.newsLink));
                linkButton.setBackgroundResource(R.drawable.button_style);
                newsLayout.addView(linkButton);

                // Listener invocado ao clicar no botão "Ver mais..." de uma notícia
                linkButton.setOnClickListener(new LinkButtonListener(news.getLink(),getContext()));
            }

            // Adicionar uma linha de separação
            ImageView line = new ImageView(getContext());
            line.setImageResource(R.drawable.line);

            newsLayout.addView(line);
        }
    }

}

/**
 * Listener invocado ao carregar no botão "Ver mais..." de uma notícia
 */
class LinkButtonListener implements View.OnClickListener {

    private String link;
    Context context;

    /**
     * Construtor do Listener
     * @param link Link para a notícia online
     * @param context Contexto da Aplicação
     */
    public LinkButtonListener(String link, Context context) {
        this.link = link;
        this.context = context;
    }

    @Override
    public void onClick(View view) {
        //Redireciona para a página da notícia
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        context.startActivity(intent);
    }
}
