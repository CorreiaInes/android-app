package pt.ua.visit.visitua.MenuFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pt.ua.visit.visitua.OtherFragments.CanteensFragment;
import pt.ua.visit.visitua.OtherFragments.OtherServicesFragment;
import pt.ua.visit.visitua.OtherFragments.AcademicServicesFragment;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página dos Serviços.
 */
public class ServicesFragment extends Fragment {

    /**
     * Retorna uma instância de ServicesFragment.
     * @return instância de ServicesFragment.
     */
    public static ServicesFragment newInstance()
    {
        ServicesFragment fragment = new ServicesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_services, container, false);

        // Listener ativado quando se clica no botão dos Serviços Académicos
        Button academicServicesButton = view.findViewById(R.id.academicServicesButton);
        academicServicesButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar para a Página dos Serviços Académicos
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, AcademicServicesFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        // Listener ativado quando se clica no botão dos Refeitórios
        Button canteensButton = view.findViewById(R.id.canteensButton);
        canteensButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar para a Página dos Refeitórios
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, CanteensFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        // Listener ativado quando se clica no botão dos Outros Serviços
        Button otherServicesButton = view.findViewById(R.id.otherServicesButton);
        otherServicesButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar para a Página dos Outros Serviços
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, OtherServicesFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

}
