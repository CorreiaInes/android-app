package pt.ua.visit.visitua.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import com.testfairy.TestFairy;


import com.darwindeveloper.horizontalscrollmenulibrary.custom_views.HorizontalScrollMenuView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.MenuFragments.SearchFragment;
import pt.ua.visit.visitua.MenuFragments.SettingsFragment;
import pt.ua.visit.visitua.MenuFragments.HomeFragment;
import pt.ua.visit.visitua.MenuFragments.MapFragment;
import pt.ua.visit.visitua.MenuFragments.MuseumFragment;
import pt.ua.visit.visitua.MenuFragments.ParksFragment;
import pt.ua.visit.visitua.MenuFragments.ServicesFragment;
import pt.ua.visit.visitua.MenuFragments.UAFragment;
import pt.ua.visit.visitua.OtherFragments.AcademicServicesFragment;
import pt.ua.visit.visitua.OtherFragments.ArtFragment;
import pt.ua.visit.visitua.OtherFragments.BuildingsFragment;
import pt.ua.visit.visitua.POJOS.Art;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Atividade que gere a página inicial da aplicação.
 */
public class HomeActivity extends AppCompatActivity {

    /**
     * Todas as obras de arte.
     */
    private List<Art> arts;

    /**
     * Gestor dos serviços de localização
     */
    private LocationManager locationManager;

    /**
     * Gestor dos serviços de notificações
     */
    private NotificationManager notificationManager;

    /**
     * Preferências: são utilizadas para guardar o estado do Switch (On/Off) para as notificações do SettingsFragment
     */
    private SharedPreferences notificationPreferences;

    /**
     * Indica se as notificações estão ativadas para a aplicação ou não
     */
    private boolean notificationsEnabled;

    /**
     * Toolbar (barra superior) da aplicação
     */
    private Toolbar toolbar;

    /**
     * Scroll menu (barra inferior) da aplicação
     */
    private HorizontalScrollMenuView menu;


    /**
     * Método de criação da página.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        TestFairy.begin(this, "SDK-XHoJ7gM6");

        // Obter as preferências guardadas relativamente às notificações
        notificationPreferences = getSharedPreferences(NOTIFICATIONS_PREFERENCES_NAME, 0);
        // Obter o estado das preferências das notificações
        checkNotificationsState();

        // Se as notificações não estiverem ativadas
        if(!notificationsEnabled)
            askForNotificationsActivationForThisApp();

        // Inicializar os gestores dos serviços de localização e notificações
        locationManager = (LocationManager) getSystemService(getBaseContext().LOCATION_SERVICE);
        notificationManager = (NotificationManager) getSystemService(getBaseContext().NOTIFICATION_SERVICE);

        // Pede para o utilizadr ativar os serviços de localização do dispositivo, se ainda não estiverem ativados
        askForLocationServicesActivation();

        // Invocar a tarefa que inicia o processo de notificar o utilizador com base na sua proximidade a obras de arte
        new locationBasedNotifications().execute(this);

        // Definir o Scroll menu e Toolbar da aplicação
        menu = findViewById(R.id.menu);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Criar o menu
        createMenu();

        // Verificar se existe alguma notificação enviada na qual o utilizador clicou
        String notification = getIntent().getStringExtra(NOTIFICATION_SENT_KEY);
        if (notification != null)
        {
            try
            {
                // Obter a obra de arte que disparou a notificação
                Art art = (Art) getIntent().getSerializableExtra(NOTIFICATION_ART_KEY);
                Log.i("NOTIFICATION ART", art.toString());  // Imprimir na consola

                // Carregar o fragmento com os detalhes da obra de arte que disparou a notificação
                ArtFragment fragment = ArtFragment.newInstance(art);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, fragment);
                transaction.addToBackStack(String.valueOf(fragment));
                transaction.commit();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * A partir das preferências, obtém o estado das preferências para as notificações.
     */
    public void checkNotificationsState()
    {
        if(notificationPreferences != null)
            notificationsEnabled = notificationPreferences.getBoolean(NOTIFICATIONS_SWITCH_KEY, false);
    }

    /**
     * Pede para o utilizador ativar as notificações desta aplicação, se ainda não estiverem ativados.
     */
    public void askForNotificationsActivationForThisApp()
    {
        // Preparar o pedido (pop up) para o utilizador ativar as notificações
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
        alertDialog.setTitle(getString(R.string.notificationsDisabledTitle));
        alertDialog.setMessage(getString(R.string.notificationsDisabledMessage));

        // Definir a ação associada ao botão de aceitação
        alertDialog.setPositiveButton(getString(R.string.notificationsDisabledPositiveButton), new DialogInterface.OnClickListener()
        {
            // Abrir a página das defenições de localização do dispositivo
            public void onClick(DialogInterface dialog, int which) {
                // Carregar o Fragmento das defenições da aplicação
                SettingsFragment settingsFragment = new SettingsFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, settingsFragment);
                transaction.addToBackStack(String.valueOf(settingsFragment));
                transaction.commit();
            }
        });

        // Definir a ação associada ao botão de rejeição
        alertDialog.setNegativeButton(getString(R.string.notificationsNegativeButton), new DialogInterface.OnClickListener()
        {
            // Cancelar o pedido
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Mostrar o pedido
        alertDialog.show();
    }

    /**
     * Pede para o utilizador ativar os serviços de localização do dispositivo, se ainda não estiverem ativados.
     */
    public void askForLocationServicesActivation()
    {
        // Se os serviços de localização não estiverem ativados
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            // Preparar o pedido (pop up) para o utilizador ativar os serviços de localização
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
            alertDialog.setTitle(getString(R.string.locationDisabledTitle));
            alertDialog.setMessage(getString(R.string.locationDisabledMessage));

            // Definir a ação associada ao botão de aceitação
            alertDialog.setPositiveButton(getString(R.string.locationDisabledPositiveButton), new DialogInterface.OnClickListener()
            {
                // Abrir a página das defenições de localização do dispositivo
                    public void onClick(DialogInterface dialog, int which) {
                    Intent locationActivationIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    HomeActivity.this.startActivity(locationActivationIntent);
                }
            });

            // Definir a ação associada ao botão de rejeição
            alertDialog.setNegativeButton(getString(R.string.locationDisabledNegativeButton), new DialogInterface.OnClickListener()
            {
                // Cancelar o pedido
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            // Mostrar o pedido
            alertDialog.show();
        }
    }

    /**
     * Cria e gere o menu da aplicação.
     */
    private void createMenu()
    {
        menu.addItem(getString(R.string.menuItem1), R.drawable.home);
        menu.addItem(getString(R.string.menuItem2), R.drawable.servicos);
        menu.addItem(getString(R.string.menuItem3), R.drawable.map);
        menu.addItem(getString(R.string.menuItem4), R.drawable.museum);
        menu.addItem(getString(R.string.menuItem5), R.drawable.parking);
        menu.addItem(getString(R.string.menuItem6), R.drawable.building);
        menu.addItem(getString(R.string.menuItem7), R.drawable.ua_logo);
        menu.addItem(getString(R.string.menuItem8), R.drawable.search);
        menu.addItem(getString(R.string.menuItem9), R.drawable.settings);

        // Listener invocado quando uma opção do menu é selecionada
        menu.setOnHSMenuClickListener(new HorizontalScrollMenuView.OnHSMenuClickListener()
        {
            @Override
            public void onHSMClick(com.darwindeveloper.horizontalscrollmenulibrary.extras.MenuItem menuItem, int position)
            {
                Fragment selectedFragment = null;

                // Obter a opção selecionada
                switch (menuItem.getText())
                {
                    case "Serviços":
                        selectedFragment = ServicesFragment.newInstance();
                        break;
                    case "Mapa":
                        selectedFragment = MapFragment.newInstance(null);
                        break;
                    case "Museu":
                        selectedFragment = MuseumFragment.newInstance();
                        break;
                    case "Parques":
                        selectedFragment = ParksFragment.newInstance();
                        break;
                    case "Edifícios":
                        selectedFragment = BuildingsFragment.newInstance();
                        break;
                    case "UA":
                        selectedFragment = UAFragment.newInstance();
                        break;
                    case "Home":
                        selectedFragment = HomeFragment.newInstance();
                        break;
                    case "Pesquisa":
                        selectedFragment = SearchFragment.newInstance();
                        break;
                    case "Definições":
                        selectedFragment = SettingsFragment.newInstance();
                        break;
                }

                // Carregar o fragmento para a página correspondente à opção do menu selecionada
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, selectedFragment);
                transaction.addToBackStack(String.valueOf(selectedFragment));
                transaction.commit();
            }
        });

        // Carregar a página inicial, caso nenhuma opção do menu seja selecionada
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_layout, HomeFragment.newInstance());
        transaction.commit();
    }

    /**
     * Cria o layout do menu da aplicação.
     * @param menu menu da aplicação.
     * @return true, se for criado com sucesso; false, no caso contrário.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        try
        {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    /**
     * Tarefa que inicia o processo de notificar o utilizador com base na sua proximidade a obras de arte.
     */
    private class locationBasedNotifications extends AsyncTask<Context, Void, String>
    {
        /**
         * Numa thread em 2º plano, obtém os dados das obrs de arte a partir do WS.
         * @param params
         * @return
         */
        @Override
        protected String doInBackground(Context... params)
        {
            WSaccess ws = new WSaccess();
            String jsonResponse = ws.get(getString(R.string.obrasarte));
            return jsonResponse;
        }

        /**
         * Método que recebe a resposta do método que executa numa thread em 2º plano.
         * @param jsonResponse resposta do método que executa numa thread em 2º plano.
         */
        protected void onPostExecute(String jsonResponse)
        {
            List<Art> temp_arts = new ArrayList<>();
            if (!jsonResponse.equals(""))
            {
                // Processar a resposta
                ParserJSON parser = new ParserJSON();
                temp_arts = parser.parseArt(jsonResponse);
            }
            HomeActivity.this.arts = temp_arts;

            // Prosseguir apenas se as notificações e os serviços de localização estiverem ativados
            if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || !notificationsEnabled)
                return;

            // Se a aplicação não tiver permissão para aceder à localização do dispositivo
            if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                // Preparar o pedido (pop up) para o utilizador permitir o acesso da aplicação à localização do seu dispositivo
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
                alertDialog.setTitle(getString(R.string.locationDeniedTitle));
                alertDialog.setMessage(getString(R.string.locationDeniedMessage));

                // Definir a ação associada ao botão de aceitação
                alertDialog.setPositiveButton(getString(R.string.locationDeniedPositiveButton), new DialogInterface.OnClickListener()
                {
                    // Abrir a página das defenições de permissões da aplicação
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                });

                // Definir a ação associada ao botão de rejeição
                alertDialog.setNegativeButton(getString(R.string.locationNotifcationGoBackButton), new DialogInterface.OnClickListener()
                {
                    // Cancelar o pedido
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                // Mostrar o pedido
                AlertDialog alert = alertDialog.create();
                alert.show();

                // Não prosseguir com o processo de notificar o utilizador com base na sua proximidade a obras de arte
                return;
            }

            // Se a aplicação tiver acesso à localização do dispositivo e as notificações estiverem ativadas para esta aplicação
            // Avisar o utilizador desse facto
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(HomeActivity.this);
            alertDialog.setTitle(getString(R.string.locationNotificationsGrantedTitle));
            alertDialog.setMessage(getString(R.string.locationNotificationsGrantedMessage));

            // Definir a ação associada ao botão de confirmação
            alertDialog.setNegativeButton(getString(R.string.locationNotifcationGoBackButton), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            // Mostrar o aviso
            AlertDialog alert = alertDialog.create();
            alert.show();

            // Inicializar o listener que recebe atualizações com as novas coordenadas do dispositivo
            // de LOCATION_UPDATE_TIME_INTERVAL em LOCATION_UPDATE_TIME_INTERVAL milisegundos
            // e LOCATION_UPDATE_DISTANCE_INTERVAL em LOCATION_UPDATE_DISTANCE_INTERVAL metros
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_UPDATE_TIME_INTERVAL, LOCATION_UPDATE_DISTANCE_INTERVAL, locationListenerGPS);
        }
    }

    /**
     * Listener que recebe atualizações com as novas coordenadas do dispositivo
     */
    private LocationListener locationListenerGPS = new LocationListener()
    {
        /**
         * Método invocado sempre que o listener recebe uma atualização das coordenadas do dispositivo
         * de LOCATION_UPDATE_TIME_INTERVAL em LOCATION_UPDATE_TIME_INTERVAL milisegundos
         * e LOCATION_UPDATE_DISTANCE_INTERVAL em LOCATION_UPDATE_DISTANCE_INTERVAL metros.
         * @param location localização do Utilizador.
         */
        @Override
        public void onLocationChanged(android.location.Location location)
        {
            // Obter as novas coordenadas do dispositivo
            double newUserLatitude = location.getLatitude();
            double newUserLongitude = location.getLongitude();

            Log.d("LOCATION CHANGED", newUserLatitude + ", " + newUserLongitude);

            int notificationId = 0;

            // Para cada obra de arte
            for (Art a : arts)
            {
                double artLatitude = 0.0;
                double artLongitude = 0.0;

                // Obter as coordenadas da obra de arte
                if (a.getCoordinates() != null)
                {
                    String[] coordsArray = a.getCoordinates().split(",");
                    artLatitude = Double.parseDouble(coordsArray[0].trim());
                    artLongitude = Double.parseDouble(coordsArray[1].trim());
                }

                // Inicializar os componentes para o envio de uma possível notificação
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.notification_icon)
                                .setContentTitle(getString(R.string.notificationTitle))
                                .setVibrate(new long[] {500,1000})
                                .setLights(Color.GREEN, 500, 500)
                                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                .setAutoCancel(true);

                // Se a distância entre as novas coordenadas do dispositivo e as da obra for igual
                // ou inferior a MAX_ART_DISTANCE metros, notificar o Utilizador da distância a que se encontra da obra de arte
                double distance = coordinatesDistance(artLatitude, artLongitude, newUserLatitude, newUserLongitude);
                //distance = coordinatesDistance(artLatitude,  artLongitude, 40.6345684, -8.6578642); // True para o caso do Animal Alma, para testar
                if (distance <= MAX_ART_DISTANCE && notificationsEnabled)
                {
                    NumberFormat formatter = new DecimalFormat("#0.00");

                    // Intent que carrega a página de detahes da obra de arte que disparou a notificação quando o utilizador clica na mesma
                    Intent myIntent = new Intent(getApplicationContext(), HomeActivity.class);
                    myIntent.putExtra(NOTIFICATION_SENT_KEY,"notification");
                    myIntent.putExtra(NOTIFICATION_ART_KEY, a);
                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    mBuilder.setContentText(getString(R.string.notificationText) + formatter.format(distance) + getString(R.string.notificationText1) + a.getName() + getString(R.string.notificationText2));
                    mBuilder.setContentIntent(pendingIntent);
                    notificationManager.notify(notificationId++, mBuilder.build());
                }
            }
        }

        /*
         * Overrides obrigatórios.
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onProviderDisabled(String provider) {}
    };

    /**
     * Calcula a distância, em metros, entre duas coordenadas, tendo em conta a altitude.
     *
     * Src: https://stackoverflow.com/questions/30842011/calculate-distance-between-two-geolocation
     *
     * @param lat1 latitude da coordenada 1.
     * @param lng1 longitude da coordenada 1.
     * @param lat2 latitude da coordenada 2.
     * @param lng2 longitude da coordenada 2.
     * @return distância, em metros, entre (lat1, long1) e (lat2, long2).
     */
    public static double coordinatesDistance(double lat1, double lng1, double lat2, double lng2)
    {
        // Raio da terra em metros
        double earthRadius = 6371000;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double dist = earthRadius * c;

        return dist;
    }

    /**
     * Atualiza a página dos serviços académicos (AcademicServicesFragment)
     * de modo a manter as informações sobre o atendimento das senhas atualizada.
     */
    public void refreshASFragment()
    {
        Log.d("REFRESH FRAGMENT", "AcademicServicesFragment"); // Imprimir na consola
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_layout, AcademicServicesFragment.newInstance());
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
