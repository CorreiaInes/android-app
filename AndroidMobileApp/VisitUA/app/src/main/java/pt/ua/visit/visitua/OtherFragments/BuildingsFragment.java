package pt.ua.visit.visitua.OtherFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Toast;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.POJOS.Building;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a Página com os Edifícios.
 */
public class BuildingsFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém os dados do WS dos Edifícios
    private getBuildings getBuildings;

    // Componentes que servem para cancelar a tarefa getBuildings,
    // se esta demorar mais de GET_BUILDINGS_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de BuildingsFragment.
     * @return instância de BuildingsFragment.
     */
    public static BuildingsFragment newInstance()
    {
        BuildingsFragment fragment = new BuildingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_buildings, container, false);

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS dos Edifícios
        getBuildings = new getBuildings();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getBuildings, getActivity(), getString(R.string.taskCancelerText));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);
        getBuildings.execute(this.getActivity());

        return view;
    }

    private class getBuildings extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(BuildingsFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS dos Edifícios
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.edificios));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    private void processTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        final List<Building> buildings = parser.parseBuilding(results);

        if(buildings.size() == 0)
        {
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText2), Toast.LENGTH_LONG).show();
            return;
        }

        // Apresentar a lista de Edifícios
        final ArrayAdapter<Building> buildingsAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, buildings);
        final ListView buildingsListView = view.findViewById(R.id.buildingsList);
        buildingsListView.setAdapter(buildingsAdapter);

        // Listener ativado quando um Edifício da lista é selecionado
        buildingsListView.setOnItemClickListener(
                new OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View item, int position, long rowId)
                    {
                        // Obter o Edifício selecionado com base na sua posição na lista
                        Building building = buildingsAdapter.getItem(position);

                        // Redirecionar para o Fragmento com a informação sobre o Edifício selecionado
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_layout, BuildingFragment.newInstance(building));
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
        );

        // Componentes para a Pesquisa de Edifícios
        Button searchButton = view.findViewById(R.id.buildingsSearchButton);
        final EditText searchBar = view.findViewById(R.id.buildingsSearchBar);

        // Listener ativado quando o utilizador clica no Botão de Pesquisa
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String searchText = searchBar.getText().toString().trim();
                String searchTextNormalized = Normalizer.normalize(searchText, Normalizer.Form.NFD).toLowerCase();
                Log.d("SEARCH BY BUILDING", searchText);    // Test only

                InputMethodManager inputMethodManager =
                        (InputMethodManager) getActivity().getSystemService(
                                getActivity().INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(
                        getActivity().getCurrentFocus().getWindowToken(), 0);

                // Verificar na lista de Edifícios os que possam corresponder à pesquisa
                List<Building> found = new ArrayList<>();
                for(Building b : buildings)
                {
                    String buildingNameNormalized = "";
                    String buildingIdNormalized = "";
                    String buildingInitialsNormalized = "";
                    if(b.getName() != null)
                        buildingNameNormalized = Normalizer.normalize(b.getName(), Normalizer.Form.NFD).toLowerCase();
                    if(b.getId() != null)
                        buildingIdNormalized = Normalizer.normalize(b.getId(), Normalizer.Form.NFD).toLowerCase();
                    if(b.getInitials() != null)
                        buildingInitialsNormalized = Normalizer.normalize(b.getInitials(), Normalizer.Form.NFD).toLowerCase();

                    if((buildingNameNormalized.equals(searchTextNormalized) || buildingNameNormalized.contains(searchTextNormalized))
                            || (buildingIdNormalized.equals(searchTextNormalized) || buildingIdNormalized.contains(searchTextNormalized))
                            || (buildingInitialsNormalized.equals(searchTextNormalized) || buildingInitialsNormalized.contains(searchTextNormalized)))
                    {
                        found.add(b);
                    }
                }

                // Se houver resultados
                if(!found.isEmpty())
                {
                    // Apresentar a lista de resultados da pesquisa
                    final ArrayAdapter<Building> searchedBuildingsAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_activated_1, found);
                    buildingsListView.setAdapter(searchedBuildingsAdapter);

                    // Listener ativado quando um Edifício da lista de resultados da pesquisa é selecionado
                    buildingsListView.setOnItemClickListener(
                            new OnItemClickListener()
                            {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View item, int position, long rowId)
                                {
                                    // Obter o Edifício selecionado com base na sua posição na lista
                                    Building building = searchedBuildingsAdapter.getItem(position);

                                    // Redirecionar para o Fragmento com a informação sobre o Edifício selecionado
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.home_layout, BuildingFragment.newInstance(building));
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }
                            }
                    );
                }
                else
                {
                    //No caso de não haver resultados, notificar o Utilizador
                    Toast.makeText(getContext(), getString(R.string.searchNoResults), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}

