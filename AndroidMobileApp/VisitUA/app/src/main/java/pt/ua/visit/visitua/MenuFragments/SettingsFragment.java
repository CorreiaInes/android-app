package pt.ua.visit.visitua.MenuFragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import pt.ua.visit.visitua.Activities.HomeActivity;
import pt.ua.visit.visitua.OtherFragments.AboutFragment;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a Página de Definições da Aplicação.
 */
public class SettingsFragment extends Fragment {

    // Estado do switch
    private boolean enabled;

    // Utilizadas para guardar o estado do switch das notificações
    SharedPreferences settings;

    /**
     * Retorna uma instância de SettingsFragment.
     * @return instância de SettingsFragment.
     */
    public static SettingsFragment newInstance()
    {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Obter as SharedPreferences a partir da Home Activity
        settings = this.getActivity().getSharedPreferences(NOTIFICATIONS_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        Switch notificationSwitch = view.findViewById(R.id.switch1);

        // Definir o estado consoante o que foi guardado nos SharedPreferences
        enabled = settings.getBoolean(NOTIFICATIONS_SWITCH_KEY, false);
        notificationSwitch.setChecked(enabled);

        // Listener invocado quando se altera o estado do switch
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                // Guarda o estado do switch nas sharedPreferences
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean(NOTIFICATIONS_SWITCH_KEY, isChecked);
                editor.commit();

                // Muda o estado das notificações na HomeActivity
                ((HomeActivity)getActivity()).checkNotificationsState();

                String result = getString(R.string.notificationDeactivatedTitle);
                if(isChecked)
                    result = getString(R.string.notificationActivatedTitle);

                // Informar o utilizador que é necessário reniciar a aplicação para a alteração surtir efeito
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle(getString(R.string.notificationnChangedTitle) + ' ' + result);
                alertDialog.setMessage(getString(R.string.notificationAlertDialogMessage));

                alertDialog.setNegativeButton(getString(R.string.notificationChangedGoBack), new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert = alertDialog.create();
                alert.show();
            }
        });

        Button aboutButton = view.findViewById(R.id.aboutButton);

        // Listener invocado quando se clica no botão Sobre
        aboutButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Redirecionar para a Página Sobre
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, AboutFragment.newInstance());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        return view;
    }

}
