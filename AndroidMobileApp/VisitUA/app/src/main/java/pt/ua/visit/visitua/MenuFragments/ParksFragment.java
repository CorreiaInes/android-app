package pt.ua.visit.visitua.MenuFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.*;

import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.POJOS.Park;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página com os Parques de Estacionamento.
 */
public class ParksFragment extends Fragment implements OnMapReadyCallback {

    // Componentes para o Mapa
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    // Lista de parques
    private List<Park> parks = new ArrayList<>();

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém os dados do WS dos parques controlados
    private ParksFragment.getControlledParks getControlledParks;

    // Tarefa que executa em 2º plano e que obtém os dados do WS dos parques públicos
    private getPublicParks getPublicParks;

    // Componentes que servem para cancelar as tarefas
    private Handler getConstrolledParksCanceler;
    private TaskCanceler getControlledParksTaskCanceler;
    private Handler getPublicParksCanceler;
    private TaskCanceler getPublicParksTaskCanceler;

    /**
     * Retorna uma instância de ParksFragment.
     * @return instância de ParksFragment.
     */
    public static ParksFragment newInstance()
    {
        ParksFragment fragment = new ParksFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_parks, container, false);

        // Inicializar os componentes necessários para a execução e possível cancelamento das tarefas
        getControlledParks = new getControlledParks();
        getConstrolledParksCanceler = new Handler();
        getControlledParksTaskCanceler = new TaskCanceler(getControlledParks, getActivity(), getString(R.string.taskCancelerText));
        getConstrolledParksCanceler.postDelayed(getControlledParksTaskCanceler, TASK_TIMEOUT);

        getPublicParks = new getPublicParks();
        getPublicParksCanceler = new Handler();
        getPublicParksTaskCanceler = new TaskCanceler(getPublicParks, getActivity(), getString(R.string.taskCancelerText));
        getPublicParksCanceler.postDelayed(getPublicParksTaskCanceler, TASK_TIMEOUT);

        getPublicParks.execute(this.getActivity());
        getControlledParks.execute(this.getActivity());

        return view;
    }

    /**
     * Tarefa que executa em 2º plano e que obtém os dados do WS dos parques públicos.
     */
    private class getPublicParks extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(ParksFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.parquespublicos));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            getPublicParksCanceler.removeCallbacks(getPublicParksTaskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText3), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processPublicParksTaskResults(jsonResponse);
        }
    }

    private void processPublicParksTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        parks = parser.parsePublicParks(results);
    }

    /**
     * Tarefa que executa em 2º plano e que obtém os dados do WS dos parques controlados.
     */
    private class getControlledParks extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(ParksFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.parquesua));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            getConstrolledParksCanceler.removeCallbacks(getControlledParksTaskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText2), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processParksTaskResults(jsonResponse);
        }
    }

    private void processParksTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        List<Park> temp_parks = parser.parseParks(results);

        for(Park p : temp_parks)
            parks.add(p);

        if(parks.size() == 0)
        {
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        // Demonstrar resultados na UI
        LinearLayout parks_layout = view.findViewById(R.id.parks);
        for(Park p : parks)
        {
            Log.d("PARK", p.toString());

            LinearLayout park_layout = new LinearLayout(getContext());
            park_layout.setOrientation(LinearLayout.HORIZONTAL);

            LinearLayout park_info = new LinearLayout(getContext());
            park_info.setOrientation(LinearLayout.VERTICAL);
            LayoutParams park_info_lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            park_info.setLayoutParams(park_info_lp);

            // Nome do Parque
            if(p.getName() != null)
            {
                TextView nameView = new TextView(getContext());
                nameView.setTypeface(null, Typeface.BOLD);
                nameView.setText(p.getName());
                nameView.setTextSize(20);
                nameView.setTextColor(Color.BLACK);
                park_info.addView(nameView);
            }

            // Capacidade
            if(p.getCapacity() != null)
            {
                TextView capacityView = new TextView(getContext());
                capacityView.setTextSize(15);
                LinearLayout.LayoutParams capacityView_lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                capacityView_lp.setMargins(30, 0, 0, 0);
                capacityView.setLayoutParams(capacityView_lp);
                String sourceString = getString(R.string.parkCapacity) + p.getCapacity().toString();
                capacityView.setText(sourceString);

                capacityView.setTextColor(Color.BLACK);
                park_info.addView(capacityView);
            }

            // Número de lugares ocupados
            if(p.getOccupied() != null)
            {
                TextView occupiedView = new TextView(getContext());
                occupiedView.setTextSize(15);
                LinearLayout.LayoutParams occupiedView_lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                occupiedView_lp.setMargins(30, 0, 0, 0);
                occupiedView.setLayoutParams(occupiedView_lp);
                String sourceString = getString(R.string.parkOcupied) + p.getOccupied().toString();
                occupiedView.setText(sourceString);

                occupiedView.setTextColor(Color.BLACK);
                park_info.addView(occupiedView);
            }

            // Número de lugares livres
            if(p.getFree() != null)
            {
                TextView freeView = new TextView(getContext());
                freeView.setTextSize(15);
                LinearLayout.LayoutParams freeView_lp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                freeView_lp.setMargins(30, 0, 0, 0);
                freeView.setLayoutParams(freeView_lp);
                String sourceString = getString(R.string.parkFree) + p.getFree().toString();
                freeView.setText(sourceString);

                freeView.setTextColor(Color.BLACK);
                park_info.addView(freeView);
            }

            park_layout.addView(park_info);
            parks_layout.addView(park_layout);
        }

        // Inicializar componentes do Mapa
        mMapView = view.findViewById(R.id.mapView);
        if (mMapView != null)
        {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync((OnMapReadyCallback) this);
        }
    }

    public void onMapReady(GoogleMap googleMap)
    {
        MapsInitializer.initialize(getContext());

        // Componentes do Mapa
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Marcar o local de cada Parque de Estacionamento no Mapa
        for(Park p : parks)
        {
            LatLng park = new LatLng(p.getLatitute(), p.getLongitude());
            mGoogleMap.addMarker(new MarkerOptions().position(park).title(p.getName()));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(park,14));
        }

        // Listener invocado ao clicar no Mapa
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {
                // Redirecionar o Utilizador para a Página do Mapa
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.home_layout, pt.ua.visit.visitua.MenuFragments.MapFragment.newInstance(parks));
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        // Animações do Mapa
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
    }
}
