package pt.ua.visit.visitua.HelperClasses;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.ua.visit.visitua.POJOS.*;
import pt.ua.visit.visitua.POJOS.Collection;

/**
 * Classe que contém os métodos que efetuam parsing aos diferentes
 * tipos de respostas em formato JSON que os WSs retornam.
 */
public class ParserJSON {

    /**
     * Efetua o parsing dos dados resultantes do WS dos Parques de Estacionamento.
     * @param jsonData dados JSON.
     * @return lista de POJOS para Parque de Estacionamneto.
     */
    public List<Park> parseParks(String jsonData)
    {
        List<Park> parks = new ArrayList<>();
        try
        {
            JSONArray jsonArray = new JSONArray(jsonData);
            // i = 1, porque o índice 0 do jsonArray corresponde à Timestamp do pedido GET
            for(int i = 1; i < jsonArray.length(); i++){
                Park park = new Park();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    park.setName(jsonObject.getString("Nome"));
                } catch (JSONException e)
                {
                    park.setName(null);
                }
                try {
                    park.setLatitute(jsonObject.getDouble("Latitude"));
                } catch (JSONException e)
                {
                    park.setLatitute(null);
                }
                try {
                    park.setLongitude(jsonObject.getDouble("Longitude"));
                } catch (JSONException e)
                {
                    park.setLongitude(null);
                }
                try {
                    park.setCapacity(jsonObject.getInt("Capacidade"));
                } catch (JSONException e)
                {
                    park.setCapacity(null);
                }
                try {
                    park.setOccupied(jsonObject.getInt("Ocupado"));
                } catch (JSONException e)
                {
                    park.setOccupied(null);
                }
                try {
                    park.setFree(jsonObject.getInt("Livre"));
                } catch (JSONException e)
                {
                    park.setFree(null);
                }
                parks.add(park);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return parks;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS das Obras e Arte.
     * @param jsonData dados JSON.
     * @return lista de POJOS para Obra de Arte.
     */
    public List<Art> parseArt(String jsonData)
    {
        List<Art> arts = new ArrayList<>();
        try
        {
            JSONArray jsonArray = new JSONArray(jsonData);
            for(int i = 0; i < jsonArray.length(); i++){
                Art art = new Art();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    art.setId(jsonObject.getInt("id"));
                } catch(JSONException e)
                {
                    art.setId(null);
                }
                try {
                    art.setName(jsonObject.getString("nome"));
                } catch(JSONException e)
                {
                    art.setName(null);
                }
                try {
                    art.setPlace(jsonObject.getString("local"));
                } catch(JSONException e)
                {
                    art.setPlace(null);
                }
                try {
                    art.setAuthor(jsonObject.getString("autor"));
                } catch(JSONException e)
                {
                    art.setAuthor(null);
                }
                try {
                    art.setMaterials(jsonObject.getString("materiais"));
                } catch(JSONException e)
                {
                    art.setMaterials(null);
                }
                try {
                    art.setSize(jsonObject.getString("dimensoes"));
                } catch(JSONException e)
                {
                    art.setSize(null);
                }
                try {
                    art.setDate(jsonObject.getString("dataColocacaoUA"));
                } catch(JSONException e)
                {
                    art.setDate(null);
                }
                try {
                    art.setCoordinates(jsonObject.getString("coordenadas"));
                } catch(JSONException e)
                {
                    art.setCoordinates(null);
                }
                arts.add(art);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return arts;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS dos Serviços.
     * @param jsonData dados JSON.
     * @return lista de POJOS para Outro Serviço.
     */
    public List<OtherService> parseService(String jsonData)
    {
        List<OtherService> otherServices = new ArrayList<>();
        try
        {
            JSONArray jsonArray = new JSONArray(jsonData);
            for(int i = 0; i < jsonArray.length(); i++){
                OtherService otherService = new OtherService();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    otherService.setId(jsonObject.getInt("id"));
                } catch(JSONException e)
                {
                    otherService.setId(null);
                }
                try {
                    otherService.setPlace(jsonObject.getString("local"));
                } catch(JSONException e)
                {
                    otherService.setPlace(null);
                }
                try {
                    otherService.setDesignation(jsonObject.getString("designacao"));
                } catch(JSONException e)
                {
                    otherService.setDesignation(null);
                }
                try {
                    otherService.setCoordinates(jsonObject.getString("coordenadas"));
                } catch(JSONException e)
                {
                    otherService.setCoordinates(null);
                }
                try {
                    otherService.setSchedule(jsonObject.getString("horario"));
                } catch(JSONException e)
                {
                    otherService.setSchedule(null);
                }
                otherServices.add(otherService);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return otherServices;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS dos Edifícios.
     * @param jsonData dados JSON.
     * @return lista de POJOS para Edifício.
     */
    public List<Building> parseBuilding(String jsonData)
    {
        List<Building> buildings = new ArrayList<>();
        try
        {
            JSONArray jsonArray = new JSONArray(jsonData);
            for(int i = 0; i < jsonArray.length(); i++){
                Building building = new Building();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    building.setId(jsonObject.getString("id"));
                } catch(JSONException e)
                {
                    building.setId(null);
                }
                try {
                    building.setName(jsonObject.getString("nome"));
                } catch(JSONException e)
                {
                    building.setName(null);
                }
                try {
                    building.setDescription(jsonObject.getString("descricao"));
                } catch(JSONException e)
                {
                    building.setDescription(null);
                }
                try {
                    building.setInitials(jsonObject.getString("sigla"));
                } catch(JSONException e)
                {
                    building.setInitials(null);
                }
                try {
                    building.setType(jsonObject.getString("tipo"));
                } catch(JSONException e)
                {
                    building.setType(null);
                }
                try {
                    building.setCoordinates(jsonObject.getString("coordenadas"));
                } catch(JSONException e)
                {
                    building.setCoordinates(null);
                }
                try {
                    building.setDate(jsonObject.getString("dataConstrucao"));
                } catch(JSONException e)
                {
                    building.setDate(null);
                }
                try {
                    building.setArchitect(jsonObject.getString("arquiteto"));
                } catch(JSONException e)
                {
                    building.setArchitect(null);
                }
                try {
                    String areasJson = jsonObject.getString("ensino");
                    // Efetuar o parsing das entradas para as Áreas de Ensino
                    List<Area> areas = this.parseAreas(areasJson);
                    for(Area a : areas)
                        building.setArea(a);
                } catch(JSONException e)
                {
                    building.setArea(null);
                }
                buildings.add(building);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return buildings;
    }

    /**
     * Efetua o parsing dos dados resultantes para as Áreas de Ensino de cada Edifício do WS dos Edifícios.
     * @param jsonData dados JSON.
     * @return lista de POJOS para Área de Ensino.
     */
    public List<Area> parseAreas(String jsonData)
    {
        List<Area> areas = new ArrayList<>();
        try
        {
            JSONArray jsonArray = new JSONArray(jsonData);
            for(int i = 0; i < jsonArray.length(); i++){
                Area area = new Area();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    area.setId(jsonObject.getString("id"));
                } catch(JSONException e)
                {
                    area.setId(null);
                }
                try {
                    area.setName(jsonObject.getString("nome"));
                } catch(JSONException e)
                {
                    area.setName(null);
                }
                try {
                    area.setType(jsonObject.getString("tipo"));
                } catch(JSONException e)
                {
                    area.setType(null);
                }
                areas.add(area);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return areas;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS das Senhas.
     * @param jsonData dados JSON.
     * @return lista de POJOS para Senha.
     */
    public List<Turn> parseTurns(String jsonData)
    {
        List<Turn> turns = new ArrayList<>();
        try
        {
            JSONObject jsonObject1 = new JSONObject(jsonData);
            JSONObject items = jsonObject1.getJSONObject("items");
            JSONArray jsonArray = items.getJSONArray("item");
            for(int i = 0; i < jsonArray.length(); i++)
            {
                Turn turn = new Turn();
                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                try {
                    turn.setLetter(jsonObject2.getString("letter"));
                } catch(JSONException e)
                {
                    turn.setLetter(null);
                }
                try {
                    turn.setDesignation(jsonObject2.getString("desc"));
                } catch(JSONException e)
                {
                    turn.setDesignation(null);
                }
                try {
                    turn.setNumber(jsonObject2.getInt("latest"));
                } catch(JSONException e)
                {
                    turn.setNumber(null);
                }
                try {
                    turn.setWaiting(jsonObject2.getInt("wc"));
                } catch(JSONException e)
                {
                    turn.setWaiting(null);
                }
                try {
                    Date date = null;
                    String dateString = jsonObject2.getString("date");
                    if(dateString != null)
                    {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {
                            date = dateFormat.parse(dateString);
                        } catch (ParseException ex)
                        {
                            date = null;
                        }
                        turn.setDate(date);
                    }
                } catch(JSONException e)
                {
                    turn.setDate(null);
                }
                turns.add(turn);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return turns;
    }

    /**
     * Invoca o método que efetua o parsing de dados resultandes do WS das Colecções.
     * @param jsonData dados JSON.
     * @return lista de POJOS para Coleção.
     */
    public List<Collection> parseCollection(String jsonData)
    {
        List<Collection> collections;
        try
        {
            collections = parseCollections(new JSONArray(jsonData));
        }
        catch (JSONException e)
        {
            collections = new ArrayList<>();
        }
        return collections;
    }

    /**
     * Efetua o parsing de Arrays JSON correspondentes a Coleções.
     * @param jsonArray array JSON.
     * @return lista com POJOS para Coleção
     */
    public List<Collection> parseCollections(JSONArray jsonArray)
    {
        List<Collection> collections = new ArrayList<Collection>();
        try
        {
            for(int i = 0; i < jsonArray.length(); i++)
            {
                Collection collection = new Collection();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    collection.setId(jsonObject.getInt("id"));
                } catch(JSONException e)
                {
                    collection.setId(null);
                }
                try {
                    collection.setName(jsonObject.getString("nome"));
                } catch(JSONException e)
                {
                    collection.setName(null);
                }
                try {
                    collection.setDescription(jsonObject.getString("descricao"));
                } catch(JSONException e)
                {
                    collection.setDescription(null);
                }
                try {
                    collection.setDonator(jsonObject.getString("doador"));
                } catch(JSONException e)
                {
                    collection.setDonator(null);
                }
                try {
                    collection.setMotherCollection(jsonObject.getInt("colecaoMae"));
                } catch(JSONException e)
                {
                    collection.setMotherCollection(null);
                }
                try {
                    collection.setVisitationPlace(jsonObject.getString("localVisita"));
                } catch(JSONException e)
                {
                    collection.setVisitationPlace(null);
                }
                try {
                    collection.setPlaceCoordinates(jsonObject.getString("coordenadasLocal"));
                } catch(JSONException e)
                {
                    collection.setPlaceCoordinates(null);
                }
                try {
                    // Efetuar o parsing das entradas para SubColeções
                    JSONArray subcollections = jsonObject.getJSONArray("subColecoes");
                    collection.setSubCollections(parseCollections(subcollections));
                } catch(JSONException e) {
                    collection.setSubCollections(null);
                }
                collections.add(collection);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return collections;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS das Ementas.
     * @param jsonData dados JSON.
     * @return lista de POJOS Refeição.
     */
    public List<Canteen> parseCanteenAndMeal(String jsonData)
    {
        List<Canteen> canteens = new ArrayList<Canteen>();
        try
        {
            JSONArray jsonArray = new JSONObject(jsonData).getJSONArray("menus");
            List<JSONArray> jsonArrayList = new ArrayList<>();
            List<JSONObject> jsonObjectList = new ArrayList<>();

            // Obter os Arrays JSON ou Objetos JSON correspondentes às Refeições a partir da resposta do WS
            for(int i=0; i<jsonArray.length(); i++)
            {
                try
                {
                    JSONArray menu = jsonArray.getJSONObject(i).getJSONArray("menu");
                    jsonArrayList.add(menu);
                }
                catch (JSONException e)
                {
                    JSONObject menuObj = jsonArray.getJSONObject(i).getJSONObject("menu");
                    jsonObjectList.add(menuObj);
                }
            }

            // Obter os Objetos JSON correspondentes às Refeições a partir dos Arrays JSON obtidos
            for( JSONArray array: jsonArrayList)
            {
                for(int i = 0; i<array.length();i++)
                {
                    jsonObjectList.add(array.getJSONObject(i));
                }
            }

            // Efetuar o parsing dos elementos de cada Refeição e das cantinas de cada Objeto JSON
            for (JSONObject object: jsonObjectList)
            {
                JSONObject ementa = object.getJSONObject("@attributes");
                JSONObject items = object.getJSONObject("items");

                if(!contains(ementa.getString("canteen"),canteens))
                {
                    Canteen canteen = parseCanteen(ementa);
                    Meal meal = parseMeal(ementa,items);
                    canteen.setMeal(meal);
                    canteens.add(canteen);
                }
                else
                {
                    for(Canteen c : canteens)
                    {
                        if(c.getName().equalsIgnoreCase(ementa.getString("canteen")))
                        {
                            Meal meal = parseMeal(ementa,items);
                            c.setMeal(meal);
                        }
                    }
                }
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return canteens;
    }

    /**
     * Verifica se uma cantina já se encontra numa lista de cantinas.
     * @param canteenName cantina em questão.
     * @param canteens lista de cantinas.
     * @return true, se já se encontrar; false, se ainda não se encontrar.
     */
    public boolean contains(String canteenName, List<Canteen> canteens)
    {
        boolean result = false;
        for(Canteen c : canteens)
        {
            if(canteenName.equalsIgnoreCase(c.getName()))
            {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Efetua o parsing dos dados resultantes para as Cantinas de cada Refeição do WS das Ementas.
     * @param canteenJson Objeto JSON para a Cantina.
     * @return um Refeitório
     */
    public Canteen parseCanteen(JSONObject canteenJson)
    {
        Canteen canteen = new Canteen();
        try{
            canteen.setName(canteenJson.getString("canteen"));
        }catch (JSONException ex){
            canteen.setName(null);
        }
        return canteen;
    }

    /**
     * Efetua o parsing dos dados resultantes para os elementos de cada Refeição do WS das Ementas.
     * @param mealJson Objeto JSON para a Refeição.
     * @param mealItemsJson Objeto JSON para os elementos da Refeição.
     * @return Refeição.
     */
    public Meal parseMeal(JSONObject mealJson, JSONObject mealItemsJson)
    {
        Meal meal = new Meal();

        try{
            meal.setType(mealJson.getString("meal"));
        }catch (JSONException ex){
            meal.setType(null);
        }

        try{
            meal.setDate(mealJson.getString("date"));
        }catch (JSONException ex){
            meal.setDate(null);
        }

        try{
            meal.setWeekday(mealJson.getString("weekday"));
        }catch (JSONException ex){
            meal.setWeekday(null);
        }

        try{
            meal.setWeekdayNr(mealJson.getInt("weekdayNr"));
        }catch (JSONException ex){
            meal.setWeekdayNr(null);
        }

        try{
            meal.setDisabled(mealJson.getString("disabled"));
        }catch (JSONException ex){
            meal.setDisabled(null);
        }

        try
        {
            HashMap<String,String> itemsHashMap = new HashMap<>();
            JSONArray jsonArray = mealItemsJson.getJSONArray("item");

            for(int i=0; i<jsonArray.length(); i++)
            {
                if(!jsonArray.getString(i).contains("@"))
                {
                    switch (i){
                        case 0:
                            itemsHashMap.put("Sopa",jsonArray.getString(i));
                            break;
                        case 1:
                            itemsHashMap.put("Prato de carne",jsonArray.getString(i));
                            break;
                        case 2:
                            itemsHashMap.put("Prato de peixe",jsonArray.getString(i));
                            break;
                        case 3:
                            itemsHashMap.put("Prato de dieta",jsonArray.getString(i));
                            break;
                        case 4:
                            itemsHashMap.put("Prato de vegetariano",jsonArray.getString(i));
                            break;
                        case 5:
                            itemsHashMap.put("Prato de opção",jsonArray.getString(i));
                            break;
                        case 6:
                            itemsHashMap.put("Salada",jsonArray.getString(i));
                            break;
                        case 7:
                            itemsHashMap.put("Diversos",jsonArray.getString(i));
                            break;
                        case 8:
                            itemsHashMap.put("Sobremesa",jsonArray.getString(i));
                            break;
                        case 9:
                            itemsHashMap.put("Bebida",jsonArray.getString(i));
                            break;
                    }
                }
            }
            meal.setItems(itemsHashMap);
        }
        catch (JSONException e)
        {
            meal.setItems(null);
        }
        return meal;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS dos Parques de Estacionamento.
     * @param jsonData dados JSON.
     * @return lista de POJOS Parques.
     */
    public List<Park> parsePublicParks(String jsonData)
    {
        List<Park> parks = new ArrayList<>();
        try
        {
            JSONArray jsonArray = new JSONArray(jsonData);
            for(int i = 0; i < jsonArray.length(); i++)
            {
                Park park = new Park();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    park.setName(jsonObject.getString("nome"));
                } catch (JSONException e)
                {
                    park.setName(null);
                }
                try {
                    String[] coordsArray = jsonObject.getString("coordenadas").split(",");
                    double latitude = Double.parseDouble(coordsArray[0].trim());
                    double longitude = Double.parseDouble(coordsArray[1].trim());
                    park.setLatitute(latitude);
                    park.setLongitude(longitude);
                } catch (JSONException e)
                {
                    park.setLatitute(null);
                    park.setLongitude(null);
                }
                parks.add(park);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return parks;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS dos Refeitórios.
     * @param jsonData dados JSON.
     * @return lista de POJOS Parques
     */
    public List<Refectory> parseRefectories(String jsonData)
    {
        List<Refectory> refectories = new ArrayList<>();
        try
        {
            JSONArray jsonArray = new JSONArray(jsonData);
            for(int i = 0; i < jsonArray.length(); i++)
            {
                Refectory refectory = new Refectory();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                try {
                    refectory.setName(jsonObject.getString("nome"));
                } catch (JSONException e)
                {
                    refectory.setName(null);
                }
                try {
                    refectory.setCoordinates(jsonObject.getString("coordenadas"));
                } catch (JSONException e)
                {
                    refectory.setCoordinates(null);
                }
                refectories.add(refectory);
            }
        }
        catch(JSONException ex)
        {
            ex.printStackTrace();
        }
        return refectories;
    }

    /**
     * Efetua o parsing da resposta do serviço de reconhecimento de imagem
     * @param jsonData dados JSON.
     * @return iniciais do edificio reconhecido pelo serviço
     */
    public static String parseBuildingImageServiceResponse(String jsonData)
    {
        String initials = "";
        try
        {
            JSONObject jsonObject = new JSONObject(jsonData);
            initials = jsonObject.getString("name");
            Log.d("PHOTO SEARCH RESPONSE", jsonObject.toString());
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return initials;
    }

    /**
     * Efetua o parsing dos dados resultantes do WS das Notícias.
     * @param jsonData dados JSON.
     * @return lista de POJOS Notícia
     */
    public List<News> parseNews(String jsonData)
    {
        List<News> newsList = new ArrayList<>();
        try
        {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONArray jsonArray = jsonObject.getJSONObject("rss").getJSONObject("channel").getJSONArray("item");

            for( int i=0; i<jsonArray.length(); i++)
            {
                News news = new News();
                try {
                    String description = new String(jsonArray.getJSONObject(i).getString("description").getBytes("ISO-8859-1"), "UTF-8");
                    String[] descriptionArray = description.split("/>");
                    news.setDescription(descriptionArray[1]);
                } catch (JSONException e)
                {
                    news.setDescription(null);
                }

                try {
                    news.setLink(jsonArray.getJSONObject(i).getString("link"));
                } catch (JSONException e)
                {
                    news.setLink(null);
                }

                try {
                    String title = new String(jsonArray.getJSONObject(i).getString("title").getBytes("ISO-8859-1"), "UTF-8");
                    news.setTitle(title);
                } catch (JSONException e)
                {
                    news.setTitle(null);
                }

                try {
                    news.setDate(jsonArray.getJSONObject(i).getString("pubDate"));
                } catch (JSONException e)
                {
                    news.setDate(null);
                }
                newsList.add(news);
            }
        }
        catch(JSONException | UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        return newsList;
    }
}