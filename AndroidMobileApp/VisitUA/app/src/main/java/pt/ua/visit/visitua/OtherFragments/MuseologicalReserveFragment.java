package pt.ua.visit.visitua.OtherFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página sobre a Reserva Museológica da UA.
 */
public class MuseologicalReserveFragment extends Fragment {

    /**
     * Retorna uma instancia de MuseologicalReserveFragment.
     * @return instância de MuseologicalReserveFragment.
     */
    public static MuseologicalReserveFragment newInstance()
    {
        MuseologicalReserveFragment fragment = new MuseologicalReserveFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_museological_reserve, container, false);
    }

}
