package pt.ua.visit.visitua.OtherFragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.POJOS.Building;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;
import pt.ua.visit.visitua.R;

/**
 * Fragmento de suporte à Página da Pesquisa por Fotografia
 */
public class SearchByPhotoFragment extends Fragment {

    // Objecto JSON
    private JSONObject imgJson;

    // Lista de POJO Edifício
    private List<Building> buildings = new ArrayList<>();

    // Tarefa que executa em 2º plano e que obtém os dados do WS dos edificios
    private getBuildings getBuildings;

    // Tarefa que executa em 2º plano e que invoca o serviço de reconhecimento de imagens
    private getBuilding getBuilding;

    // Componentes que servem para cancelar as tarefas
    private Handler getBuildingsCanceler;
    private TaskCanceler getBuildingsTaskCanceler;
    private Handler getBuildingCanceler;
    private TaskCanceler getBuildingTaskCanceler;

    // Intervalo de tempo máximo para a execução das tarefas (milisegundos)
    private static final int TASK_TIMEOUT = 10 * 1000;

    /**
     * Retorna uma instância de SearchByPhotoFragment.
     * @return instância de SearchByPhotoFragment
     */
    public static SearchByPhotoFragment newInstance()
    {
        SearchByPhotoFragment fragment = new SearchByPhotoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_search_by_photo, container, false);

        // Inicializar os componentes necessários para a execução e possível cancelamento das tarefas
        getBuildings = new getBuildings();
        getBuildingsCanceler = new Handler();
        getBuildingsTaskCanceler = new TaskCanceler(getBuildings, getActivity(), getString(R.string.taskCancelerText));
        getBuildingsCanceler.postDelayed(getBuildingsTaskCanceler, TASK_TIMEOUT);
        getBuildings.execute(this.getActivity());

        Button photoButton = view.findViewById(R.id.photoButton);

        // Listener para o botão de tirar uma fotografia
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // test(); // Para testar o serviço de reconhecimento de imagens
                if (ActivityCompat.checkSelfPermission(getActivity().getBaseContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                {
                    // Pedir permissão para aceder à camera
                    getActivity().requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                } else {
                    // Iniciar o Intent de tirar uma fotografia
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });

        return view;
    }

    /*
        Método invocado após pedidos de permissão.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(getContext(), getString(R.string.cameraPermission), Toast.LENGTH_LONG).show();

                // Iniciar o Intent de tirar uma fotografia
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else {
                Toast.makeText(getContext(), getString(R.string.cameraNoPermission), Toast.LENGTH_LONG).show();

                // Pedir permissão para aceder à camera
                ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, requestCode);
            }
        }
    }

    /*
        Método invocado quando um Intent termina e envia o resultado.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == CAMERA_REQUEST && resultCode == getActivity().RESULT_OK)
        {
            // Obter a foto tirada
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bytes = stream.toByteArray();
            photo.recycle();
            String imageBase64String = Base64.encodeToString(bytes, Base64.DEFAULT);
            Map<String, String> map = new HashMap<>();
            map.put("image", imageBase64String);
            imgJson = new JSONObject(map);

            // Invocar uma nova instancia da tarefa que invoca o serviço de reconhecimento de imagens
            getBuilding = new getBuilding();
            getBuildingCanceler = new Handler();
            getBuildingTaskCanceler = new TaskCanceler(getBuilding, getActivity(), getString(R.string.cameraServiceError));
            getBuildingCanceler.postDelayed(getBuildingTaskCanceler, TASK_TIMEOUT);
            getBuilding.execute(getActivity());
        }
    }

    private class getBuildings extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS dos Edifícios
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.edificios));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            getBuildingsCanceler.removeCallbacks(getBuildingsTaskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processGetBuildingsTaskResults(jsonResponse);
        }
    }

    public void processGetBuildingsTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        buildings = parser.parseBuilding(results);
    }

    /**
     * Tarefa que executa em 2º plano e que invoca o serviço de reconhecimento de imagens.
     */
    private class getBuilding extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(SearchByPhotoFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage1), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            if(imgJson == null)
                return "";

            // Invocar o serviço de reconhecimento de imagens
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.sendPost(getString(R.string.search), imgJson.toString());
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            getBuildingCanceler.removeCallbacks(getBuildingTaskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.cameraServiceError), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    private void processTaskResults(String results)
    {
        if(buildings.size() == 0)
        {
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        String id = ParserJSON.parseBuildingImageServiceResponse(results);

        Log.d("RECOGNIZED BUILDING ID", id);
        if(id.equals(""))
        {
            Toast.makeText(getActivity(), getString(R.string.cameraServiceError2), Toast.LENGTH_LONG).show();
            return;
        }

        // Encontrar o Edifício que corresponde à resposta do serviço de reconhecimento de imagem
        Building found = null;
        for(Building b : buildings)
        {
            if(b.getId() != null && b.getId().toLowerCase().equals(id.toLowerCase()))
            {
                found = b;
            }
        }

        // Indicar que o serviço não consegui reconhecer o Edifício
        if(found == null)
        {
            Toast.makeText(getActivity(), getString(R.string.cameraServiceError2), Toast.LENGTH_LONG).show();
            return;
        }

        // Redirecionar para o Fragmento com a informação sobre o Edifício encontrado
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.home_layout, BuildingFragment.newInstance(found));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    // Método que serve para testar o serviço de reconhecimento de imagens.
    private void test()
    {
        try
        {
            Bitmap photo = BitmapFactory.decodeResource(getResources(), R.drawable.ieeta);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bytes = stream.toByteArray();
            photo.recycle();
            String imageBase64String = Base64.encodeToString(bytes, Base64.DEFAULT);
            Map<String, String> map = new HashMap<>();
            map.put("image", imageBase64String);
            imgJson = new JSONObject(map);
            // Invocar o serviço de reconhecimento de imagens
            getBuilding.execute(getActivity());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

}
