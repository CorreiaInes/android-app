package pt.ua.visit.visitua.MenuFragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import pt.ua.visit.visitua.POJOS.Art;
import pt.ua.visit.visitua.POJOS.Building;
import pt.ua.visit.visitua.POJOS.Canteen;
import pt.ua.visit.visitua.POJOS.Collection;
import pt.ua.visit.visitua.POJOS.OtherService;
import pt.ua.visit.visitua.POJOS.Park;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento que suporta a página do Mapa
 */
public class MapFragment extends Fragment implements OnMapReadyCallback{

    // Indicação do botão da minha localização no Mapa
    public static boolean flag = false;

    // Componentes para o Mapa
    private MapView mMapView;
    private GoogleMap mGoogleMap;

    // View correspondente ao layout completo da página
    private View view;

    // POJOS a marcar no Mapa
    private static Building building;
    private static Art art;
    private static List<OtherService> otherServices;
    private static List<Park> parks;
    private static Canteen canteen;
    private static Collection collection;

    // Tarefa que executa em 2º plano e que obtém os dados do WS dos Edifícios
    private getbuildings getbuildings;

    // Componentes que servem para cancelar a tarefa getbuildings,
    // se esta demorar mais de GET_BUILDINGS_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    // Botão utilizado para visualizar a Universidade de Aveiro no mapa
    private Button uaButton;

    /**
     * Retorna uma instância de MapFragment.
     * @param object POJO (Art ou Building) a enviar para o MapFragment.
     * @return instância de MapFragment.
     */
    public static MapFragment newInstance(Object object)
    {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();

        Art art = null;
        Building building = null;
        Canteen canteen = null;
        Collection collection = null;

        if(object != null)
        {
            if(object instanceof Art)
            {
                // O POJO é uma instância de Art
                art = (Art) object;
            }
            else if (object instanceof Building)
            {
                // O POJO é uma instância de Building
                building = (Building) object;
            }
            else if (object instanceof Canteen)
            {
                canteen = (Canteen) object;
            }
            else if (object instanceof  Collection){
                collection = (Collection) object;
            }
            else if (object instanceof List){
                if(((List) object).get(0) instanceof OtherService) {
                    otherServices = (List<OtherService>) object;
                }else if(((List) object).get(0) instanceof Park){
                    parks = (List<Park>) object;
                }
            }
        }

        args.putSerializable(BUILDING_KEY, building);
        args.putSerializable(ART_KEY, art);
        args.putSerializable(CANTEEN_KEY, canteen);
        args.putSerializable(COLLECTION_KEY,collection);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_map, container, false);

        // Obter os POJOs enviados
        Bundle args = getArguments();
        building = (Building) args.getSerializable(BUILDING_KEY);
        art = (Art) args.getSerializable(ART_KEY);
        canteen = (Canteen) args.getSerializable(CANTEEN_KEY);
        collection = (Collection) args.getSerializable(COLLECTION_KEY);

        uaButton = view.findViewById(R.id.goToUaButton);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        // Criar a View para o Mapa
        mMapView = view.findViewById(R.id.mapView);
        if (mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
    }

    private void enableMyLocationIfPermitted()
    {
        // Verificar se os Serviços de Localização estão ativados
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(getContext().LOCATION_SERVICE);
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
            // Notificar o utilizador
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle(getString(R.string.locationDisabledTitle));
            alertDialog.setMessage(getString(R.string.locationDisabledMessage));

            alertDialog.setPositiveButton(getString(R.string.locationDisabledPositiveButton), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which) {
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    getActivity().startActivity(myIntent);
                }
            });

            alertDialog.setNegativeButton(getString(R.string.locationDisabledNegativeButton), new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            alertDialog.show();

            return;
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        }
        else if (mGoogleMap != null)
        {
            flag = true;
            mGoogleMap.setMyLocationEnabled(true);
        }
    }

    /**
     * Por defeito, a posição inicialmente marcada no mapa corresponde às coordenadas da UA.
     */
    private void showDefaultLocation()
    {
        goToUA();
    }

    /**
     * Redireciona para o local da UA.
     */
    private void goToUA()
    {
        LatLng uaCoords = new LatLng(40.6303024,-8.657506);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(uaCoords));
    }

    /**
     * Redireciona o mapa para a localização atual do utilizador caso seja dada permissão
     * Redireciona para a localização predefinida
     * @param requestCode código do pedido de localização
     * @param permissions permissão de Localização
     * @param grantResults resultado do pedido de permissão de localização
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocationIfPermitted();
                } else {
                    showDefaultLocation();
                }
                return;
            }

        }
    }

    /**
     * Redireciona o mapa para a localização atual do utilizador caso seja premido o botão
     */
    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    mGoogleMap.setMinZoomPreference(15);
                    return false;
                }
            };

    /**
     * Mostra um circulo com um raio de 20 metros centrado no ponto onde se encontra o utilizador
     */
    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {
                    mGoogleMap.setMinZoomPreference(12);

                    CircleOptions circleOptions = new CircleOptions();
                    circleOptions.center(new LatLng(location.getLatitude(),
                            location.getLongitude()));

                    circleOptions.radius(20);
                    circleOptions.fillColor(Color.BLUE);
                    circleOptions.strokeWidth(6);

                    mGoogleMap.addCircle(circleOptions);
                }
            };

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        MapsInitializer.initialize(getContext());

        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Por defeito as coordenadas marcadas no mapa são as da UA
        LatLng latLng = new LatLng(40.6303024,-8.657506);

        // Verificar quais as componentes a marcar no mapa com um marcador, segundo os argumentos
        // que o Mapa recebeu
        if(building != null && art == null && otherServices == null && parks == null)
        {
            String[] array = building.getCoordinates().split(",");
            latLng = new LatLng(Double.parseDouble(array[0].trim()), Double.parseDouble(array[1].trim()));

            mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(building.getName()).snippet(building.getInitials()));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
        }
        else if(canteen != null)
        {
            String[] array = canteen.getCoordinates().split(",");
            latLng = new LatLng(Double.parseDouble(array[0].trim()), Double.parseDouble(array[1].trim()));

            mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(canteen.getName()));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
        }
        else if(collection != null)
        {
            String[] array = collection.getPlaceCoordinates().split(",");
            latLng = new LatLng(Double.parseDouble(array[0].trim()), Double.parseDouble(array[1].trim()));

            mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(collection.getName()));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
        }
        else if(building == null && art != null && otherServices == null && parks == null)
        {
            String[] array = art.getCoordinates().split(",");
            latLng = new LatLng(Double.parseDouble(array[0].trim()),Double.parseDouble(array[1].trim()));
            mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(art.getName()));
        }
        else if(building == null && art == null && otherServices != null && parks == null){
            for(OtherService service : otherServices)
            {
                String[] coordinatesArray = service.getCoordinates().split(",");
                Double latitude = Double.parseDouble(coordinatesArray[0].trim());
                Double longitude = Double.parseDouble(coordinatesArray[1].trim());

                LatLng servicos = new LatLng(latitude, longitude);
                mGoogleMap.addMarker(new MarkerOptions().position(servicos).title(service.getDesignation()));
            }
        }
        else if(building == null && art == null && otherServices == null && parks != null){
            for(Park p : parks)
            {
                LatLng park = new LatLng(p.getLatitute(), p.getLongitude());
                mGoogleMap.addMarker(new MarkerOptions().position(park).title(p.getName()));
            }
        }
        else
        {
            // Inicializar os componentes necessários para a execução e possível
            // cancelamento da tarefa que obtém os dados do WS dos Edifícios
            getbuildings = new getbuildings();
            canceler = new Handler();
            taskCanceler = new TaskCanceler(getbuildings, getActivity(), getString(R.string.taskCancelerText));
            canceler.postDelayed(taskCanceler, TASK_TIMEOUT);
            getbuildings.execute(this.getActivity());
        }

        // Redireciona o mapa para a UA
        uaButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try{
                    goToUA();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });

        // Redirecionar o mapa para as coordenadas marcadas
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));

        // Animações do mapa
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

        // Disponibilizar o botão de localização no Mapa, se houver permissão
        enableMyLocationIfPermitted();
        if(flag)
        {
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            mGoogleMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
            mGoogleMap.setOnMyLocationClickListener(onMyLocationClickListener);
        }

        // Disponibilizar os botões de Zoom no Mapa
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);

        // Valor minímo de Zoom no Mapa
        mGoogleMap.setMinZoomPreference(15);
    }

    /**
     * Tarefa que é executada numa thread em 2º plano.
     * Obtém os Edifícios a partir do WS;
     */
    private class getbuildings extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(MapFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados dos Edifícios a partir do WS
            WSaccess ws = new WSaccess();
            String jsonResponse = ws.get(getString(R.string.edificios));
            return jsonResponse;
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.progressDialogDataMessage), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    private void processTaskResults(String results)
    {
        // Coordenadas da UA, por defeito
        LatLng latLng = new LatLng(40.6303024,-8.657506);

        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        final List<Building> buildings = parser.parseBuilding(results);

        if(buildings.size() == 0)
        {
            // Indicar que a tarefa falhou
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        LatLng latlng;

        // Marcar os Edifícios mais importantes e os Departamentos no Mapa
        for (Building b: buildings)
        {
            if (b.getId().equalsIgnoreCase("17") ||
                    b.getId().equalsIgnoreCase("25") ||
                    b.getId().equalsIgnoreCase("6")) // Biblioteca, Reitoria, SASUA
            {
                if (b.getCoordinates() != null)
                {
                    String[] array = b.getCoordinates().split(", ");
                    latlng = new LatLng(Double.parseDouble(array[0]), Double.parseDouble(array[1]));
                    mGoogleMap.addMarker(new MarkerOptions().position(latlng).title(b.getName())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                }
            }else if(b.getName().contains("Departamento") || b.getName().contains("departamento")){
                if (b.getCoordinates() != null)
                {
                    String[] array = b.getCoordinates().split(", ");
                    latlng = new LatLng(Double.parseDouble(array[0]), Double.parseDouble(array[1]));
                    mGoogleMap.addMarker(new MarkerOptions().position(latlng).title(b.getName())
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                }
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mMapView.onPause();
        art = null;
        parks = null;
        otherServices = null;
        building = null;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mMapView.onDestroy();
        art = null;
        parks = null;
        otherServices = null;
        building = null;
        collection = null;
    }

    @Override
    public void onLowMemory()
    {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

}
