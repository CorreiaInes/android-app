package pt.ua.visit.visitua.HelperClasses;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Classe que contém um método que converte as datas no formato potuguês
 */
public class DateConverter {

    /**
     * Converte a data para o formato Português
     * @param date data que vem do WS
     * @return data em Português
     * @throws ParseException
     */
    public String convertDatePT(String date) throws ParseException{
        String newDate = "";

        // Obter o formato da data recebida
        DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.US);
        Date mealDate = null;
        mealDate = df.parse(date);

        // Definir o novo formato para a data
        DateFormat df2 = new SimpleDateFormat("MMMM");
        Locale portugal = new Locale("pt","PT");
        df2 = DateFormat.getDateInstance(DateFormat.FULL, portugal);

        // Formatar a data com o novo formato
        newDate = df2.format(mealDate);

        return newDate;
    }
}
