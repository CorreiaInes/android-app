package pt.ua.visit.visitua.OtherFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.MenuFragments.MapFragment;
import pt.ua.visit.visitua.POJOS.Art;
import pt.ua.visit.visitua.R;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;

/**
 * Fragmento de suporte à Página da Arte Pública
 */
public class ArtFragment extends Fragment {

    // POJO da Arte Pública
    private Art art;

    // Layout utilizado para demonstrar as Informações
    private LinearLayout artLayout;

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém a imagem para a Obra de Arte
    private ArtFragment.getImage getImage;

    // Componentes que servem para cancelar a tarefa getImage,
    // se esta demorar mais de GET_IMAGE_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de ArtFragment.
     * @param art POJO Arte Pública
     * @return  instância de ArtFragment
     */
    public static ArtFragment newInstance(Art art)
    {
        ArtFragment fragment = new ArtFragment();
        Bundle args = new Bundle();
        args.putSerializable(ART_KEY, art);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_art, container, false);

        // Obter o POJO Art enviado
        Bundle args = getArguments();
        art = (Art) args.getSerializable(ART_KEY);
        Log.d("SELECTED ART", art.toStringAll());

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS das Artes Públicas
        getImage = new ArtFragment.getImage();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getImage, getActivity(), getString(R.string.taskCancelerText4));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);

        artLayout = view.findViewById(R.id.art);

        // Obter a imagem a partir do serviço
        getImage.execute();

        if(art.getCoordinates() != null)
        {
            // botão para ver a Arte Pública em questão no Mapa
            Button mapa = new Button(getContext());
            mapa.setText(getString(R.string.mapButton));
            mapa.setBackgroundResource(R.drawable.button_style);
            artLayout.addView(mapa);

            // Listener ativado quando se clica no botão para ver no Mapa
            mapa.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    // Redireciona para a Página do Mapa, marcando a Arte Pública em questão no mesmo
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.home_layout, MapFragment.newInstance(art));
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }

        // Definir parametros para o layout
        LinearLayout.LayoutParams textViews_lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textViews_lp.setMargins(5, 5, 5, 5);

        // Demonstrar resultados na UI
        // Nome da Arte
        if(art.getName() != null)
        {
            TextView nameView = view.findViewById(R.id.fragmentTitle);
            nameView.setText(art.getName());
            nameView.setTextColor(Color.BLACK);
        }

        // Local onde se encontra a Arte
        if(art.getPlace() != null)
        {
            TextView placeView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.artLocal) + "</b>" + art.getPlace();
            placeView.setText(Html.fromHtml(sourceString));
            placeView.setTextSize(15);
            placeView.setLayoutParams(textViews_lp);
            placeView.setTextColor(Color.BLACK);
            artLayout.addView(placeView);
        }

        // Nome do Autor da Arte
        if(art.getAuthor() != null)
        {
            TextView authorView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.artAuthor) + "</b>" + art.getAuthor();
            authorView.setText(Html.fromHtml(sourceString));
            authorView.setTextSize(15);
            authorView.setLayoutParams(textViews_lp);
            authorView.setTextColor(Color.BLACK);
            artLayout.addView(authorView);
        }

        // Materiais utilizados na construção da Arte
        if(art.getMaterials() != null)
        {
            TextView materialsView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.artMaterials) + "</b>" + art.getMaterials();
            materialsView.setText(Html.fromHtml(sourceString));
            materialsView.setTextSize(15);
            materialsView.setLayoutParams(textViews_lp);
            materialsView.setTextColor(Color.BLACK);
            artLayout.addView(materialsView);
        }

        // Tamanho da Arte
        if(art.getSize() != null)
        {
            TextView sizeView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.artMeasures) + "</b>" + art.getSize();
            sizeView.setText(Html.fromHtml(sourceString));
            sizeView.setTextSize(15);
            sizeView.setLayoutParams(textViews_lp);
            sizeView.setTextColor(Color.BLACK);
            artLayout.addView(sizeView);
        }

        // Data de construção da Arte
        if(art.getDate() != null)
        {
            TextView dateView = new TextView(getContext());
            String sourceString = "<b>" + getString(R.string.artDate) + "</b>" + art.getDate();
            dateView.setText(Html.fromHtml(sourceString));
            dateView.setTextSize(15);
            dateView.setLayoutParams(textViews_lp);
            dateView.setTextColor(Color.BLACK);
            artLayout.addView(dateView);
        }

        return view;
    }

    /**
     * Tarefa que executa em 2º plano e que obtém a imagem para a Obra de Arte.
     */
    private class getImage extends AsyncTask<Context, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Context... params)
        {
            // Obter imagem
            try
            {
                URL url = new URL(getString(R.string.obrasarteimagem) + art.getId());
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bmp;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        protected void onPostExecute(Bitmap bmp)
        {
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(bmp == null)
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText4), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            try
            {
                ImageView art_img = view.findViewById(R.id.image);
                art_img.setImageBitmap(bmp);
            }
            catch(Exception e)
            {
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText4), Toast.LENGTH_LONG).show();
            }
        }
    }

    // Cancelar a tarefa que obtém a imagem da Coleção ao fechar o fragmento.
    @Override
    public void onPause()
    {
        super.onPause();
        canceler.removeCallbacks(taskCanceler);
    }
}
