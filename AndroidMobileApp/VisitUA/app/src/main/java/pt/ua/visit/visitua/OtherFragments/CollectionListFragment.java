package pt.ua.visit.visitua.OtherFragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pt.ua.visit.visitua.HelperClasses.ParserJSON;
import pt.ua.visit.visitua.HelperClasses.TaskCanceler;
import pt.ua.visit.visitua.HelperClasses.WSaccess;
import static pt.ua.visit.visitua.HelperClasses.ConstantsClass.*;
import pt.ua.visit.visitua.POJOS.Collection;
import pt.ua.visit.visitua.R;

/**
 * Fragmento que suporta a Página com as Coleções.
 */
public class CollectionListFragment extends Fragment {

    // View correspondente ao layout completo da página
    private View view;

    // Tarefa que executa em 2º plano e que obtém os dados do WS das Coleções
    private getCollections getCollections;

    // Componentes que servem para cancelar a tarefa getCollections,
    // se esta demorar mais de GET_COLLECTIONS_TASK_TIMEOUT milisegundos
    private Handler canceler;
    private TaskCanceler taskCanceler;

    /**
     * Retorna uma instância de MuseumFragment.
     * @return instância de MuseumFragment.
     */
    public static CollectionListFragment newInstance()
    {
        CollectionListFragment fragment = new CollectionListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_collection_list, container, false);

        // Inicializar os componentes necessários para a execução e possível
        // cancelamento da tarefa que obtém os dados do WS das Coleções
        getCollections = new getCollections();
        canceler = new Handler();
        taskCanceler = new TaskCanceler(getCollections, getActivity(), getString(R.string.taskCancelerText));
        canceler.postDelayed(taskCanceler, TASK_TIMEOUT);

        // Invocar a tarefa que obtém os dados do WS das Coleções
        getCollections.execute(this.getActivity());

        return view;
    }

    /**
     * Tarefa que executa em 2º plano e que obtém os dados do WS das Coleções.
     */
    private class getCollections extends AsyncTask<Context, Void, String> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute()
        {
            progressDialog= ProgressDialog.show(CollectionListFragment.this.getActivity(), getString(R.string.progressDialogDataTitle),getString(R.string.progressDialogDataMessage), true);
        };

        @Override
        protected String doInBackground(Context... params)
        {
            // Obter os dados do WS das Coleções
            WSaccess ws = new WSaccess();
            String jsonResponse =  "";
            jsonResponse = ws.get(getString(R.string.colecoes));
            return jsonResponse;
        }

        @Override
        protected void onCancelled (String jsonResponse)
        {
            progressDialog.dismiss();
        }

        protected void onPostExecute(String jsonResponse)
        {
            progressDialog.dismiss();
            canceler.removeCallbacks(taskCanceler);

            // Se a resposta for vazia
            if(jsonResponse.equals(""))
            {
                // Indicar que a tarefa falhou
                Toast.makeText(getActivity(), getString(R.string.taskCancelerText), Toast.LENGTH_LONG).show();
                return;
            }
            // Senão, processar a resposta
            processTaskResults(jsonResponse);
        }
    }

    /**
     * Processa o resultado da tarefa getCollections.
     * @param results dados JSON.
     */
    @SuppressLint("ClickableViewAccessibility")
    private void processTaskResults(String results)
    {
        // Efetuar o parsing da resposta do WS
        ParserJSON parser = new ParserJSON();
        final List<Collection> collectionsList = parser.parseCollection(results);

        if(collectionsList.size() == 0)
        {
            // Indicar que a tarefa falhou
            Toast.makeText(getActivity(), getString(R.string.taskCancelerText1), Toast.LENGTH_LONG).show();
            return;
        }

        // Obter uma lista com os nomes das colecções
        List<String> collectionNames = new ArrayList<>();
        for(Collection collection : collectionsList)
        {
            Log.i("COLLECTION", collection.toString());

            if(collection.getName() != null) {
                collectionNames.add(collection.getName());
            }

            // Inserir os nomes das Coleções na lista de Coleções
            final ArrayAdapter<String> collectionsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, collectionNames);
            ListView collectionsListView = view.findViewById(R.id.collectionsList);
            collectionsListView.setAdapter(collectionsAdapter);

            // Listener ativado quando se toca na lista de Coleções
            collectionsListView.setOnTouchListener(new View.OnTouchListener() {
                // Setting on Touch Listener for handling the touch inside ScrollView
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

            });

            // Listener ativado quando uma Coleção da lista de Coleções é ativada
            collectionsListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View item, int position, long rowId){
                        Collection collection = null;

                        // Obter a Coleção selecionada com base na sua posição na lista
                        for(Collection c : collectionsList){
                            if(c.getName().equalsIgnoreCase(collectionsAdapter.getItem(position))){
                                collection = c;
                            }
                        }

                        // Redirecionar para o Fragmento com a informação sobre a Coleção selecionada
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.home_layout, CollectionFragment.newInstance(collection));
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
            );
        }
    }

}
