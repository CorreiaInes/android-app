package pt.ua.visit.visitua.POJOS;

/**
 * POJO Serviço.
 */
public class OtherService {

    private Integer id;             // Número de identificação
    private String place;           // Local onde se encontra
    private String designation;     // Designação
    private String coordinates;     // Coordenadas
    private String schedule;        // Horário

    public OtherService() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "OtherService{" +
                "id=" + id +
                ", place='" + place + '\'' +
                ", designation='" + designation + '\'' +
                ", coordinates='" + coordinates + '\'' +
                ", schedule='" + schedule + '\'' +
                '}';
    }

}
