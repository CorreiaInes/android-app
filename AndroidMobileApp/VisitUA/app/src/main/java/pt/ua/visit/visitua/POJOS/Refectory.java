package pt.ua.visit.visitua.POJOS;

/**
 * POJO Refeitório.
 */
public class Refectory {

    private String name;            // Nome
    private String coordinates;     // Coordenadas

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "Refectory{" +
                "name='" + name + '\'' +
                ", coordinates='" + coordinates + '\'' +
                '}';
    }

}
