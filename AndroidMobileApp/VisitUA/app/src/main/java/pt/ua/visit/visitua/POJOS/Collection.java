package pt.ua.visit.visitua.POJOS;

import java.io.Serializable;
import java.util.List;

/**
 * POJO Coleção.
 */
public class Collection implements Serializable {

    private Integer id;                         // Número de identificação
    private String name;                        // Nome
    private String description;                 // Descrição
    private String donator;                     // Nome do Doador
    private Integer motherCollection;           // Número de identificação da coleção mãe
    private String visitationPlace;             // Local de visita
    private String placeCoordinates;            // Coordenadas do local de visita
    private List<Collection> subCollections;    // Lista de Sub Coleções

    public Collection(){}

    public void setId(Integer id) { this.id = id; }

    public void setName(String name) { this.name = name; }

    public void setDescription(String description) { this.description = description; }

    public void setDonator(String donator) { this.donator = donator; }

    public void setMotherCollection(Integer motherCollection) { this.motherCollection = motherCollection; }

    public void setSubCollections(List<Collection> subCollections) { this.subCollections = subCollections; }

    public Integer getId() { return id; }

    public String getName() { return name; }

    public String getDescription() { return description; }

    public String getDonator() { return donator; }

    public Integer getMotherCollection() { return motherCollection; }

    public String getVisitationPlace() {
        return visitationPlace;
    }

    public void setVisitationPlace(String visitationPlace) {
        this.visitationPlace = visitationPlace;
    }

    public String getPlaceCoordinates() {
        return placeCoordinates;
    }

    public void setPlaceCoordinates(String placeCoordinates) {
        this.placeCoordinates = placeCoordinates;
    }

    public List<Collection> getSubCollections() { return subCollections; }

    @Override
    public String toString() {
        return "Collection{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", donator='" + donator + '\'' +
                ", motherCollection=" + motherCollection +
                ", subCollections=" + subCollections +
                '}';
    }
}

